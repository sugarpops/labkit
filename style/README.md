## Coding Style Notes
(This repo is not being maintained any more after completion of Ben's 5th grade course work and is just getting a few updates here and there.)

This file is a work in progress to define common coding style and documentation style conventions for the LabKit project. Use of automatic tools (e.g. doxygen, bug/feature/"issue" database) is still in the planning stage and might cause a revision of this guide to cater to the tool requirements.

### C and C++

The idea for the following style guides is that .ino files contain applications, .h files define application programming interfaces, and .cpp files contain application programming interface implementations. The header comment information for each is a bit different. In particular, .h files contain comments about what an API does while .cpp files contain comments about how the API functionality is implemented. Likewise, changes, todo, and bug/feature comments have distinct boundaries. For example it's wrong to add a todo for an API feature inside the application. Instead, the application feature request section might have an item describing the feature and, at most, a mention of the API such as "see Foo.h for details". In the .h requests header comments would be the details of the API needed to allow the application feature to be provided. These are of course ideals (and, as part of this project, subject to experimentation). This style guide is also relatively new, so it will take some work to retrofit existing source files to fit these models. 

Aren't CHANGES, TODO, and BUGS/FEATURE REQUESTS overlapping and/or redundent? Here's the intention: A flow from bug/feature request (ideally with creation of an issue in a bug/feature database), to agreed to "todo" item (i.e. being in this section implies agreement to implement the item), to completed change, ideally with the date and commit message being the same info that will be seen in the repository records.

#### .ino header model
```
/*
 * <source file name> -- <summary application>
 *
 * <functional specification of the application>
 *
 * <copyright notice>
 * <license notice>
 *
 * CHANGES:
 *   <change date> <change commit message>
 * TODO:
 *   <todo date> <todo item description and/or issue reference>
 * BUGS AND FEATURE REQUESTS:
 *   <report date> <application bug/feature description and/or issue reference>
 */
```

#### .ino header example

```
/*
 * GraphicalShell.ino -- A graphical shell for the Lab Kit.
 * 
 * There are letters on the screen. 
 * You touch the letters to spell commands, and then press the big green
 *  button to invoke them. Kind of like with a mini OS.
 * 
 * Specified Commands: ls, cd, cat, edit, mkdir, draw, rm
 * Commands In Beta: ls
 * Commands In Alpha: cd, draw
 * 
 * Copyright (c) 2018 Ben Goldberg
 * MIT License (see LICENSE in this repository)
 *
 * CHANGES:
 *   3/29/18 Established header comment style conventions
 * TODO:
 *   3/29/18 make all commands output to TFT
 * BUGS AND FEATURE REQUESTS:
 *   3/29/18 Add . and / to letters so you can type more characters
 */
```

#### .h header comment model

```
/*
 * <source filename> - <summary API description>
 * 
 * <Overview of API functionality>
 * See public interfaces below for details.
 *
 * <copyright notice>
 * <license notice>
 *
 * API CHANGES:
 *   <change date> <API interface syntax/semantic change commit message>
 * API TODO:
 *   <todo entry date> <planned interface change description and/or issue ref>
 * API BUGS AND FEATURE REQUESTS:
 *   <report date> <API bug description and/or issue reference>
 */
```

#### .cpp header comment model

```
/*
 * <source filename> - Implementation of <filename>.h API
 * <Implementation overview>
 * See <.h source filename> for API details.
 * 
 * <copyright notice>
 * <license notice>
 *
 * IMPLEMENTATION CHANGES:
 *   <change date> <change commit message>
 * IMPLEMENTATON TODO:
 *   <entry date> <todo item description and/or issue reference>
 * IMPLEMENTATON BUGS AND FEATURE REQUESTS:
 *   <report date> <bug/feature description or issue reference>
 */
```
[test](https://apexprotofactor.com)
