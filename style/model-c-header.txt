/*
 * DELETE This is a template for .c file header comments to answer these
 * DELETE questions:
 * DELETE 1) What does this do? (VERY brief list of interfaces implemented:
 * DELETE functional spec used by callers of this code should be in the .h 
 * DELETE file with full detail)
 * DELETE 2) How does this code implement the APIs? These are
 * DELETE global details such as algorithms or approaches used, subsystem
 * DELETE compositions, etc, not copies of per-function details.
 * DELETE 3) What external dependencies does this code have? A brief list of
 * DELETE called functions and read only extern data references, at least.
 * DELETE 4) What side effects does this code have? EVERY extern mutated should
 * DELETE be mentioned. Where it's relevant pointing out how a called function
 * DELETE might effect a significant mutation is also relevant.
 * DELETE 5) What assumptions are made by this code?
 *
 * <source filename> - Implementation of <filename>.h API
 * 
 * <Answers to questions above>
 *
 * Copyright (c) 2018 BruVue, Inc. All rights reserved.
 *
 * DELETE Following meant for temporary use to augment Git commit msgs but
 * DELETE should be depended on if the commit msgs are not meaningful and
 * DELETE sufficiently detailed. Clean out when genuinely redundent, especially
 * DELETE when it overlaps info in the .h file. Don't pollute this section
 * DELETE with architectural details: put those in the .h file.
 *
 * IMPLEMENTATION CHANGES:
 *   <change date> <change details>
 * IMPLEMENTATON TODO:
 *   <entry date> <todo item description and/or issue reference>
 * IMPLEMENTATON BUGS AND FEATURE REQUESTS:
 *   <report date> <bug/feature description and any issue reference>
 */
