#!/bin/bash
#
# Compile and link all .ino files, sending output to local file log.
# Display's "failed" for build failures in the directory listed just above
# the message.
#
here=`pwd`
LOG=$here/log
list=`find .. -name '*.ino' -print`
rm -f $LOG
find .. -name '*.ino' -print | while read f ; do
  pushd `dirname $f` >/dev/null 2>&1
  echo `pwd`
  b -nolog >>"$LOG" 2>&1
  if [ $? -ne 0 ] ; then
    echo "== $f build failed ==" >>"$LOG" 2>&1
    echo "== $f build failed =="
  fi
  popd >/dev/null 2>&1
done
