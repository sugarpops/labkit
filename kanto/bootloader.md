#Fuses

##Standard Atmega328P fuses in an Arduino Uno:

###Low fuse (lfuse)  0xff none set

###High fuse (hfuse)  0xde SPIEN, BOOTRST
  SPIEN ON (SPI programming enabled)
  BOOTRST ON (vectors default to boot area)

###Extended fuse (efuse)  0x05 BODLEVEL2, BODLEVEL0
  BODLEVL0-2 == 5: 2.5/2.7/2.9 min/typ/max brownout voltage

###Lock bits 0x0f
  BLB11-12 programmed (zero), preventing application from accessing boootloader area. 
  HOWEVER, the actual "uno compatible" being used (Yourduino "Robo Red") has all lock bits unprogrammed. This explans why a test was able to access bootloader space without having modified the lock bits.

##LabKit Atmega328P fuses in an Arduino Uno changes:

###Low fuse, extended fuse: no changes
###High fuse: 0xdc SPIEN, BOOTSZ0, BOOTRST
  Change sets boot area size to 1024 bytes (512 words) starting
  at B Y T E address 0x7C00
###Lock bits: 0x3f (Not needed: see above)
  Change allows app code to modify high flash memory.
                
TODO:
   Beware the fact that boards.txt, or whatever it's current equivalent is, 
will have a code limit of 32256 bytes. But this is now 512 bytes too large.
