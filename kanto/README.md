## Kanto: A "control program for microcontrollers", or: recapitulation of CP/M 

This directory will hold code that attempts to implement a very simple, Atmega328-based program execution system with a "shell" (aka CLI aka "Command Line Interpreter") that can be used to run programs from a FAT filesystem on an SD card (i.e. the SD card that is part of the graphic display module of LabKit). The very low cost of Atmega chips makes this cost effective even with the limited write endurance of flash memory, assuming easily replacable chips (e.g. DIP packaged).

The shell will execute a few built in commands and if it doesn't recognize a command it will assume there is a same-named file with .HEX suffix and try to load that and copy the decoded binary into flash and execute it. 

Command line arguments will be communicated to the program with a key/value store in EEPROM and the program can return a string back to the shell as an "exit value". The shell will arrange to always get control after a reset, but a means of running the currently loaded program again will be arranged if possible. 

Another feature that might be handled by the shell is execution of scripts that call for a sequence of program executions orchestrated with the shell. But this implies that the shell's execution state can be "parked" in a stable fashion while an application program is executing. 

The lifetimes of shell and application use of RAM will be entirely disjoint: the shell will have to rely on EEPROM and/or SD files to save/restore the state needed to execute scripts.

To build the execution system custom linker scripts will be used to split the system's flash sections into multiple pieces. See "Implementation" below for details of how flash is used and how the system is expected to work.

If memory of CP/M serves (and we're too lazy to go look it up), the "BIOS" was accessed by a simple software interrupt/system call mechanism that involved a jump table. This insulated the application from changes in the memory layout of the BIOS. Kanto does not currently use anything like this (except something vaguely similar for hiding interrupt vector details from bootloader section-resident code). Instead, a big bet is being made on deterministic cross-linker behavior involving the Kanto "BIOS" which is simply defined as the set of flash-based resources shared between an application and the shell arranging to run the application. (As mentioned in memory.md, RAM usage lifetimes of shell and app are disjoint: they both have absolute freedom to use all of RAM with "BIOS-global" variables able to work with different address assignments by the linker.

This project is a proof of concept aimed at learning. Practicality isn't a top priority. It may remain difficult to VERY difficult to build this code properly, it will be very sensitive to tool chain versioning and anything that might cause shared resources to wiggle in memory (this code will be a poster child for the computer science jargon term "brittle").

Current plans are targeting the Atmega328 chip, but with the expectation that LabKit will eventually have a custom PCB featuring an Atmega644. The latter is very similar to the 328 but with twice the memory (of all types) of the 328. This would take a lot of pressure off flash memory, which currently feels like a five pound bag holding six pounds of code. 


## Implementation in terms of memory usage

###Here are flash memory layouts for a typical AVR Atmega-based Arduino system in terms of installation scenarios. Details of the implementation for these follow the maps.

(Yes, the table addresses are upside down from Atmel conventions, but we have to stick with the mental models of the lead developer that predate AVR by decades. With the AVR view "top of stack" is at the bottom of the RAM memory map: we rest our case.)

![Kanto Memory Maps](/kanto/kanto-table.jpg "Memory Maps")

* "New Chip". 
    * Atmega328 from the factory: all flash bits are set (erased)
* "Optiboot and Regular Arduino App". 
    * Optiboot in high memory from 7e00 upward, applications programmed by Optiboot into flash memory from zero upward. 
    * The bootloader section starts at 0x7e00 
    * Apps run with their interrupt vectors starting at zero.
    * An external reset causes execution to start at the beginning of the boot section, that is 0x7e00, and Optiboot decides to handle an upload or run the user app starting at address 0 depending on the situation.
    * For the upload case Optiboot gets serial data, decodes it and writes it to flash it to write a new program to flash, and after writing that it starts the app with a jump to location 0. That goes to "interrupt vector one" that ordinarily holds a different jump instruction that goes to the start of the application code. This is commonly location 0x60, just past the application's interrupt vectors. (The AVR vectors don't actually use 0x60 bytes: there are a few conveniently undefined ones but Arduino-land doesn't attempt to use them.)
* "Kantoboot"
    * There is a potential catch-22 with respect to getting Kanto installed, and some scenarios that caused executing code to be page-erased have already been explored. The current idea is to have a higher-order, but extremely simple bootloader ("Kantoboot") that can either pass control to Optiboot or else attempt to start up the Kanto shell. This code starts at 0x7c00 and the bootloader section start is moved to 0x7c00 for Kanto.
    * The kanto shell needs to write to flash and that can only be done by code running in the bootloader section. So a simple "flashPage" function also resides in the bootloader section at a fixed address where it can be referenced from the shell's support libraries.
    * The kanto shell also needs to both use interrupts and be able to set up interrupts for use by a loaded application. But it also has to be possible to update Kantoboot with Optiboot and update the interrupt vectors that will be used by the Kanto shell. 
    * The AVR architecture supports two sets of vectors: one in the application section starting at zero and one in the bootloader section starting at the first address of that section (in this case 0x7c00). An MCU register flag changes which vectors are being used, and this feature will be used to "hand off" to a loaded application that will start up and use interrupts, while the vectors to be used by the shell getting control back (by the app using an "exit" function or a hardware reset) are preserved. 
    * The current scheme is to make the vectors at 0x7c00 "tranpolines" that simply jump to the "real" shell vectors in the application section. The real vectors are what the AVR linker is targeting when it builds a new Kanto system, and critically, programming this new system does not require modifying the bootloader section. This last detail avoids various "just erased the page and failed to fill it with critical code" scenarios.
    * The shell vector target addresses have to be known at Kantoboot link time, so they are assigned to a fixed set of addresses at the top of the application section, below the start of the bootloader section. So, when the Kanto shell is using the bootloader section vectors control goes to a particular trampoline vector, from there to the corresponding shell vector, and from there to someplace within the application section that the linker assigned when the Kanto shell was built.
        * This begs the question of how the shell startup is going to work. Tricky. The runtime CRT module is defined by an AVR binary object. It wouldn't be too had to make an alternative version of that and arrange for it to be linked instead of the avr5 species file. If the weak reference to initvariant() in the Arduino runtime main.cpp was called before init it could get the vector mode switched from userspace vectors to bootloader vectors, but the calling order is init and then initvariant. Something has to get in ahead of the call to main to switch vector mode. Maybe a class constructor?
    * A chip programmer is used to write the Kantoboot trampoline vectors, the Kantoboot initialization and main function code, the flashPage function, and the Kantoboot version number into the bootloader section starting at 0x7c00 to just below the 0x7e00 start of optiboot. The flashPage function must be in a special linker section at a fixed address the linker can find when it is building a new shell (which is going to call flashPage).
        * The flashPage call is going to be slightly tricky. The most straight forward approach is going to be to jam the flashPage address into a function pointer and invoke the function with that. 
            * Maybe kantoboot could be part of the shell build? It would take some hacks to avoid dupe definitions of main, etc, but if this could be done and then the bootloader part of the image was stripped out of the hex file references like the flashPage function could be fully symbolic. Naaah, too much pain and one could argue this approach is even more ugly than just jamming 0x7cxx into a function pointer variable.
    * To avoid excess hair-pulling the Kanto bootloader will delgate to Optiboot any time LabKit button one is NOT pressed. Then, once the Kanto bootloader and flashPage ar stable and not likely to be requiring more changes the sense will be reversed and use of Optiboot will require holding button one down while issuing a build command that includes an upload.  To summarize the new bootloader code:
        * Look for LabKit button one being pressed
        * NOW: if pressed, start up Kanto shell
        * LATER: if NOT pressed, start Kanto shell else start Optiboot
    * Likewise, the Kantoboot code in the bootloader section needs to be able to "find" the Kanto shell's initialization code that runs just before the Kanto main function is used to set up the runtime and start the shell. A second "trampoline" is put by the kanto linking stage into unused memory just above the shell's (real) interrupt vectors but before the start of the bootloader section. This is the target of the jump to "start Kanto shell" in the description just above.
    * Kantoboot is not expected to be changed frequently because it requires a chip programmer. Any major mistake updating Kantoboot may render the chip unusuable as it ruins the Optiboot startup process (forcing it to be reflashed with a chip programmer or second Arduino acting as an ISP interface).
* "Shell/BIOS". The Labkit shell and basic I/O ("BIOS") libraries in flash.
    * The labkit shell interrupt vectors, starting code and the flashPage function are written to flash
    * There are two regions of flash involved:
        * The BIOS (mostly libraries shared by both the Kanto shell and the applications it runs) 
            * This goes from a fixed starting address known to the linker when it's building an app to run under Kanto. 
            * ONLY FUNCTION BODIES will be allowed in the first (lowest address range) part of this region. It's critical that the library function addresses don't move around between linking the Kanto shell and linking an application: they must use identical addresses. The expectation is that the customized linker script will be able to control this well enough.
            * The Kanto shell's bss-clearing, class constructor, runtime and helper library code along with the body of the shell immediately follow the BIOS along with anything like this specific to the BIOS modules. The key is that the total glob of special code in this category can grow and shrink as the shell is changed, apps change, etc, without causing the BIOS function addresses to move.
                * T H I S is the big bet that will determine whether sharing between the shell and applications will work.
                * The Kanto shell's startup includes switching from the usual to the bootsection interrupt vectors using a scheme TBD.
        * The shell's interrupt vectors are linked just below the start of the bootloader section.
            * If this bootloader section interrupt scheme turns into a mess plan B will be to "stash and later restore" the shell's vectors with additional logic in the Kanto bootloader.
* "Shell + application" after the shell has loaded an application's code and written it to flash
    * The application is linked into three regions of code:
        * The application interrupt vectors followed immediately by the applications bss-clearing, class constructor, runtime and application body, including libraries that are not part of the BIOS. This goes from the start of flash memory upward to larger addresses.
        * The BIOS libraries. This is an IDENTICAL DUPLICATE of the BIOS used by the Kanto shell. The application's hex records defining this region are deleted by a process TBD so the shell doesn't overwrite the BIOS. 
    * So, the application to be run off the LabKit SD card by the shell is linked as if the BIOS libraries are at exactly the same addresses, but the bss-clearing, constructors and other code related to the BIOS *that belong to the application* reside with the application code, while the same code related to the Kanto shell are linked with that code. 
    * Additional hex file editing replaces the vector 0 jump address for the application with a jump into Kantoboot and parks the application start address where the Kanto shell can use it. The shell's startup of an application involves switching it's own interrupts off, switching the interrupt vector mode so application section vectors are used, then branching to to the application's init code.

Below for convenience is a table of the AVR interrupt vectors used by Kanto. 

Note that there is a major SCREW UP in the Micochip/Atmel AVR documentation and code files that's been there forever and that probably won't ever get fixed. Anybody else doing a deep dive into AVR land and the Ardino runtimes needs to realize that in the table below (that is derived from a datasheet compatible with the 328p) VECTOR 1 IS THE RESET VECTOR. In the AVR header files (e.g. iom328p.h) and within the Arduino runtime code VECTOR 1 IS THE FIRST EXTERNAL INTERRUPT VECTOR. A comment in the AVR header mentions that "vector 0 is the reset vector" but doesn't define a label for it. This situation takes the lead developer back to the old days at a computer manufacturer when, after getting to the bottom of a deep can of worms, one developer announced to another that there was an immediate need for a new invention he just designed in his head and he called it "the face slapping machine."

Notice the addresses below are word addresses: shift left one bit for actual byte addresses in flash memory. 

![Atmega328 Vectors: WORD ADDRESSES](/contrib/Atmega328-Vectors.jpg "Atmega328 Vector Table")

[Atmega328 Regs and ISA](http://triembed.org/images/Atmega328-Datasheet-Extract.pdf)

### C/C++ ABI (Application Binary Interface) Notes

* Caller saved registers: r18-r27, r30-r31
* Callee saved registers: r2-r17, r28-r29 (latter pair are FP if used)
* r1 ordinarily always contains zero and instruction that clobbers it must restore it back to zero. r1 and r0 are never allocated by the compiler.
* Register parameters are allocated for actual parameters left to right into registers r25 down to r8 in even-start pairs. foo(uint8_t x = 0x12, uint16_t y=0x4321) passes 0x12 in r25 (r24 ignored) and 0x4321 r24/r25 (r25==0x21). So first nine parameters can be passed if they're all one or two bytes, fewer if some are larger than two bytes.
* IMPORTANT: register parameters MUST BE SAVED BY CALLEE and restored before return if they're mutated by a called function

### Some random notes

* The optiboot bootloader does not use linked variables. It has a special "naked" main function that starts executing without the usual function premamble code.
* Support for a fast as possible nonvolatile memory storage system such as feroelectric RAM with an SPI interface might be useful. The support software would provide a trivial virtual memory subsystem to map "pages" of FRAM to blocks of the Atmega's RAM. C++ dereference operator overloading would hopefully play with the VM to make it relatively transparent to application code.
* The BIOS would naturally include all the stable LabKit library functionality, most especially the planned cooperative (no premption)  multitasking mechanism.
* The ability to chain between programs is orthogonal to Task support:
  * Task support allows a cooperative multitask capability that is effectively like a conventional Arduino program having a handful of functions, but where the functions are "scheduled" by each other and by themselves to be called at various times or in response to various situations.

### Some other notes that were parked in one of the Task library files


On exploring the running of "apps" off the labkit external SD card
  
The basic idea for SD card-based tasks is to have an "overlay" area in the flash memory map that is programmed with "external task" code linked as a self-contained, static binary. At a known location relative to the start of the overlay region would be a header containing one or more tables of function addresses. If nothing else this learning tool would establish just how many times flash can be reprogrammed. So a simple CRC for the programmed bytes is in order to detect a failure of the flash memory. For instructional purposes being able to load and run "apps" off the SD card several thousand would be adequate, especially with socketed MCU chips.
  
Finally, having two or more overlay areas would allow for an extensible "shell" that could run a handful of utility programs, where the semantic would be that when the utility program "exited" the Task scheduler would naturaly hand control back to the shell.  The hard problem with all this is arranging for dynamically allocated (and freed) RAM memory regions. Extern scoped variables will be a nono except for a very few bytes that are linked with a fixed origin corresponding to the overlay. For instance, overlay 1 might have 8KB of flash and 16 bytes of RAM, starting at two specific addresses that the Task manager can find.  But per-task stack support seems impossible for little Atmega chips. But imposing a very strict discipline with respect to RAM memory would not be a bad thing.
   
This whole scheme would need to work with a recapitulation of the old "BIOS" idea where a set of utility functions (starting with malloc()!!) are made available to any code anywhere through calls going through a jump table or the like within the BIOS flash region. The BIOS would simply be an expansion/reorganization of the typical AVR chip bootloader.  

GCC Linker arguments cheat sheet

-static-pie makes static position independent code (must use w compile too)

-u symbol to pretend symbol is undefined and force a library to be linked 

linker command language: https://www.math.utah.edu/docs/info/ld_3.html
