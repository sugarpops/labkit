/*
 * Skeletal shell for Kanto. The body of the shell (Ben's shell!) is
 * intended to go into function "kantoShell".
 *
 * See kanto/README.md for more details
 *
 * Copyright (c) Pete Soper
 * MIT License (see LICENSE in this repository)
 */

#include <Arduino.h>
#include <LabKit.h>
#include <inttypes.h>
#include <avr/io.h>
#include <avr/boot.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include "../kantoboot/kantoboot.h"

extern "C" {
#include "eeprom.h"
    void vectorSelect(uint8_t val); // 1 == bootsection 0 == app section
    typedef void (*void_function_ptr)(void);
    void vdummy();
    void_function_ptr p = vdummy;
}

// TODO: put this in a header when it's clear how to refactor this stuff

void kantoShell();

#define SHELL_MAJOR_VERSION 0
#define SHELL_MINOR_VERSION 1

unsigned const int __attribute__((section(".kantoversion"))) shell_version =
    256 * (SHELL_MAJOR_VERSION) + SHELL_MINOR_VERSION;;

// Weak empty variant initialization function. May be redefined by variant
// files (i.e. some other module defining this function). Without this here
// the linker loads the Arduino main.cpp.o out of libcores.a and that causes
// a duplicate definition of main.

void initVariant() __attribute__((weak));
void initVariant() { }
/*
 * This code starts executing via a call to main from the Arduino runtime
 * that was in turn started by a branch from the Kanto bootloader.
 */

int main(void) {

    // Disassembly of the generated code for this function shows that
    // interrupts are disabled, the "zero reg" (aka r1) is cleared, and
    // the stack pointer is set to the top of RAM. That is, the main preamble
    // code is doing everything that the optiboot main function optimized out
    // via its use of the "OS_main" attribute!

    //vectorSelect(1);

    eeprom_erase();
    eeprom_labelValue("before init", 0x1234);
    init();

    eeprom_appendString("before initVariant");
    initVariant();

    eeprom_appendString("before Serial.begin");
    Serial.begin(9600);
    eeprom_appendString("before shell main");
    Serial.println("shell main");
    Serial.flush();

    // Start the Kanto shell

    kantoShell();
    eeprom_appendString("return from kantoShell!");

    // Should never get here!!

    __asm__ __volatile__ ( JMP_KANTOBOOT );

}
