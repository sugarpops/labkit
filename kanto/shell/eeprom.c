/*
 * Dead simple eeprom interface for logging strings
 *
 * Copyright (c) 2017 Pete Soper
 * MIT license (see LICENSE in this repository)
 */

#include "eeprom.h"

void eeprom_erase() {
  for (uint16_t addr = 0; addr < 1024; addr++) {
    eeprom_write_byte((uint8_t *) addr, 0);
  }
}

void eeprom_appendString(char *msg) {
  uint8_t *addr = 0;
  do {
    if (eeprom_read_byte(addr) == '\0') {
      do {
        eeprom_write_byte((uint8_t *) addr++, (uint8_t) *msg);
      } while (*msg++ != '\0');
      return;
    } else {
      while(((int) addr < 1024) && (eeprom_read_byte(addr++) != 0)) {
      }
    }
  } while(addr < 1024);
}
  
void eeprom_labelValue(char *msg, uint16_t value) {
  char hex_line[6];
  sprintf(hex_line,"=%0x", value);
  eeprom_appendString(msg);
  eeprom_appendString(hex_line);
}
