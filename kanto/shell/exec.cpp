/*
 * This code will be absorbed into the shell after testing is complete.
 *
 * This is an initial program loader to prove out the basic approach.
 * It's specially linked to reside well above the normal starting point in
 * flash (e.g. 0x60), instead being below the bootloader, but actually split
 * into two pieces: one below and a flashPage() function linked near the
 * kanto and optiboot bootloaders so it's part of the BLS (bootloader section).
 * This is because rewriting flash in the app section can only be done from
 * code in the boot section.
 *
 * Read an Intel hex record file and write it to flash memory. The starting
 * address of the first hex record must be flash page aligned and there must
 * be no "gaps" in the records that might confuse the buffering/copying process.
 *
 * New pages of flash are written as dictated by the hex records until they're
 * exhausted.
 *
 * TODO: switch interrupt vectors
 *
 * A branch to zero is then done to start the new program running.
 *
 * Copyright 2017 Pete Soper
 * MIT license (see LICENSE in this repo)
 */

#define DEBUG 1

#include "SPI.h"
#include <SD.h>
extern "C" {

#include "../kantoboot/kantoboot.h"

    void vectorSelect(uint8_t val); // 1 == bootsection vectors 0 == app section

}

// Return the page number for an address

#define PAGE(address) (address >> 7)

// Return the offset with an page for an address

#define OFFSET(address) (address & (FLASH_PAGE_SIZE - 1))

// Return the starting address of a page

#define PAGE_ADDRESS(page) (page << 7)

// Return true if an address is page aligned

#define ALIGNED(address) (!(address & (FLASH_PAGE_SIZE-1)))

#define HEX_RECORD_SIZE 32  // max data bytes per record

#define HEX_RECORD_PREAMBLE_SIZE 9

// Conversions

#define HEX2BIN(c) ((toupper(c) <= '9' ? toupper(c) - '0' : (toupper(c) - '7')))
#define HEXCHAR(v) ((v <= 9 ? '0' + v : '7' + v))
#define HEXSTR(b,a) a[0] = HEXCHAR(((unsigned)b >> 4)); \
                    a[1] = HEXCHAR(((unsigned)b & 0xf)); a[2] = '\0';

#define HEX_DATA_RECORD 0
#define HEX_EOF_RECORD 1
#define HEX_START_RECORD 2
#define HEX_BAD_RECORD 3   // Indicates lexer bug or corrupt file

// Wait for Godot
#define HANG while(true){}

#ifdef DEBUG
/*
 * Print a failure message and wait for a reset
 */

void fail(const __FlashStringHelper *msg, uint16_t data) {
    Serial.print(msg);
    Serial.println(data, HEX);

    // Wait for reset
    // HANG;
}

#endif

/*
 * Read a hex record and return the record type, filling the buffer and
 * setting any address and length info depending on type.
 */

uint16_t readRecord(File hex_file, uint8_t buffer[], uint16_t *address,
                    uint16_t *length) {

    uint8_t temp_buffer[HEX_RECORD_PREAMBLE_SIZE + 1];
    uint8_t sum;
    uint16_t record_type;
    char dummy_char;
    uint16_t temp_val;

    if (hex_file.read((void *)temp_buffer, HEX_RECORD_PREAMBLE_SIZE) !=
            HEX_RECORD_PREAMBLE_SIZE) {
#ifdef DEBUG
        fail(F("EOF "), 0);
#endif
        return HEX_BAD_RECORD;
    }

    if (temp_buffer[0] != ':') {
#ifdef DEBUG
        fail(F("missing : "), 0);
#endif
        return HEX_BAD_RECORD;
    }

    sscanf((const char *) temp_buffer, "%c%2x%4x%2x", &dummy_char,
           length, address, &record_type);

    sum = *length + (*address & 0xff) + (*address >> 8) + record_type;

    // length is number of BYTES following the two hex characters denoting the
    // length. We want to next go through the data bytes, which are each two
    // characters long as hex digits. But before we do we'll confirm here that
    // the checksum and line terminator (return, linefeed) are available.

    if (hex_file.available() < ((int) (*length * 2) + 4)) {
#ifdef DEBUG
        fail(F("EOF "), 0);
#endif
        return HEX_BAD_RECORD;
    }

    // decode data two ascii characters at a time
    for (uint8_t i = 0; i < *length; i++) {
        hex_file.read((void *)temp_buffer, 2);
        sscanf((const char *) temp_buffer, "%2x", &temp_val);
        sum += temp_val;
        buffer[i] = temp_val;
    }

    hex_file.read((void *)temp_buffer, 2);
    sscanf((const char *) temp_buffer, "%2x", &temp_val);
    //uint8_t cmp_val = temp_val;
    sum = ~sum + 1;
    if (temp_val != sum) {
#ifdef DEBUG
        fail(F("check "), temp_val);
#endif
        return HEX_BAD_RECORD;
    }

    // Finally, there should be "DOS/Windows style" line delimiters

    if (hex_file.read() != '\r' || hex_file.read() != '\n') {
#ifdef DEBUG
        fail(F("nl "), 0);
#endif
        return HEX_BAD_RECORD;
    }

    return record_type;
}

void exec(char *filename, char *arguments) {
    uint16_t current_page = 9999;
    uint8_t hex_data_buffer[50];
    uint8_t flash_data_buffer[FLASH_PAGE_SIZE];
    uint8_t page_zero_buffer[FLASH_PAGE_SIZE];
    uint16_t address, length;
    File hex_file;
#if 0
    hex_file = SD.open(filename, FILE_READ);
    if (! hex_file) {
#ifdef DEBUG
        fail(F("open "), 0);
#endif
    }
#else
    Serial.print("launching: ");
    Serial.print(filename);
    Serial.print(" with args: ");
    Serial.println(arguments);
    cli();
    vectorSelect(0);
    asm volatile (" jmp 0");
#endif

    // Just read, erase, copy, and write to flash until EOF seen
    while (true) {
        switch (readRecord(hex_file, hex_data_buffer, &address, &length)) {
        case HEX_START_RECORD:
            break;  // Redundant: vector 1 defines the start
        case HEX_DATA_RECORD:
            // TODO fill buffer until the next byte goes to "some
            // other page", at which point this buffer gets passed
            // to flashPage, EXCEPT if this is page zero, park it
            // in a different buffer until eof seen.
            // don't forget sei() after most pages.
            for (uint16_t i = 0; i < length; i++) {
                uint16_t this_page = PAGE((address + i));
                uint16_t this_offset = OFFSET((address + i));
                if (current_page == 9999) {
                    if (this_page == 0) {
                        page_zero_buffer[this_offset] = hex_data_buffer[i];
                    } else {
                        current_page = this_page;
                        flash_data_buffer[this_offset] = hex_data_buffer[i];
                    }
                } else {
                    if (this_page != current_page) {
                        if (current_page != 9999) {
                            cli();
                            uint8_t status = flashPage(flash_data_buffer, PAGE_ADDRESS(current_page));
                            sei();
                            Serial.println("page flashed");
                            if (status != __FLASH_OK) {
#ifdef DEBUG
                                fail(F("flash failed "), status);
#endif
                                return;
                            }
                        }
                        current_page = this_page;
                        flash_data_buffer[this_offset] = hex_data_buffer[i];
                    } else {
                        flash_data_buffer[this_offset] = hex_data_buffer[i];
                    }
                }
            }
            break;
        case HEX_EOF_RECORD:  {
            // By definition we're done with the current page and
            // it cannot have been flashed already.
            cli();
            uint8_t status = flashPage(flash_data_buffer, PAGE_ADDRESS(current_page));
            sei();
            Serial.println("penultimate page flashed");
            if (status != __FLASH_OK) {
#ifdef DEBUG
                fail(F("flash failed "), status);
#endif
                return;
            }
            cli();
            status = flashPage(page_zero_buffer, 0);
            // OK, THIS is where a "system failure" library function
            // would be useful. No way to signal a problem here

            // Finished. Start new program executing!
            asm volatile (" jmp 0");
        }
        break; // ha ha!
        case HEX_BAD_RECORD:
#ifdef DEBUG
            fail(F("bad record! "), 0);
#endif
            return;
        default:
#ifdef DEBUG
            fail(F("type "), 0);
#endif
            return;
        } // switch
    } // while
} // exec
