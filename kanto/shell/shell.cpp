/*
 * This is a placeholder for the body of the Kanto shell.
 */

#include <Arduino.h>
#include <SPI.h>
#include <EEPROM.h>
#include <SD.h>
#include <LabKit.h>
#include <KeyValue.h>
#include "../kantoboot/kantoboot.h"

// TODO put this in a header when it's clear how to refactor this stuff

void exec(char *filename, char *args);

const char *cmds[] = {"ls", "cd", "cat", "edit", "mkdir", "draw", "rm"};
#define NUMBER_OF_COMMANDS (sizeof(cmds) / sizeof (char *))

/*
 * Read a line of characters from serial input until "enter" is seen.
 * Recognizes "backspace" to erase a character and "control C" to abort
 * the line and repeat the command line prompt. Only buffers printable
 * characters. Support for a single command line argument. The command and
 * argument buffer addresses and max lengths have to be passed in, as this
 * code is headed for one of the libraries.
 *
 * When a space is seen it is considered the delimiter between a command and
 *
 */

void readLine(char *cmd_buffer, uint8_t cmd_buffer_length,
              char *arg_buffer, uint8_t arg_buffer_length, void (*prompt)()) {

    char *p = cmd_buffer;         // Start with command
    uint8_t buffer_length = cmd_buffer_length;// Start with command buffer length
    uint8_t index = 0;            // Next buffer index to store to
    uint8_t blank_count = 0;      // K L U D G E ! !
    char ch;              // Current input character

    *p = 0;               // Make sure empty strings are returned
    arg_buffer[0] = 0;

    /*
     * Loop until enter is seen
     */

    do {
        while (! Serial.available()) {  // Avoids idiotic -1 int return by read
        }
        ch = Serial.read();         // next character
#if 0
        Serial.println();           // debug code
        Serial.print("read ");
        if (isprint(ch)) {
            Serial.print(ch);
        } else {
            Serial.print('.');
        }
        Serial.print(" ");
        Serial.println(ch, HEX);
#endif

        // Handle line editing characters
        switch (ch) {

        case ' ':             // space: switch to arg buffer
            Serial.write(' ');
            blank_count += 1;
            if (buffer_length != arg_buffer_length) {  // ignore redundant space
                cmd_buffer[index] = 0; // terminate command string
                buffer_length = arg_buffer_length; // switch buffers
                p = arg_buffer;
                index = 0;         // restart insertion point
            }
            break;

        case 0x7f:            // backspace
            if (index > 0) {     // Nothing to do if at line start
                Serial.write('\r');
                prompt();                  // BRUTE FORCE: blanks better, except
                index = index - 1;         // we don't know how many to put out
                if (p == arg_buffer) {
                    Serial.print(cmd_buffer);
                    for (int i = 0; i < blank_count; i++) {
                        Serial.write(' ');
                    }
                }
                for (uint8_t i = 0; i < index; i++) {
                    Serial.write(p[i]);
                }
                Serial.print(" ");         // Erase the "deleted character"
                Serial.write('\r');        // Now do it all again
                prompt();
                if (p == arg_buffer) {
                    Serial.print(cmd_buffer);
                    for (int i = 0; i < blank_count; i++) {
                        Serial.write(' ');
                    }
                }
                for (uint8_t i = 0; i < index; i++) {
                    Serial.write(p[i]);
                }
            }
            break;

        case 3:               // CONTROL C to "abort" line
            Serial.println();     // for some reason '\c' doesn't work!
            prompt();
            index = 0;
            break;

        case '\r':            // enter (carriage return)
            Serial.println();
            break;

        case '?':             // Give me the cheat sheet!
            Serial.println();
            for (uint8_t i = 0; i < NUMBER_OF_COMMANDS; i++) {
                Serial.println(cmds[i]);
            }
            index = 0;
            break;

        default:              // Default stored if printable
            if (isprint(ch)) {
                p[index++] = ch;
                Serial.write(ch);
            }
        }

        // A too long line is truncated with extreme prejudice
    } while (index < (buffer_length - 2) && (ch != '\r'));

    p[index] = 0;
}

void printPrompt() {
    Serial.print("arduino@LabKit ");
    Serial.print("$ ");
}

// #include <KeyValue.h> this won't compile yet

void kantoShell() {
    // Cause some nice fat libraries to be linked in to make a pretend BIOS
    String dummy;
    //LabKit.begin();
    Serial.begin(9600);
    Serial.setTimeout(1000000000L); // a timeout is worthless to us
#if 0  // SD card isn't plugged in yet
    SPI.begin(); // Is this duped in SD?
    SD.begin(LabKit.SD_CS);
    // For now just call the init function to force the module to be linked in
    KeyValue.begin();
#endif

    // Now just get a filename from the user and try to load and run it.

    while (true) {

#define LINE_SIZE 32

        char line[LINE_SIZE + 1];
        char args[LINE_SIZE + 1];
        readLine(line, LINE_SIZE, args, LINE_SIZE, printPrompt);
        uint8_t cmd_number = 255;

        for (int i = 0; i < NUMBER_OF_COMMANDS; i++) {
            if (strcmp(cmds[i], line) == 0) {
                cmd_number = i;
                break;
            }
        }

        if (cmd_number < NUMBER_OF_COMMANDS) {
            Serial.print("executing ");
            Serial.print(cmds[cmd_number]);
            Serial.print(" with args: ");
            Serial.println(args);
        } else {
            if (strcmp("test3", line) == 0) {
                exec(line, args);
            } else {
                Serial.print(line);
                Serial.println(" ignored");
            }
        }
    }
}
