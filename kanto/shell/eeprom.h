/*
 * Dead simple eeprom interface for logging strings
 *
 * Copyright (c) 2017 Pete Soper
 * MIT license (see LICENSE in this repository)
 */

#include <stdint.h>
#include <avr/eeprom.h>
#include <stdint.h>
#include <stdio.h>

// Fill the EEPROM with zero bytes
void eeprom_erase();

// Append the given null-terminated string after any other strings in EEPROM
void eeprom_appendString(char *msg);

// Append a string of the form <msg>=<value> after any other strings in EEPROM
void eeprom_labelValue(char *msg, uint16_t value);

