/*
 * Test4 - Experimental linkage test program.
 *
 * This program is just a simple one to run via the Exec program and/or the
 * shell.
 *
 * Copyright (c) 2017 Pete Soper
 *  MIT License (see LICENSE in this repository)
 */


void setup() {
    Serial.begin(9600);
}

void loop() {
    Serial.println("This is Test4");
    delay(1000);
}

