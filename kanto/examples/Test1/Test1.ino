/*
 * Test1 - Experimental linkage test program.
 *
 * This program is just a simple one to run via the Exec program and/or the
 * shell.
 *
 * Copyright (c) 2017 Pete Soper
 *  MIT License (see LICENSE in this repository)
 */

#include <EEPROM.h>
#include <LabKit.h>

void setup() {
    LabKit.begin(9600);
    pinMode(13, OUTPUT); // In case LEDs STILL not working!!
}

void loop() {
    Serial.println("Hello world.");
    digitalWrite(13, HIGH);
    delay(250);
    digitalWrite(13, LOW);
    LabKit.beep();
    delay(1000);
}

