/*
 * Test3 - Experimental linkage test program.
 *
 * This program is just a simple one to run via the Exec program and/or the
 * shell.
 *
 * Copyright (c) 2017 Pete Soper
 *  MIT License (see LICENSE in this repository)
 */

void setup() {
    Serial.begin(9600);
}

uint8_t y;
void foo(uint8_t x) {
    y = x;
}

void loop() {
    Serial.println("This is Test3");
    delay(1000);
    foo(1);

}

