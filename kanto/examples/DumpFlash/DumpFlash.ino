/*
 * DumpFlash - Dump portions of flash memory
 *
 * This is quick and dirty to confirm assumptions about what's going into flash.
 *
 * Copyright (c) 2017 Pete Soper
 *  MIT License (see LICENSE in this repository)
 */


#define START 0x1000            // Start address of dump
#define BYTE_COUNT 256          // Number of bytes to dump

#include <LabKit.h>
#include <avr/pgmspace.h>

/*
 * Dump a set of interrupt vectors.  If app_vectors is false the boot section
 * vectors are dumped. This is dumb: change the define below to set the start
 * address of the boot section.
 */

#define BOOT_SECTION_ADDRESS 0x7c00

void printVectors(uint8_t number_of_vectors, bool app_vectors = true) {
    char temp_string[5];
    uint16_t address = app_vectors ? 0 : BOOT_SECTION_ADDRESS;
    for (uint16_t i = 0; i < number_of_vectors; i++)  {
        Serial.print(i + 1);
        Serial.print(" jmp ");
        uint16_t jump_address = pgm_read_byte_near(address + (i * 4) + 2) ||
                                ((uint16_t) pgm_read_byte_near(i * 4 + 3) << 8);
        sprintf(temp_string, "%04x", jump_address);
        Serial.println(temp_string);
    }
}

void setup() {
    uint8_t buffer[BYTE_COUNT];
    Serial.begin(9600);
    for (uint16_t i = 0; i < BYTE_COUNT; i++) {
        buffer[i] = pgm_read_byte_near(START + i);
    }
    printBytes(START, buffer, BYTE_COUNT);
}

void loop() {
}

