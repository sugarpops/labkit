/*
 * Declarations shared across parts of Kanto
 *
 * Copyright (c) 2017 Pete Soper
 * MIT License (see LICENSE in this repository)
 */

#define __AVR_ATmega328P__

// Page writing parameters and flashPage return values

#define FLASH_PAGE_SIZE 128    // Atmega328 flash page size in bytes

#define __FLASH_OK 0           // normal completion
#define __FLASH_ALIGN 1        // flash address not page aligned
#define __FLASH_ERASE 2        // erase failure
#define __FLASH_INVALID 3      // flash doesn't match buffer contents
#define __FLASH_NOT_ALIGNED 4  // flash doesn't match buffer contents

// (see flashPage function in main.cpp for details)

uint8_t flashPage(uint8_t buffer[], uint8_t *address) __attribute__ ((section (".flashpage")));

typedef uint8_t (*flash_page_pointer)(uint8_t [], uint8_t *);

// Kantoboot is so space-constrained it can't use the usual Arduino runtime
// for I/O. So it uses this ultra-compact function that appears to be C,
// but is actually assembly language. See util.S for details.

uint8_t buttonPressed ();

// Temporary hacks

#define JMP_OPTIBOOT "jmp 0x7e00\n"
#define JMP_KANTO_SHELL "jmp 0x7ba0\n"
#define JMP_KANTOBOOT "jmp 0x7c00\n"

