## Kanto implementation notes

This is a scratch pad used to get details in one place while development is going on.

* Memory Sections
  .ktrampolines - 0x7C00 Makefile.kantoboot
  .kantobootversion - 0x7DFE Makefile.kantoboot
  .flashpage - 0x7D00 Makefile.kantoboot, Makefile.shell(but edited out of that)
  .kshellvectors - 0x7b90 Makefile.shell
  .kantoversion - 0x7bFE Makefile.shell
* Hardwired addresses
    * 0x0000 - application program interrupt vectors, starting with jump to __init of the application's startup code
    * 0x0064 - application program Kanto version, part of bios linkage. This serves as a failsafe to make sure a shell is capable of properly supporting a particular application, as the shell and app version numbers always have to match. (NOT CLEAR HOW YET HOW TO IMPLEMENT THIS)
    * 0x7b90 - shell program interrupt vectors (reached from trampolines in bootloader section), starting with jump to __init of the shell startup code
    * 0x7bfe - two byte major/minor version number of Kanto system. MUST match the version of any app to be run under the shell
    * 0x7c00 - trampoline jumps, starting with jump to __init of Kanto bootloader
    * 0x7cfe - two byte major/minor version number of Kanto bootloader
* Tricky bits
    * Before the Kanto shell starts up the MCUCR register has to be goosed to cause interrupt vectors to be used at the start of the bootloader section.
    * Before the Kanto shell starts an application running the MCUCR register ha to be goosed to cause interrupt vectors used at the start of the application section
    * Space is very tight in the bootloader section, ruling out use of the regular Arduino runtime for things like GPIO pin manipulation. The most staright forward approach turns out to be using assembly language for the I/O there.
    * Likewise, the "trampolines" making up the interrupt vectors at the start of the bootloader section are most easily implemented with assembly language. Why are these necessary? It might might be possible later to put the shell's vectors straight into the bootloader section, but for now, any foul up in this regard makes it an extra pain to fix up the chip (since kbootoader and/or optiboot are disabled by corruption of location 0x7c00)
    * Making the Kanto bootloader modules C vs C++ eliminates another layer of initialization glop so there's plenty of room.
