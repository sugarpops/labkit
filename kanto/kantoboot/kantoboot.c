/*
 * This is the Kanto bootloader. It's function is to define a set of special 
 * interrupt vectors that are "trampolines" to the ones the ones defined by the
 * address space (that stops just short of this code). Its main function
 * conditionally start up the shell or passes control to the Optiboot 
 * bootloader.
 *
 * An alternative to the Arduino runtime code is used for GPIO pin manipulation
 * to avoid the big wad of stuff that would be included in the link if there
 * were references to that. See contrib/README.md for more details.
 *
 * See README.md in the containing directory for more details.
 *
 * Copyright (c) Pete Soper
 * MIT License (see LICENSE in this repository)
 */


#include <stdint.h>

#include "kantoboot.h"
#include <avr/pgmspace.h>
#include <avr/io.h>

// Only used to force linkage fo special sections
uint8_t trampFunc();
uint8_t tramp_ref();
uint16_t dummy1, dummy2;

#define KANTOBOOT_MAJOR_VERSION 0
#define KANTOBOOT_MINOR_VERSION 1

const uint16_t __attribute__((section(".version"))) kantoboot_version = 
                  256*(KANTOBOOT_MAJOR_VERSION) + KANTOBOOT_MINOR_VERSION;

/*
 * This code starts executing via a system reset.
 *   NOW:
 * If LabKit button one is NOT being pushed (low) then immediately jump into 
 * the serial bootloader (optiboot) at 0x7E00. Otherwise, start up the 
 * interactive shell.
 *   LATER:
 * If LabKit button one IS being pushed (low) then immediately jump into 
 * the serial bootloader (optiboot) at 0x7E00. Otherwise, start up the 
 * interactive shell.
 */

int main(void) __attribute__ ((OS_main)) __attribute__ ((section (".init9")));

int main(void) {

  __asm__ __volatile__ ( "cli" );

  // This code will never execute the true block. It's only to force the
  // linker to include special sections and pevent them from being discarded.

  if (trampFunc()) {
    dummy1 = kantoboot_version;
    dummy2 = tramp_ref;
    flashPage(0,0);
  }

  if (buttonPressed()) {

    // It IS being pressed: start the Kanto shell by branching to the jump
    // stashed above the shell's  vectors. 

    __asm__ __volatile__ ( JMP_OPTIBOOT );
    __asm__ __volatile__ ( JMP_KANTO_SHELL );
   
  } else {

    // It's NOT being pressed. Start the serial bootloader.

    __asm__ __volatile__ ( JMP_OPTIBOOT );
  }
}
