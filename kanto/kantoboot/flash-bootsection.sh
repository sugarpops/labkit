#!/bin/sh
if [ ! -d $ARDUINO_DIR ] ; then
  echo "ARDUINO_DIR not defined"
  exit 1
fi
DUDE=$ARDUINO_DIR/hardware/tools/avr/bin/avrdude
CONF=$ARDUINO_DIR/hardware/tools/avr/etc/avrdude.conf

# Set the labkit Atmega328P chip's fuses and flash a relocated optiboot

## Bootloader section to go from 0x7C00 to 0x7FFF
$DUDE -C $CONF -c avrispMKII -p m328p -v -U hfuse:w:0xDC:m >boot.log 2>&1

## Kantoboot in bootloader section 0x7C00 to 0x7DFF
## Need -D to enable "layering" along side shell and shell+apps
$DUDE -D -C $CONF -c avrispMKII -p m328p -v -U flash:w:build-uno/kantoboot.hex:i >>boot.log 2>&1

## Optiboot in bootloader section 0x7E00 to 0x7FFF
## BE SURE -D INCLUDED!!

$DUDE -D -C $CONF -c avrispMKII -p m328p -v -U flash:w:$ARDUINO_DIR/hardware/arduino/avr/bootloaders/optiboot/optiboot_atmega328.hex:i >>boot.log 2>&1
less boot.log
