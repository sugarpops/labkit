/*
 * A simple flash programming function to allow code in the application 
 * section to modify itself.
 *
 * An alternative to the Arduino runtime code is used for GPIO pin manipulation
 * to avoid the big wad of stuff that would be included in the link if there
 * were references to that. See contrib/README.md for more details.
 *
 * See README.md in the containing directory for more details.
 *
 * Copyright (c) Pete Soper
 * MIT License (see LICENSE in this repository)
 */

#include <stdint.h>
#include <avr/pgmspace.h>
#include <avr/boot.h>
#include "../kantoboot/kantoboot.h"

#define TEST_NEEDED	// remove after testing complete

/*
 * This code erases and then writes a page of flash memory (128 bytes with
 * the Atmega328). It's assigned to a special linker section so the shell can
 * reference it symbolically. 
 *
 * TODO:
 *  1) Remove the erase confirmation code below after testing
 *  2) Remove the alignment check after testing
 *  2) Change the linker config (.xn) file to simply include this section
 *     after .text, eliminating the need for any "gap".
 */

__attribute__ ((section (".flashpage")))uint8_t
    flashPage(uint8_t buffer[], uint8_t *address) {

#ifdef TEST_NEEDED
  if ((uint16_t) address & 1) {
    return __FLASH_NOT_ALIGNED;
  }
#endif

  // Erase the page

  __boot_page_erase_normal(address);

  // Wait for SPM to finish

  boot_spm_busy_wait();

#ifdef TEST_NEEDED
  // Confirm page is erased

  for (unsigned int offset = 0; offset < FLASH_PAGE_SIZE; offset++) {
    if (pgm_read_byte_near(address + offset) != 0xff) {
      return __FLASH_ERASE;
    }        
  }
#endif

  // Copy bytes into the AVR chip's programming buffer.
  // Have to use a separate pointer because the start address is needed later
  // and this is a WORD address (++ bumps it by two, not one).

  uint16_t *wordp = (uint16_t *) address;

  for (unsigned int offset = 0; offset < FLASH_PAGE_SIZE; offset += 2) {
      unsigned int temp = buffer[offset] | (buffer[offset+1] << 8);
      // C pointer semanics dictate that variable address gets increased by
      // sizeof(what it points to) with ++. Since this is a pointer to two
      // byte integers, the byte address is increased by two by the ++.
      __boot_page_fill_normal(wordp++, temp);
  }

  // Now write the internal buffer to flash and wait for it to finish

  __boot_page_write_normal((unsigned int)(void*)address);

  boot_spm_busy_wait();

  // Make sure access to the app section is enabled. Is this necessary?

  __boot_rww_enable();

  // Check to see if the bytes were correctly written. THIS IS CRITICAL.
  // THIS CODE IS GOING TO DETECT THE ENDURANCE FAILURE OF FLASH.

  // Byte by byte comparison, so use a uint8_t pointer
  uint8_t *bytep = (uint8_t *) address;

  for (unsigned int offset = 0; offset < FLASH_PAGE_SIZE; offset++) {
    if (buffer[offset] != pgm_read_byte_near(bytep++)) {
      return __FLASH_INVALID;
    }        
  }

  return __FLASH_OK;
}

#if 0
ISR(0) {
  __asm__ __volatile__ ( "jmp 0x7C70");
}
#endif

