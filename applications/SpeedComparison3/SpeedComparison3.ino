/*
 * SpeedComparison3.ino -- adds 75 integers and prints the elapsed time.
 *
 * This program adds a ton of integers and prints out the
 * start time, end time, and elapsed time.
 * It was meant to compare the speeds of the Arduino and ESP32.
 * Copy of SpeedComparison2 except with integers.
 *
 * Copyright (c) 2018 Ben Goldberg
 * MIT License (see LICENSE in this repository)
 *
 * CHANGES:
 *   5/2/2018 Initial
 * TODO:
 *
 * BUGS AND FEATURE REQUESTS:
 *
 */
int stuff[250] = {264, 68, 413, 727, 88, 95, 616, 618, 655, 262, 716, 716, 784, 896, 566, 178, 30, 870, 175, 891, 794, 999, 178, 526, 280, 983, 327, 560, 854, 935, 621, 380, 247, 533, 370, 534, 159, 188, 465, 890, 451, 394, 742, 24, 323, 573, 420, 994, 406, 816, 952, 744, 348, 914, 299, 229, 345, 501, 281, 537, 718, 716, 140, 852, 938, 999, 21, 278, 394, 732, 794, 546, 175, 601, 973, 253, 888, 531, 500, 518, 741, 524, 104, 381, 204, 716, 415, 276, 665, 251, 483, 914, 965, 592, 846, 589, 625, 281, 348, 762, 494, 488, 747, 96, 222, 903, 783, 342, 172, 722, 712, 609, 827, 825, 342, 620, 936, 790, 434, 232, 324, 781, 486, 16, 618, 208, 383, 57, 352, 89, 279, 59, 806, 998, 10, 544, 993, 7, 277, 68, 292, 476, 946, 114, 346, 508, 542, 262, 982, 788, 393, 568, 353, 634, 593, 359, 922, 37, 237, 704, 456, 516, 445, 317, 898, 172, 923, 179, 999, 488, 890, 999, 295, 460, 520, 476, 154, 753, 789, 914, 340, 378, 806, 27, 139, 527, 865, 609, 893, 992, 489, 41, 161, 917, 913, 264, 716, 615, 184, 882, 484, 356, 491, 688, 881, 774, 286, 873, 346, 526, 289, 686, 583, 412, 924, 870, 830, 502, 138, 887, 533, 88, 301, 229, 137, 274, 743, 212, 178, 893, 692, 573, 709, 542, 349, 488, 508, 26, 132, 354, 55, 474, 884, 483, 303, 792, 446, 772, 386, 25};
// above numbers randomized using Python
unsigned long total;
void setup() {
    // put your setup code here, to run once:
    // initialize Serial connection
    Serial.begin(9600);
    Serial.println("Connected over USB");
    // start the "stopwatch"
    unsigned long start = millis();
    for (int i = 0; i < 30000; i++) {
        total = 0;
        for (int f : stuff) {
            // tick tock tick tock
            // adding all of the #s
            total += f;
        }
    }
    // and... stop!
    unsigned long end = millis();
    // print our friendly old statistics
    Serial.println("Integer Adding Finished!! Stats:");
    Serial.println(String("Start: ") + start + " ms");
    Serial.println(String("End: ") + end + " ms");
    Serial.println(String("Duration: ") + (end - start) + " ms");
    Serial.println("G'day mate!");
}

void loop() {
    // put your main code here, to run repeatedly:

}
