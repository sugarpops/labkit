/*
 * SpeedComparisonArduinoESP32.ino -- This program compares the speed of the KeyValue library on the Arduino & ESP32.
 *
 * This tests the speed of the KeyValue library by writing and reading 2,160 keys of length 2 with values of length 1.
 *
 * Copyright (c) 2018 Ben Goldberg
 * MIT License (see LICENSE in this repository)
 *
 * CHANGES:
 *   5.2.2018 Initial
 * TODO:
 *
 * BUGS AND FEATURE REQUESTS:
 *
 */

#include <KeyValue.h>
#include <EEPROM.h>
void setup() {
    // put your setup code here, to run once:
    // initialize Serial connection at 9600 bits per second
    Serial.begin(9600);
    // start up KeyValue
    KeyValue.create(0, 1000);
    KeyValue.begin(0, 1000);
    // start stopwatch. tick, tock...
    unsigned long start = millis();
    for (int b = 0; b < 10; b++) {
        for (int i = 0; i < 25; i++) {
            KeyValue.putValue(String('A' + i) + String('0' + b), "*");
        }
    }
    for (int b = 0; b < 10; b++) {
        for (int i = 0; i < 25; i++) {
            String q;
            KeyValue.getValue(String('A' + i) + String('0' + b), q);
            if (q != "*") {
                Serial.println("Error: value isn't an asterisk");
            }
        }
    }
    // hit stop on that silly stopwatch!
    unsigned long end = millis();
    unsigned long duration = end - start;
    // print statistics
    Serial.println("Key/Value Speed Comparison Finished! Stats:");
    Serial.println(String("Start: ") + start + " ms");
    Serial.println(String("End: ") + end + " ms");
    Serial.println(String("Duration: ") + duration + " ms");
    Serial.println("Adios!");
}

void loop() {
    // put your main code here, to run repeatedly:

}
