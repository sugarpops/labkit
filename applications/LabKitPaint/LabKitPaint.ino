/*
 * LabKitPaint.ino
 *
 * Application that uses the display and touchscreen to draw stuff.
 * Copyright (c) 2017-2018 Ben Goldberg
 * MIT License (see LICENSE in this repository)
 * CHANGES:
 *   4/3/18 Updated header comment
 * TODO:
 *
 * BUGS:
 *
 */
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>
#include <SeeedTouchScreen.h>
#include <SPI.h>
#include <SD.h>

/* Below code copied from touchScreen.ino, an example from the Seeed Touch Screen library */

/* Here's the license for that library:

  The MIT License (MIT)

  Copyright (c) 2013 Seeed Technology Inc.

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

// Correct pins
#define YP A2
#define XM A1
#define YM 14
#define XP 17

// Calibrated!
#define TS_MINX 200
#define TS_MAXX 1768
#define TS_MINY 128
#define TS_MAXY 1740

TouchScreen ts = TouchScreen(XP, YP, XM, YM);

/* End of copied code */

/* Below code copied from spitftbitmap.ino, an example from the Adafruit ILI9341 Library */


/***************************************************
  This is our Bitmap drawing example for the Adafruit ILI9341 Breakout and Shield
  ----> http://www.adafruit.com/products/1651
  Check out the links above for our tutorials and wiring diagrams
  These displays use SPI to communicate, 4 or 5 pins are required to
  interface (RST is optional)
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!
  Written by Limor Fried/Ladyada for Adafruit Industries.
  MIT license, all text above must be included in any redistribution
 ****************************************************/

// TODO: Find out values in the LabKit for TFT_DC, TFT_CS, and SD_CS
#define TFT_DC 6
#define TFT_CS 5
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

#define SD_CS 4

/* End of copied code */
/*
// Reads one pixel/color from the TFT's GRAM
uint8_t *readPixel(int16_t x, int16_t y) {
  uint8_t r, g, b;
  tft.setAddrWindow(x, y, x + 1, y + 1);
  tft.writecommand(ILI9341_RAMRD);
  digitalWrite(TFT_DC, HIGH);
  delay(5);
  tft.spiread(); //dummy
  uint8_t pixel[3];
  pixel[0] = tft.spiread();
  pixel[1] = tft.spiread();
  pixel[2] = tft.spiread();
  Serial.print(pixel[0] + pixel[1] + pixel[2]);
  //uint8_t pixel[3] = {r, g, b};
  return pixel;    //.kbv
}

File f, f2, bmp;

void save() {
  if (SD.exists("NUMBER.TXT")) {
    f = SD.open("NUMBER.TXT");
    char num = f.read();
    char buf[13];
    (String("saved") + String(num)).toCharArray(buf, 13);
    bmp = SD.open(buf);
    f.close();
    SD.remove("NUMBER.TXT");
    f = SD.open("NUMBER.TXT", FILE_WRITE);
    f.print(num); // add one
    f.close();
    // BMP header starts here
    // ASCII for "BM", always at the start of a BMP file
    bmp.write(0x42);
    bmp.write(0x4D);
    // File size is 212454 bytes, which is 03 3D E6 in hexadecimal
    bmp.write((byte)0x00);
    bmp.write(0x03);
    bmp.write(0x3D);
    bmp.write(0xE6);
    // Just some zeros
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    // Length of header; 36 in hex; 54 in decimal
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    bmp.write(0x36);
    // Length of remaining bytes; 40; 28 hex
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    bmp.write(0x28);
    // Width in Pixels; 240; F0 hex
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    bmp.write(0xF0);
    // Height in Pixels; 295; 01 27 hex
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    bmp.write(0x01);
    bmp.write(0x27);
    // Number of color planes; 1; 01 hex
    bmp.write((byte)0x00);
    bmp.write(0x01);
    // Bit depth; 24; 18 hex
    bmp.write((byte)0x00);
    bmp.write(0x18);
    // Compression disabled; 0; 00 hex
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    // Size of raw pixel data; 212400; 03 3D B0 hex
    bmp.write((byte)0x00);
    bmp.write(0x03);
    bmp.write(0x3D);
    bmp.write(0xB0);
    // Horizontal resolution; 2835; 0B 13 hex
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    bmp.write(0x0B);
    bmp.write(0x13);
    // Vertical resolution; 2835; 0B 13 hex
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    bmp.write(0x0B);
    bmp.write(0x13);
    // Number of colors; 0 (default); 00 hex
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    // Number of important colors; 0 (default); 00 hex
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    bmp.write((byte)0x00);
    // ACTUAL PIXEL DATA BEGINS HERE
    for (int y = 295; y >= 0; y--) {
      for (int x = 0; x <= 240; x++) {
        uint8_t *px;
        px = readPixel(x, y);
        if (y == 150) Serial.print(px[2] + px[1] + px[0]) ;
        bmp.write(px[2]);
        bmp.write(px[1]);
        bmp.write(px[0]);
      }
    }
    bmp.close();
  } else {
    int num = 0;
    File bmp = SD.open("saved0.bmp");
    File f2 = SD.open("NUMBER.TXT", FILE_WRITE);
    char *numstr = "";
    itoa(num, numstr, 10);
    f2.print(numstr);
    f2.close();
  }
}*/

uint16_t currentColor = ILI9341_WHITE;
uint16_t colors[7] = {ILI9341_RED, ILI9341_ORANGE, ILI9341_YELLOW, ILI9341_GREEN, ILI9341_BLUE, ILI9341_PURPLE, ILI9341_BLACK};

void setup() {
    // put your setup code here, to run once:
    Serial.begin(921600);
    tft.begin();
    tft.fillScreen(ILI9341_WHITE);

    for (int i = 0; i < 7; i++) {
        tft.fillRect(i * 25, 295, 25, 25, colors[i]);
    }
}

void loop() {
    // put your main code here, to run repeatedly:
    Point p = ts.getPoint();

    if (!(p.z > __PRESSURE)) {
        return;
    } else {
        int x = map(p.x, TS_MINX, TS_MAXX, 0, 240);
        int y = map(p.y, TS_MINY, TS_MAXY, 0, 320);
        Serial.print(F("Touch detected at X: "));
        Serial.print(x);
        Serial.print(F(" Y: "));
        Serial.println(y);
        if (y >= 295 and x <= 175) {
            // color touched
            currentColor = colors[x / 25];
        } else if (y < 295) {
            tft.drawPixel(x, y, currentColor);
        }
    }
}
