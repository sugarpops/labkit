/*
 * DACtest -- Try out the ESP32 DAC hardware
 *
 * Copyright (c) 2018 Ben Goldberg
 * MIT License (see LICENSE in this repository)
 *
 * CHANGES:
 *   5/15/18 aw yeah, the DAC works!! it's on GPIO25
 * TODO:
 *   
 * BUGS AND FEATURE REQUESTS:
 *   
 */


void setup() {
  // put your setup code here, to run once:
  
}

/*
 * Try connecting your oscilloscope to the DAC pin!
 */
uint8_t counter;
bool plus = true;
void count() {
  if (plus) {
    counter++;
  } else {
    counter--;
  }
}
void loop() {
  // put your main code here, to run repeatedly:
  if (counter >= 255) {
    plus = false;
  }
  if (counter <= 0) {
    plus = true;
  }
  count();
  dacWrite(DAC1, counter);
  delay(10);
}
