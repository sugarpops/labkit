/*
 * LabKitPaint.ino
 * Application that uses the display and touchscreen to draw stuff.
 * Copyright (c) 2017-2018 Ben Goldberg
 * MIT License
 */
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>
#include <SeeedTouchScreen.h>
#include <SPI.h>
#include <SD.h>

/* Below code copied from touchScreen.ino, an example from the Seeed Touch Screen library */

// Correct pins
#define YP A2
#define XM A1
#define YM 14
#define XP 17

// Calibrated!
#define TS_MINX 200
#define TS_MAXX 1768
#define TS_MINY 128
#define TS_MAXY 1740

TouchScreen ts = TouchScreen(XP, YP, XM, YM);

/* End of copied code */

/* Below code copied from spitftbitmap.ino, an example from the Adafruit ILI9341 Library */


/***************************************************
  This is our Bitmap drawing example for the Adafruit ILI9341 Breakout and Shield
  ----> http://www.adafruit.com/products/1651
  Check out the links above for our tutorials and wiring diagrams
  These displays use SPI to communicate, 4 or 5 pins are required to
  interface (RST is optional)
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!
  Written by Limor Fried/Ladyada for Adafruit Industries.
  MIT license, all text above must be included in any redistribution
 ****************************************************/

// TODO: Find out values in the LabKit for TFT_DC, TFT_CS, and SD_CS
#define TFT_DC 6
#define TFT_CS 5
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

#define SD_CS 4

uint16_t currentColor = ILI9341_WHITE;
uint16_t colors[7] = {ILI9341_RED, ILI9341_ORANGE, ILI9341_YELLOW, ILI9341_GREEN, ILI9341_BLUE, ILI9341_PURPLE, ILI9341_BLACK};

void setup() {
    // put your setup code here, to run once:
    Serial.begin(9600);
    tft.begin();
    tft.fillScreen(ILI9341_WHITE);

    for (int i = 0; i < 7; i++) {
        tft.fillRect(i * 25, 295, 25, 25, colors[i]);
    }
}

void loop() {
    static uint16_t count = 0;
    // put your main code here, to run repeatedly:
    Point p = ts.getPoint();

    if (!(p.z > __PRESSURE)) {
        return;
    } else {
        int x = map(p.x, TS_MINX, TS_MAXX, 0, 240);
        int y = map(p.y, TS_MINY, TS_MAXY, 0, 320);
        Serial.print(F("Touch detected at X: "));
        Serial.print(x);
        Serial.print(F(" Y: "));
        Serial.println(y);
        if (y >= 295 and x <= 175) {
            // color touched
            currentColor = colors[x / 25];
        } else if (y < 295) {
            tft.drawPixel(x, y, currentColor);
            count += 1;
            if (count > 200) {
                for (uint16_t x = 0; x < 240; x++) {
                    for (uint16_t y = 0; y < 320; y++) {
                        uint16_t v = tft.readPixel(x, y);
                        if (v != 0) {
                            Serial.print("x: ");
                            Serial.print(x);
                            Serial.print(" y: ");
                            Serial.print(y);
                            Serial.print(" c: ");
                            Serial.println(v, HEX);
                        }
                    }
                }
                Serial.println("finished");
                while (1);
            }
        }
    }
}
