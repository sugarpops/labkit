/*
 * GraphicalShell.ino -- A graphical shell for the Lab Kit.
 *
 * There are letters on the screen.
 * You touch the letters to spell commands, and then press the big green button to invoke them.
 * Kind of like with a mini OS.
 *
 * Specified Commands: ls, cd, cat, edit, mkdir, draw, rm
 * Commands In Beta: ls
 * Commands In Alpha: cd, draw
 *
 * Copyright (c) 2018 Ben Goldberg
 * MIT License (see LICENSE in this repository)
 * CHANGES:
 *   3/29/18 Establishing conventions
 * TODO:
 *   make all commands output to TFT
 * BUGS:
 *   Add . and / to letters so you can type more characters
 */

#include <SPI.h>
#include <SD.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>
#include <SeeedTouchScreen.h>

#define YP A2
#define XM A1
#define YM 14
#define XP 17
#define TS_MINX 116*2
#define TS_MAXX 890*2
#define TS_MINY 83*2
#define TS_MAXY 913*2

TouchScreen ts = TouchScreen(XP, YP, XM, YM);

/* Below code copied from spitftbitmap.ino, an example from the Adafruit ILI9341 Library */


/***************************************************
  This is our Bitmap drawing example for the Adafruit ILI9341 Breakout and Shield
  ----> http://www.adafruit.com/products/1651
  Check out the links above for our tutorials and wiring diagrams
  These displays use SPI to communicate, 4 or 5 pins are required to
  interface (RST is optional)
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!
  Written by Limor Fried/Ladyada for Adafruit Industries.
  MIT license, all text above must be included in any redistribution
 ****************************************************/

#define TFT_DC 6
#define TFT_CS 5
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

#define SD_CS 4

/* End of copied code */

const String cmds[] = {"ls", "cd", "cat", "edit", "mkdir", "draw", "rm"};
#define NUMBER_OF_COMMANDS 7
#define SD_CS 4
String currentDir = "/";

// ls = list files in current directory
void ls() {
    // set cursor to place where we output text for commands
    tft.setCursor(0, 100);
    tft.setTextColor(ILI9341_BLACK, ILI9341_WHITE);
    tft.setTextSize(1);
    // print files
    tft.println(F("Name\tSize"));
    File dir = SD.open(currentDir);
    dir.seek(0);
    while (1) {
        // if we're at the end, start back at start of output
        if (tft.getCursorY() >= 244) {
            delay(2500);
            tft.fillRect(0, 100, 240, 144, 0xFFFF);
            tft.setCursor(0, 100);
        }
        // go through files
        File f = dir.openNextFile();
        if (!f) {
            break;
        }
        // print their names/sizes
        tft.print(f.name());
        if (f.isDirectory()) {
            tft.println(F("\tDIRECTORY"));
        } else {
            tft.print(F("\t"));
            tft.println(f.size(), DEC);
        }
        f.close();
    }
    dir.seek(0);
    dir.close();
}

// cd = *c*hange *d*irectories
void cd(String arg) {
    char buf[32];
    currentDir.toCharArray(buf, 32);
    //convert arg to char[]
    // empty cd means cd /
    if (arg == "") {
        currentDir = "/";
        return;
    }
    // does currentDir + arg exist? If so, set currentDir to that
    if (SD.exists(currentDir + arg)) {
        currentDir = currentDir + arg;
    }
    // if not set currentDir to currentDir + "/" + arg
    else if (SD.exists(currentDir + "/" + arg)) {
        currentDir += "/" + arg;
    }
    // if that doesn't work currentDir = arg
    else {
        currentDir = arg;
    }
    // back 1 dir is ..
    if (arg == "..") {
        String path;
        int numSlash = 0;
        for (int i = 0; i < 32; i++) {
            if (buf[i] == '/') {
                numSlash++;
            }
        }
        for (int i = 0; i < numSlash - 2; i++) {
            if (i == 0) {
                path += strtok(buf, "/");
            } else {
                path += "/";
                path += strtok(NULL, "/");
            }
        }
        currentDir = "/";
        currentDir += path;
    }
    if (currentDir[currentDir.length() - 1] != '/') {
        currentDir += "/";
    }
    // spaces not needed
    currentDir.replace(" ", "");
    Serial.println("Changed directories to " + currentDir);
}

// cat = what does the file say?
void cat(String arg) {
    // this is pretty simple, just print file contents
    String fname = currentDir + arg;
    if (!(SD.exists(fname))) {
        Serial.println(F("Correct usage: cat <file name>"));
        return;
    } else {
        File f = SD.open(fname);
        while (f.available()) {
            Serial.write(f.read());
        }
        f.close();
    }
}

// edit = my own way of editing a file, kind of clumsy now ;-)
void edit(String arg) { // <backslash> to stop editing
    // print file contents
    File f = SD.open(currentDir + arg, FILE_WRITE);
    if (f.read()) {
        f.seek(0);
        while (f.available()) {
            tft.print((char)f.read());
        }
    }
    // go to beginning of file, start editing
    f.seek(0);
    while (1) {
        // listen for characters, if \ then save
        while (!(Serial.available()));
        char ch = Serial.read();
        if (ch == '\\') {
            f.close();
            return;
        } else {
            Serial.write(ch);
            f.write(ch);
        }
    }
}

// mkdir = MaKe DIRectory
void mkdir(String arg) {
    String path = currentDir + arg;
    SD.mkdir(path);
}
// draw .bmp file on TFT - probably won't work with graphical shell
void draw(String arg) {
    arg += ".bmp";
    // add .bmp to end of filename
    char argy[arg.length() + 1];
    arg.toCharArray(argy, arg.length() + 1);
    Serial.println(argy);
    if (SD.exists(argy)) {
        // if argument exists, draw it
        tft.fillScreen(ILI9341_WHITE);
        tft.bmpDraw((char *)argy, 0, 0);
    } else {
        // otherwise, prepend currentDir to arg and draw that filename
        String path = currentDir + argy;
        char argu[path.length() + 1];
        path.toCharArray(argu, path.length() + 1);
        if (SD.exists(argu)) {
            tft.fillScreen(ILI9341_WHITE);
            tft.bmpDraw((char *)argu, 0, 0);
        } else {
            Serial.println(F("File not found."));
        }
    }
}

// rm = ReMove file (capitalization to show how I think the cmd name got made up)
void rm(String arg) {
    String f = currentDir + arg;
    SD.remove(f);
}

// execFromSD will re-load the application running.
// Right now it just prints the file contents like cat
void execFromSD(String cmd) {
    // open file
    File f = SD.open(currentDir + cmd + ".HEX");
    // print its name
    Serial.println(String("Executing ") + f.name());
    // print file contents to Serial or "File not found"
    if (SD.exists(f.name())) {
        while (f.available()) {
            Serial.write(f.read()); // replace this with writing f.read() to flash
        }
    } else {
        Serial.println(F("File not found"));
    }
    f.close();
}

// execute command by #, simple switch
void execCMD(int cmdNum, String args) {
    switch (cmdNum) {
    case 0:
        ls();
        break;
    case 1:
        cd(args);
        break;
    case 2:
        cat(args);
        break;
    case 3:
        edit(args);
        break;
    case 4:
        mkdir(args);
        break;
    case 5:
        draw(args);
        break;
    case 6:
        rm(args);
        break;
    }
}

void printPrompt() {
    tft.setCursor(0, 0);
    tft.setTextColor(ILI9341_BLACK, ILI9341_WHITE);
    tft.setTextSize(2);
    tft.print(F("arduino@LabKit "));
    tft.print(currentDir);
    tft.print(F(" $"));
}

// this computes the letter
char touched(uint16_t w, uint16_t h) {
    // make variable for letter
    char al = ' ';
    int height;
    // if above 245 but below 100, then put a space
    if (h < 245 && height > 100) {
        return ' ';
    }
    // otherwise, round height
    else if (h >= 245 and h < 270) {
        height = 245;
    } else if (h >= 270 and h < 295) {
        height = 270;
    } else {
        height = 295;
    }
    // and do complicated math that I figured out on calculator
    // to find out letter.
    // once we find out letter, make it lowercase
    al = (char)(65 + (int((height - 245) / 2.777777777777778) + int(w / 25)));
    al = (char)tolower(al);
    if (al == '\\') {
        al = '/';
    }
    return al;
}

void setup() {
    // put your setup code here, to run once:
    tft.begin();
    tft.fillScreen(0x0000);
    Serial.begin(921600); // blazing fast for UART
    Serial.print(F("Initializing SD card"));
    while (!SD.begin(SD_CS)) {
        Serial.print(F("."));
    }
    Serial.println();
    Serial.println(F("Done!"));
    tft.fillScreen(0xFFFF);
    // bmpDraw wants a character array!
    // capital A in ASCII is 65 decimal
    for (uint8_t i = 0; i < 10; i++) { // we can only have 9 letters on one row
        char tmp[6] = " .bmp";
        tmp[0] = (char)('A' + i);
        tft.bmpDraw(tmp, i * 25, 245);
    }
    for (uint8_t i = 0; i < 10; i++) {
        char tmp[6] = " .bmp";
        tmp[0] = (char)('A' + 9 + i);
        tft.bmpDraw(tmp, i * 25, 270);
    }
    for (uint8_t i = 0; i < 8; i++) {
        char tmp[6] = " .bmp";
        tmp[0] = (char)('A' + 18 + i);
        tft.bmpDraw(tmp, i * 25, 295);
    }
    tft.fillRoundRect(0, 50, 240, 50, 10, ILI9341_GREEN);
    printPrompt();
}

String cmd = "";

void loop() {
    // put your main code here, to run repeatedly:
    Point p = ts.getPoint();

    if (!(p.z > __PRESSURE)) {
        return;
    } else {
        int x = map(p.x, TS_MINX, TS_MAXX, 0, 240);
        int y = map(p.y, TS_MINY, TS_MAXY, 0, 320);
        Serial.print(F("Touch detected at X: "));
        Serial.print(x);
        Serial.print(F(" Y: "));
        Serial.println(y);
        char ch = touched(x, y);
        tft.print(ch);
        cmd += (String)ch;
        Serial.println(ch);
        Serial.println(cmd);
        // if green button pressed
        if (y >= 50 && y <= 100) {
            // find & run command
            if (cmd != "") {
                Serial.println(cmd);
                bool cmdFound = false;
                for (int i = 0; i < NUMBER_OF_COMMANDS; i++) {
                    if (cmd.indexOf(cmds[i]) != -1) {
                        cmdFound = true;
                        String arg = cmd;
                        arg.replace(cmds[i] + " ", "");
                        execCMD(i, arg);
                    }
                }
                if (!cmdFound) {
                    execFromSD(cmd);
                }
                // then print prompt for new cmd
                printPrompt();
            }
            cmd = "";
        }
        delay(500);
    }
}
