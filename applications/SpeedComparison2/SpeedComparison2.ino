/*
 * SpeedComparison2.ino -- adds 75 floating-point numbers and prints the elapsed time.
 *
 * This program adds a ton of floating point numbers and prints out the
 * start time, end time, and elapsed time.
 * It was meant to compare the speeds of the Arduino and ESP32.
 *
 * Copyright (c) 2018 Ben Goldberg
 * MIT License (see LICENSE in this repository)
 *
 * CHANGES:
 *   5/2/2018 Initial
 * TODO:
 *
 * BUGS AND FEATURE REQUESTS:
 *
 */
float stuff[75] = {312.7, 416.1, 377.7, 174.5, 649.2, 515.0, 54.2, 964.3, 555.8, 521.9, 257.2, 287.7, 902.6, 504.4, 335.4, 67.0, 108.8, 942.1, 59.5, 184.5, 285.2, 445.5, 214.2, 763.4, 778.8, 725.3, 913.9, 805.1, 141.8, 561.2, 765.9, 425.9, 264.0, 910.3, 342.4, 820.2, 852.8, 656.9, 945.0, 669.5, 194.7, 243.4, 962.5, 182.2, 574.1, 20.5, 714.1, 204.9, 329.1, 879.3, 103.3, 345.9, 808.2, 284.1, 262.5, 734.6, 46.7, 320.3, 493.0, 110.6, 645.9, 714.7, 881.2, 481.7, 371.2, 472.4, 374.7, 263.0, 378.3, 430.3, 521.4, 645.0, 787.9, 772.2, 659.9};
// above numbers randomized using Python
float total;
void setup() {
    // put your setup code here, to run once:
    // initialize Serial connection
    Serial.begin(9600);
    Serial.println("Connected over USB");
    // start the "stopwatch"
    unsigned long start = millis();
    for (int i = 0; i < 10000; i++) {
        total = 0;
        for (float f : stuff) {
            // tick tock tick tock
            // adding all of the #s
            total += f;
        }
    }
    // and... stop!
    unsigned long end = millis();
    // print our friendly old statistics
    Serial.println("Float Adding Finished!! Stats:");
    Serial.println(String("Start: ") + start + " ms");
    Serial.println(String("End: ") + end + " ms");
    Serial.println(String("Duration: ") + (end - start) + " ms");
    Serial.println("G'day mate!");
}

void loop() {
    // put your main code here, to run repeatedly:

}
