/*
 * Form.ino
 *
 * This program loads a form off of the SD card (with questions) and you have to answer the questions.
 * The question filenames end in .lkq, and the response filenames end in .lkr.
 * It will ask you to select a form (<filename>.lkq) and then it will show questions.
 * Press the buttons 1, 2, 3, and 4 to answer.
 * It will save your response as a .lkr file.
 *
 * Copyright (c) 2017-2018 Ben Goldberg
 * MIT License (see LICENSE in this repository)
 * CHANGES:
 *   4/3/18 Updated header comment
 * TODO:
 *
 * BUGS:
 *
 */
#include <SPI.h>
#include <SD.h>
#include <Adafruit_GFX.h>
#include <SeeedTouchScreen.h>
#include <Adafruit_ILI9341.h>
#include <LabKit.h>

#define YP A2
#define XM A1
#define YM 14
#define XP 17

//Measured ADC values for (0,0) and (210-1,320-1)
//TS_MINX corresponds to ADC value when X = 0
//TS_MINY corresponds to ADC value when Y = 0
//TS_MAXX corresponds to ADC value when X = 240 -1
//TS_MAXY corresponds to ADC value when Y = 320 -1

#define TS_MINX 116*2
#define TS_MAXX 890*2
#define TS_MINY 83*2
#define TS_MAXY 913*2

TouchScreen ts = TouchScreen(XP, YP, XM, YM);

#define TFT_DC 6
#define TFT_CS 5
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

#define SD_CS 4

char *lkqFilename;

int pollButtons() {
    int btns[5] = {0, 2, 3, 7, 8};
    while (1) {
        if (digitalRead(2) == LOW) {
            return 1;
        }
        if (digitalRead(3) == LOW) {
            return 2;
        }
        if (digitalRead(7) == LOW) {
            return 3;
        }
        if (digitalRead(8) == LOW) {
            return 4;
        }
    }
}

bool listenForTouches() {
    // 60x16 px for "Select" in font size 2
    Point p = ts.getPoint();
    while (p.z < __PRESSURE) {
        p = ts.getPoint();
    }
    int x = map(p.x, TS_MINX, TS_MAXX, 0, 240);
    int y = map(p.y, TS_MINY, TS_MAXY, 0, 320);
    if ((x >= 120 and x <= 180) and (y >= 160 and y <= 176)) {
        return true;
    } else {
        return false;
    }
}

void displayFiles(File directory) {
    bool fileFound = false;
    while (1) {
        File next = directory.openNextFile();
        if (! next) {
            next.close();
            directory.close();
            tft.fillScreen(0xFFFF);
            tft.setTextColor(ILI9341_RED);
            tft.setTextSize(3);
            tft.setCursor(0, 0);
            tft.println("ERROR: NO MORE LKQ FILES. Resetting...");
            delay(5000); // give them time to read error
            asm volatile ("jmp  0"); // software reset :)
        }
        Serial.println(next.name());
        if (String(next.name()).indexOf(".LKQ") != -1) {
            fileFound = true;
            tft.fillScreen(ILI9341_WHITE);
            tft.setCursor(0, 0);
            tft.setTextSize(2);
            tft.setTextColor(ILI9341_BLACK);
            tft.println(next.name());

            tft.setCursor(120, 160);
            tft.setTextSize(2);
            tft.println("Select");
            bool touched = listenForTouches();
            if (touched) {
                lkqFilename = next.name();
                return;
            }
        }
        next.close();
    }
    if (!fileFound) {
        tft.fillScreen(ILI9341_WHITE);
        tft.setCursor(0, 0);
        tft.setTextSize(2);
        tft.setTextColor(ILI9341_BLACK);
        tft.println("No .lkq files found on SD card.");
        tft.println("If you have a .lkq file on the SD card, it probably doesn't have an 8.3 filename.");
    }
}

void showQuestionsForm() {
    File f = SD.open(lkqFilename);
    String res = lkqFilename;
    res.replace(".LKQ", ".LKR");
    uint8_t counter = 0;
    while (SD.exists(res)) {
        counter++;
        res = lkqFilename;
        res.replace(".LKQ", String(counter) + ".LKR");
    }
    tft.setCursor(0, 0);
    tft.setTextSize(2);
    tft.setTextColor(ILI9341_BLACK, ILI9341_WHITE);
    tft.print("Response filename: ");
    tft.println(res);
    delay(3000);
    uint8_t numberOfQuestions = 0;
    while (f.available()) {
        f.read() == '/' ? numberOfQuestions++ : numberOfQuestions += 0;
    }
    f.seek(1);
    for (int i = 0; i < numberOfQuestions; i++) {
        tft.fillScreen(ILI9341_WHITE);
        f.seek(f.position() - 1);
        if (f.read() != '/') {
            Serial.println("Invalid .lkq file");
            return;
        } else {
            String question = "";
            char ch = ' ';
            while (ch != ':') {
                question += ch;
                ch = f.read();
            }
            ch = ' ';
            tft.setCursor(0, 0);
            tft.setTextSize(2);
            tft.println(question);
            String answer = "";
            while (ch != ':') {
                answer += ch;
                ch = f.read();
            }
            ch = ' ';
            tft.println("1: ");
            tft.println(answer);
            answer = "";
            while (ch != ':') {
                answer += ch;
                ch = f.read();
            }
            ch = ' ';
            tft.println("2: ");
            tft.println(answer);
            answer = "";
            while (ch != ':') {
                answer += ch;
                ch = f.read();
            }
            ch = ' ';
            tft.println("3: ");
            tft.println(answer);
            answer = "";
            while (ch != '/') {
                answer += ch;
                ch = f.read();
            }
            tft.println("4: ");
            tft.println(answer);
            answer = "";
            int answered = pollButtons();
            Serial.println(answered);
            File response = SD.open(res, FILE_WRITE);
            Serial.println(response.name());
            response.write('/');
            response.print(question);
            response.write(':');
            response.print(String(answered));
            response.close();
        }
    }
    f.close();
}

void setup() {
    // put your setup code here, to run once:
    LabKit.begin();
    tft.begin();

    Serial.print("Initializing SD card");
    while (!SD.begin(SD_CS)) {
        Serial.print(".");
    }
    Serial.println();
    Serial.println("Done!");
    pinMode(2, INPUT_PULLUP);
    pinMode(3, INPUT_PULLUP);
    pinMode(7, INPUT_PULLUP);
    pinMode(8, INPUT_PULLUP);
    File root = SD.open("/");
    displayFiles(root);
    showQuestionsForm();
    root.close();
}

void loop() {
    // put your main code here, to run repeatedly:

}
