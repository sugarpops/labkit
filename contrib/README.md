## Source dependencies outside the project

###These library repos are from outside sources such as independent projects and vendor libraries. 

*  [SimplePinChange - pin change interrupt support](https://github.com/petesoper/SimplePinChange)
*  [M41T6X - ST clock/calendar chip library](https://github.com/petesoper/M41T6X)
*  [Adafruit_ILI9341 - MIT-licensed display library (forked)](https://bitbucket.com/sugarpops/Adafruit_ILI9341)
*  [Seeed Studio TFT V2 - ILI9341 library](https://github.com/Seeed-Studio/TFT_Touch_Shield_V2)
*  [Touch Screen Driver (fork of Seeed Studio code)](https://bitbucket.com/sugarpops/Touch_Screen_Driver)
* [avr_utilities: AVR pin manipulation - MIT-like licence](https://github.com/DannyHavenith/avr_utilities)

###These libraries are GPL. The GFX library usage is temporary

* [Adafruit GFX Library (GPL-licensed)](https://github.com/adafruit/Adafruit-GFX-Library)

### Additional resources

* [Linux/Cygwin Makefile](https://github.com/sudar/Arduino-Makefile)
* [AVR Fuse Calculator](http://www.engbedded.com/fusecalc/)
* [AVR libc project - modified BSD](http://savannah.nongnu.org/projects/avr-libc/)
* [AVR disassembler (some funky formatting issues, but quite usable) - GPL3](https://github.com/vsergeev/vavrdisasm)
* [Arduino makefile (works very well with Linux and Cygwin) - GPL2.1](https://github.com/sudar/Arduino-Makefile)
* [AVR self-programming App Note 109 - READ THIS FIRST](http://www.atmel.com/images/doc1644.pdf)
* [AVR C functions for flash App Note 106](http://www.atmel.com/images/atmel-2575-c-functions-for-reading-and-writing-to-flash-memory_applicationnote_avr106.pdf)
* [AVR Open Source Programmer App Note 911](http://www.atmel.com/images/doc2568.pdf)
* [Atmega328P Datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/40001906A.pdf)
* [AVR Instruction Set](http://www.atmel.com/images/Atmel-0856-AVR-Instruction-Set-Manual.pdf)
* [AVR Libc Reference Manaual ](http://www.atmel.com/webdoc/avrlibcreferencemanual/)
* [AVR GCC](https://gcc.gnu.org/wiki/avr-gcc)
* [Atmel AVR libc ref inline asm cookbook](http://www.atmel.com/webdoc/avrlibcreferencemanual/inline_asm.html)
* [Yourduino RoboRED Uno compatible](http://www.yourduino.com/sunshop/index.php?l=product_detail&p=429)
* [Seeed wiki for V2 touch screen display](http://wiki.seeed.cc/2.8inch_TFT_Touch_Shield_v2.0/)
* [Display schematic](https://github.com/SeeedDocument/TFT_Touch_Shield_V2/raw/master/resources/TFT%20Touch.pdf)
* [Display PCB](https://github.com/SeeedDocument/TFT_Touch_Shield_V2/raw/master/resources/TFT%20Touch%20PCB.pdf)

