/*
 * Duke-UNC.ino
 *
 * Which team do you like best? Duke... or UNC?
 * Click the screen and it will tell you its opinion!
 *
 * Copyright (c) 2017-2018 Ben & Steve Goldberg
 * MIT License (see LICENSE in this repository)
 * CHANGES:
 *   4/3/18 Updated header comment
 * TODO:
 *
 * BUGS:
 *
 */

#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>
#include <SeeedTouchScreen.h>
#include <SPI.h>
#include <SD.h>

/* Below code copied from touchScreen.ino, an example from the Seeed Touch Screen library */

/* Here's the license for that library:

  The MIT License (MIT)

  Copyright (c) 2013 Seeed Technology Inc.

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

// Correct pins
#define YP A2
#define XM A1
#define YM 14
#define XP 17

// Calibrated!
#define TS_MINX 200
#define TS_MAXX 1768
#define TS_MINY 128
#define TS_MAXY 1740

TouchScreen ts = TouchScreen(XP, YP, XM, YM);

/* End of copied code */

/* Below code copied from spitftbitmap.ino, an example from the Adafruit ILI9341 Library */


/***************************************************
  This is our Bitmap drawing example for the Adafruit ILI9341 Breakout and Shield
  ----> http://www.adafruit.com/products/1651
  Check out the links above for our tutorials and wiring diagrams
  These displays use SPI to communicate, 4 or 5 pins are required to
  interface (RST is optional)
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!
  Written by Limor Fried/Ladyada for Adafruit Industries.
  MIT license, all text above must be included in any redistribution
 ****************************************************/

// TODO: Find out values in the LabKit for TFT_DC, TFT_CS, and SD_CS
#define TFT_DC 6
#define TFT_CS 5
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

#define SD_CS 4

/* End of copied code */

/* Setting initial variable values to false, because ain't nobody touched nothin' yet */

bool dukeClick = false;
bool uncClick = false;
bool middleClick = false;
bool initialized = false;

void setup() {
    // put your setup code here, to run once:
    Serial.begin(921600);

    tft.begin();

    if (!SD.begin(SD_CS)) {
        Serial.println(F("Could not initialize SD card. Are you sure you have it plugged in and formatted correctly?"));
    } else {
        initialized = true;
        Serial.println(F("Initialized SD card and ready to rock and roll!"));
    }
    tft.bmpDraw("DukeUNC0.bmp", 0, 0);

}

void loop() {
    // put your main code here, to run repeatedly:
    Point p = ts.getPoint();

    if (!(p.z > __PRESSURE)) { // This misspelling isn't my fault, it's in the touchscreen library! I have to use that variable name!
        return;
    } else {
        int x = map(p.x, TS_MINX, TS_MAXX, 0, 240);
        int y = map(p.y, TS_MINY, TS_MAXY, 0, 320);
        Serial.print(F("Touch detected at X: "));
        Serial.print(x);
        Serial.print(F(" Y: "));
        Serial.println(y);
        // Do stuff with touch coords:
        if (y < 50) {
            dukeClick = true;
        } else if (y > 275) {
            uncClick = true;
        } else {
            middleClick = true;
        }
        if ((dukeClick && uncClick) || middleClick) {
            tft.bmpDraw("DukeUNC3.bmp", 0, 0);
        } else if (dukeClick && !uncClick) {
            tft.bmpDraw("DukeUNC2.bmp", 0, 0);
        } else if (uncClick && !dukeClick) {
            tft.bmpDraw("DukeUNC1.bmp", 0, 0);
        }
    }
    yield();
}
