/*
   Capture and display touch screen data. A periodic interrupt samples the
   touchscreen Z axis measurement and captures NUM_SAMPLES Z values each time
   a press is detected.  A simple state machine controls the interrupt handler.
   The handler signals to the mainline code when a new set of samples are
   available to print out.

   Copyright (c) 2017 Pete Soper
   MIT License (see LICENSE.txt in this repo)
*/

#include <SPI.h>
#include <SD.h>
#include <LabKit.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>
#include <NRingBuffer.h>
#include <Task.h>
#include <Display.h>
#include <SeeedTouchScreen.h>
#include <MsTimer2.h>

const int16_t THRESHOLD = 0;          // Minimum Z value to call a "touch"

const uint32_t MILLISECONDS_PER_TICK = 10;// Interval between tick interrupts

const uint8_t IDLE = 0;           // Waiting for press state
const uint8_t CAPTURE = 1;        // Gathering press samples
const uint8_t WAITING = 2;        // Waiting for samples to be taken

const uint8_t NUM_SAMPLES = 40;       // Size of sample buffer
const uint8_t MAX_ROWS = 10;              // Max sample rows to print

// Touchscreen interface object is declared in Display library

// Shared between tick interrupt handler and mainline code

static volatile bool triggered = false;   // True when samples available
static uint16_t samples[NUM_SAMPLES];     // Sample buffer

/*
 * Periodic interrupt handler called very MILLISECONDS_PER_TICK.
 * Use a simple state machine to wait until minimal touchscreen press is
 * detected, then gather a set of samples and signal their availability with
 * shared variable "triggered". Then wait until the mainline code sees and
 * handles the samples and then resets trigger.
 */

void tick() {
    static uint8_t sample_index;      // Next sample buffer index
    static uint8_t state = IDLE;      // Current state

    Point p = ts.getPoint();      // Fetch touchscreen x, y, z values

    if (state == IDLE) {          // Waiting for first press?
        if (p.z > THRESHOLD) {      // Yes, check Z: high enough?
            sample_index = 0;         // Yes, start sample index
            state = CAPTURE;          // Set state and FALL THROUGH
        }
    }

    if (state == CAPTURE) {       // Capturing samples?
        samples[sample_index] = p.z;    // Yes, save current Z
        sample_index += 1;
        if (sample_index == NUM_SAMPLES) {  // Buffer full?
            triggered = true;         // Yes: signal mainline code
            state = WAITING;
        }
    } else if (state == WAITING) {
        if (! triggered) {          // Samples consumed yet?
            state = IDLE;         // Yes, look for new press
        }
    }
}

/*
 * Set up the hardware
 */

void setup() {
    LabKit.begin(9600);           // Initialize the lab kit hardware

    MsTimer2::set(MILLISECONDS_PER_TICK, tick);// Periodic timer interrupt
    MsTimer2::start();
}

/*
 * Look for available samples and print them when found, omitting trailing
 * zero samples.
 */

void loop() {
    if (triggered) {          // Set by interrupt handler
        Serial.println("Touch:");
        uint8_t last_sample = NUM_SAMPLES - 1;// Remove zeros from buffer end
        do {
            if (samples[last_sample] != 0) {
                break;
            }
            last_sample = last_sample - 1;
        } while (last_sample > 0);

        uint8_t sample_count = last_sample + 1; // Figure out print format
        uint8_t rows = sample_count >= MAX_ROWS ? MAX_ROWS : sample_count;
        uint8_t columns = sample_count / MAX_ROWS;

        if (sample_count % MAX_ROWS) {     // Bump if NOT multiple of MAX_ROWS
            columns += 1;
        }

        /* For each row of samples print values for columns necessary to
         * complete the row or exhaust the sample values using column major order.
         */

        for (uint8_t row = 0; row < rows; row++) {
            uint8_t index = 0;
            for (uint8_t column = 0; column < columns; column++) {
                index = (rows * column) + row;
                if (index > last_sample) {  // This handles last column not full
                    break;
                }
                Serial.print(index);
                Serial.print(" ");
                Serial.print(samples[index]);
                Serial.print("\t\t");
            }
            Serial.println();
        }

        triggered = false;          // Tell handler to get new samples
    } // if triggered
}
