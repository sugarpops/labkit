/*
   NumberGuessingEventLoop.ino
   Game for the LabKit where the Arduino tries to guess your number.
   Have fun!
   Copyright (c) 2018 Ben Goldberg
   Copy of NumberGuessing.ino but modified  by Pete Soper to use
   registerForTouch.
*/
#include <SPI.h>
#include <SD.h>
#include <SeeedTouchScreen.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>
#include <Task.h>
#include <NRingBuffer.h>
#include <MsTimer2.h>
#include <Display.h>

uint16_t displayNumber = 500;
uint16_t lower = 0;
uint16_t upper = 1000;
uint8_t guesses = 1;

void touched(uint8_t x, uint16_t y) {
    if (y >= 120 && y <= 160) {
        // button pressed.
        guesses += 1;
        if (x > 125) {
            upper = displayNumber;
        } else {
            lower = displayNumber;
        }
        displayNumber = lower + (upper - lower) / 2;
        tft.setCursor(0, 60);
        tft.println(String("Greater than or lessthan ") + String(displayNumber) + "?");
    } else if (y >= 170 && y <= 210) {
        // equal
        tft.fillScreen(0xFFFF);
        tft.setCursor(0, 0);
        // no spaces 'cause of text wrapping
        tft.println("Your number was " + String(displayNumber) + ".I guessed your number in " + String(guesses) + " guesses. Havea good day!");
        while (1);
    }
}

// I AM THE COOKIE MONSTER. COOOOOKEEEES!

void cookieMonster() {
    uint16_t x = random(240);
    uint16_t y = random(320);
    // Munch!
    tft.drawPixel(x, y, 0xFFFF);
}

void setup() {
    Serial.begin(9600);
    // put your setup code here, to run once:
    randomSeed(analogRead(A0));
    Display::begin();
    // Make screen white, set up text
    tft.fillScreen(0xFFFF);
    tft.setTextColor(0x0000, 0xFFFF);
    tft.setTextSize(2);
    tft.setCursor(0, 0);
    tft.println("Think of a number from 0 to 999. I'll try to guess it.");
    tft.setCursor(0, 60);
    // No space between less and than because of text wrapping
    tft.println(String("Greater than or lessthan ") + String(displayNumber) + "?");
    // Draw greater than or less than buttons (NOTE: y0 = 120)
    tft.fillRoundRect(5, 120, 110, 40, 7, ILI9341_GREEN);
    tft.fillRoundRect(125, 120, 110, 40, 7, ILI9341_GREEN);
    tft.fillRoundRect(65, 170, 110, 40, 7, ILI9341_GREEN);
    // make text bigger and change color
    tft.setTextSize(3);
    tft.setTextColor(0xFFFF);
    // set to correct position and print > and <
    tft.setCursor(50, 128);
    tft.print(">");
    tft.setCursor(170, 128);
    tft.print("<");
    tft.setCursor(110, 178);
    tft.print("=");
    // clean up
    tft.setTextSize(2);
    tft.setTextColor(0x0000, 0xFFFF);
    // Ask the Task library to invoke touched each time a touch is detected.
    // The screen is sampled 50 times per second. The millisecond parameter
    // can be adjusted to find the happy medium between getting multiple touches
    // or missing a touch. The 200ms number is just a starting point.
    Task::begin();
    Display::registerForTouch(touched, 200);
    uint8_t taskid = Task::NO_TASKID;
    // Schedule a task to run every 500 times a second to eat the screen up!
    Task::scheduleTask(cookieMonster, 2, true, taskid);
    // Control never returns from run(). Instead, touched is called every time
    // a touch is detected and the, um, display consumption is called 500
    // times per second.
    Task::run();
}

void loop() {
}
