/*
 * Hangman.ino -- Play a two-player version of game Hangman on your LabKit touchscreen!
 *
 * Player 1 types a word in on the touchscreen using the letters.
 * When Player 1 is done, they should tap somewhere that's not a letter to tell the program that that's the end of their word.
 * After that, it's Player 2's turn.
 * Player 2 taps letters to guess the word. If Player 2 gets it correct, GOOD JOB! If they don't, Player 2 gets a Pikachu.
 *
 * Copyright (c) 2017-2018 Ben Goldberg
 * MIT License (see LICENSE in this repository).
 * CHANGES:
 *   4/3/18 Updated header comment
 * TODO:
 *
 * BUGS:
 *   Add delete button in case you mistype a letter
 */
// library includes, and yes, I do need 5 libraries.
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>
#include <SeeedTouchScreen.h>
#include <SPI.h>
#include <SD.h> // SD library needs 8.3 filenames

/* Below code copied from touchScreen.ino, an example from the Seeed Touch Screen library */

/* Here's the license for that library:

  The MIT License (MIT)

  Copyright (c) 2013 Seeed Technology Inc.

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

// Correct pins
#define YP A2
#define XM A1
#define YM 14
#define XP 17

//Measured ADC values for (0,0) and (210-1,320-1)
//TS_MINX corresponds to ADC value when X = 0
//TS_MINY corresponds to ADC value when Y = 0
//TS_MAXX corresponds to ADC value when X = 240 -1
//TS_MAXY corresponds to ADC value when Y = 320 -1

#define TS_MINX 116*2
#define TS_MAXX 890*2
#define TS_MINY 83*2
#define TS_MAXY 913*2

TouchScreen ts = TouchScreen(XP, YP, XM, YM);

/* End of copied code */

/* Below code copied from spitftbitmap.ino, an example from the Adafruit ILI9341 Library */


/***************************************************
  This is our Bitmap drawing example for the Adafruit ILI9341 Breakout and Shield
  ----> http://www.adafruit.com/products/1651
  Check out the links above for our tutorials and wiring diagrams
  These displays use SPI to communicate, 4 or 5 pins are required to
  interface (RST is optional)
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!
  Written by Limor Fried/Ladyada for Adafruit Industries.
  MIT license, all text above must be included in any redistribution
 ****************************************************/

#define TFT_DC 6
#define TFT_CS 5
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

#define SD_CS 4

/* End of copied code */

bool initialized = false; // Is SD card initialized?
String hangmanWord; // The word to guess
String correct; // The guesses
const char alphabet[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'}; // Letters of the alphabet
uint8_t currentHangmanGraphics = 0; // Which HangmanGraphics file we are on
bool wordCompleted = false; // Did Player 1 finish entering the word?
// File names for the pictures of the letters
const char *alphabetFiles[] = {"A.bmp", "B.bmp", "C.bmp", "D.bmp", "E.bmp", "F.bmp", "G.bmp", "H.bmp", "I.bmp", "J.bmp", "K.bmp", "L.bmp", "M.bmp", "N.bmp", "O.bmp", "P.bmp", "Q.bmp", "R.bmp", "S.bmp", "T.bmp", "U.bmp", "V.bmp", "W.bmp", "X.bmp", "Y.bmp", "Z.bmp"};

void touched(int w, int h) { // This function is called when the touchscreen is touched.
    char al = ' ';
    int height;
    if (h >= 245 and h < 270) {
        height = 245;
    } else if (h >= 270 and h < 295) {
        height = 270;
    } else {
        height = 295;
    }
    al = alphabet[int((height - 245) / 2.777777777777778) + int(w / 25)];
    Serial.println(al);
    Serial.println(int((height - 245) / 2.777777777777778) + int(w / 25));
    if (wordCompleted and h >= 245 and h <= 320 and w >= 0 and w <= 240 and isAlpha(al)) { // If the touch is for Player 2 and is a letter
        /*switch (height) {
          case 245:
            al = alphabet[int(w / 25)];
            break;
          case 270:
            al = alphabet[9 + int(w / 25)];
            break;
          case 295:
            al = alphabet[18 + int(w / 25)];
            break;
          }*/
        if (hangmanWord.indexOf(al) != -1) {
            tone(9, 1000, 500);
            tft.fillRect(int(w / 25) * 25, height, 25, 25, ILI9341_WHITE); // The letter is in the word; blank it out so Player 2 doesn't guess it again
        } else {
            tone(9, 100, 500);
            tft.fillRect(int(w / 25) * 25, height, 25, 25, ILI9341_RED); // The letter is not in the word; color it red so Player 2 doesn't guess it again
        }
        int index = hangmanWord.indexOf(al);
        if (index != -1) { // If your guess is in the word
            for (byte i = 0; i < hangmanWord.length(); i++) {
                if (hangmanWord[i] == al) {
                    Serial.println(al);
                    correct[i] = al;
                    Serial.println(correct);
                }
            }
            if ((correct.length() == hangmanWord.length()) and (hangmanWord.indexOf(correct) != -1)) {
                tone(9, 3000, 1000);
                tft.fillScreen(ILI9341_WHITE);
                tft.bmpDraw("correct.bmp", 0, 0);
                Serial.println(F("Amazing job!!!! You guessed the word!!!! Press reset to play again."));
            } else if (currentHangmanGraphics >= 7) {
                tone(9, 75, 500);
                tone(9, 31, 500);
                Serial.println(F("Look at the touchscreen for encouragement. I'm sorry you lost."));
                delay(1500); // wait 1.5sec
                tft.bmpDraw("pikachu.bmp", 0, 0); // Display an encouraging Pikachu
                Serial.println(F("Press reset to play again. I hope you had fun playing!"));
                while (1); // and do nothing
            }
        } else {
            currentHangmanGraphics += 1;
            char buf[11];
            (String("Image") + String(currentHangmanGraphics) + String(".bmp")).toCharArray(buf, 11);
            tft.bmpDraw(buf, 0, 0);
            if (currentHangmanGraphics >= 7) {
                tone(9, 75, 500);
                tone(9, 31, 500);
                Serial.println(F("Look at the touchscreen for encouragement. I'm sorry you lost."));
                delay(1500); // wait 1.5 seconds
                tft.bmpDraw("pikachu.bmp", 0, 0); // Display an encouraging Pikachu
                tft.setCursor(0, 259);
                tft.setTextColor(ILI9341_BLUE, ILI9341_WHITE);
                tft.setTextSize(2);
                tft.print("The word was: ");
                tft.println(hangmanWord);
                Serial.println(F("Press reset to play again. I hope you had fun playing!"));
                while (1); // and do nothing
            }
        }
    } else if (not h >= 245) {
        Serial.println(F("Please touch a letter."));
    } else if (!wordCompleted) { // If the touch is for Player 1
        if (h >= 245 and h <= 320 and w >= 0 and w <= 240 and isAlpha(al)) { // A letter was touched
            hangmanWord += al;
            Serial.println(al);
        } else { // A letter wasn't touched, the word is completed
            Serial.println(hangmanWord);
            for (int i = 0; i < hangmanWord.length(); i++) {
                correct += '-';
            }
            Serial.println(F("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"));
            Serial.println(F("Player 2, it's your turn!"));
            wordCompleted = true;
        }
    }
    tft.setCursor(0, 146);
    tft.setTextSize(2);
    tft.setTextColor(ILI9341_BLACK, ILI9341_WHITE);
    tft.print("Your guesses: ");
    tft.println(correct);
    Serial.println(correct);
}

void setup() {
    // put your setup code here, to run once:
    Serial.begin(9600);

    pinMode(9, OUTPUT);

    tft.begin();

    if (!SD.begin(SD_CS)) {
        Serial.println(F("Could not initialize SD card. Are you sure you have it plugged in and formatted correctly?"));
        Serial.println(F("program stopped"));
        while (true) {
        }
    } else {
        initialized = true;
        Serial.println(F("Initialized SD card and ready to rock and roll!"));
    }
    tft.fillScreen(0xFFFF); // make the screen white
    tft.bmpDraw("Image0.bmp", 0, 0); // display the first image
    // Display the letters on the screen
    for (uint8_t i = 0; i < 10; i++) { // we can only have 9 letters on one row
        tft.bmpDraw(alphabetFiles[i], i * 25, 245);
    }
    for (uint8_t i = 0; i < 10; i++) {
        tft.bmpDraw(alphabetFiles[9 + i], i * 25, 270);
    }
    for (uint8_t i = 0; i < 8; i++) {
        tft.bmpDraw(alphabetFiles[18 + i], i * 25, 295);
    }

    tft.setCursor(0, 146);
    tft.setTextSize(3);
    tft.setTextColor(ILI9341_BLACK);

}

void loop() {
    // put your main code here, to run repeatedly:
    Point p = ts.getPoint();

    if (!(p.z > __PRESSURE)) { // This misspelling isn't my fault, it's in the touchscreen library! I have to use that variable name!
        return;
    } else {
        int x = map(p.x, TS_MINX, TS_MAXX, 0, 240);
        int y = map(p.y, TS_MINY, TS_MAXY, 0, 320);
        Serial.print(F("Touch detected at X: "));
        Serial.print(x);
        Serial.print(F(" Y: "));
        Serial.println(y);
        touched(x, y);
        delay(500);
    }
    yield();
}
