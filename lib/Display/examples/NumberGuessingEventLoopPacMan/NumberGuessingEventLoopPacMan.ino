/*
 * NumberGuessingEventLoop.ino -- Number guessing game on the LabKit.
 *
 * Game for the LabKit where the Arduino tries to guess your number.
 * Have fun!
 * Except there's a twist: Pac-Man eating up the screen.
 * Copy of NumberGuessingEventLoop, changed by Ben Goldberg to have a Pac-Man running around. =D
 * Copyright (c) 2018 Ben Goldberg
 * MIT License (see LICENSE in this repository)
 * CHANGES:
 *   4/3/18 Updated header comment
 * TODO:
 *
 * BUGS:
 *
 */
#include <SPI.h>
#include <SD.h>
#include <SeeedTouchScreen.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>
#include <Task.h>
#include <NRingBuffer.h>
#include <MsTimer2.h>
#include <Display.h>

uint16_t displayNumber = 500;
uint16_t lower = 0;
uint16_t upper = 1000;
uint8_t guesses = 1;

void touched(uint8_t x, uint16_t y) {
    if (y >= 120 && y <= 160) {
        // button pressed.
        guesses += 1;
        if (x > 125) {
            upper = displayNumber;
        } else {
            lower = displayNumber;
        }
        displayNumber = lower + (upper - lower) / 2;
        tft.setCursor(0, 60);
        tft.println(String("Greater than or lessthan ") + String(displayNumber) + "?");
    } else if (y >= 170 && y <= 210) {
        // equal
        tft.fillScreen(0xFFFF);
        tft.setCursor(0, 0);
        // no spaces 'cause of text wrapping
        tft.println("Your number was " + String(displayNumber) + ".I guessed your number in " + String(guesses) + " guesses. Havea good day!");
        while (1);
    }
}

// I AM THE COOKIE MONSTER. COOOOOKEEEES!

void cookieMonster() {
    uint16_t x = random(240);
    uint16_t y = random(320);
    // Munch!
    tft.drawPixel(x, y, 0xFFFF);
}

// I like to eat pixels. Give me pixels. My name is Pac-Man:

void pacMan() {
    uint16_t x = random(240);
    uint16_t y = random(320);
    for (uint16_t i = y + 5; i < y + 13; i++) {
        tft.drawPixel(x, i, ILI9341_YELLOW);
    }
    for (uint16_t i = y + 4; i < y + 14; i++) {
        tft.drawPixel(x + 1, i, ILI9341_YELLOW);
    }
    tft.drawPixel(x + 2, y + 3, ILI9341_YELLOW);
    tft.drawPixel(x + 2, y + 4, ILI9341_YELLOW);
    tft.drawPixel(x + 2, y + 5, ILI9341_YELLOW);
    tft.drawPixel(x + 2, y + 13, ILI9341_YELLOW);
    tft.drawPixel(x + 2, y + 14, ILI9341_YELLOW);
    tft.drawPixel(x + 3, y + 2, ILI9341_YELLOW);
    tft.drawPixel(x + 3, y + 3, ILI9341_YELLOW);
    tft.drawPixel(x + 3, y + 4, ILI9341_YELLOW);
    tft.drawPixel(x + 3, y + 14, ILI9341_YELLOW);
    tft.drawPixel(x + 3, y + 15, ILI9341_YELLOW);
    tft.drawPixel(x + 4, y + 1, ILI9341_YELLOW);
    tft.drawPixel(x + 4, y + 2, ILI9341_YELLOW);
    tft.drawPixel(x + 4, y + 14, ILI9341_YELLOW);
    tft.drawPixel(x + 4, y + 15, ILI9341_YELLOW);
    tft.drawPixel(x + 5, y + 0, ILI9341_YELLOW);
    tft.drawPixel(x + 5, y + 1, ILI9341_YELLOW);
    tft.drawPixel(x + 5, y + 14, ILI9341_YELLOW);
    tft.drawPixel(x + 5, y + 15, ILI9341_YELLOW);
    tft.drawPixel(x + 6, y + 0, ILI9341_YELLOW);
    tft.drawPixel(x + 6, y + 1, ILI9341_YELLOW);
    tft.drawPixel(x + 6, y + 14, ILI9341_YELLOW);
    tft.drawPixel(x + 6, y + 15, ILI9341_YELLOW);
    tft.drawPixel(x + 7, y + 0, ILI9341_YELLOW);
    tft.drawPixel(x + 7, y + 1, ILI9341_YELLOW);
    tft.drawPixel(x + 7, y + 14, ILI9341_YELLOW);
    tft.drawPixel(x + 7, y + 15, ILI9341_YELLOW);
    tft.drawPixel(x + 8, y + 0, ILI9341_YELLOW);
    tft.drawPixel(x + 8, y + 1, ILI9341_YELLOW);
    tft.drawPixel(x + 8, y + 14, ILI9341_YELLOW);
    tft.drawPixel(x + 8, y + 15, ILI9341_YELLOW);
    tft.drawPixel(x + 8, y + 4, ILI9341_BLACK);
    tft.drawPixel(x + 9, y + 0, ILI9341_YELLOW);
    tft.drawPixel(x + 9, y + 1, ILI9341_YELLOW);
    tft.drawPixel(x + 9, y + 14, ILI9341_YELLOW);
    tft.drawPixel(x + 9, y + 15, ILI9341_YELLOW);
    tft.drawPixel(x + 9, y + 4, ILI9341_BLACK);
    tft.drawPixel(x + 9, y + 8, ILI9341_YELLOW);
    tft.drawPixel(x + 10, y + 0, ILI9341_YELLOW);
    tft.drawPixel(x + 10, y + 1, ILI9341_YELLOW);
    tft.drawPixel(x + 10, y + 14, ILI9341_YELLOW);
    tft.drawPixel(x + 10, y + 15, ILI9341_YELLOW);
    tft.drawPixel(x + 10, y + 7, ILI9341_YELLOW);
    tft.drawPixel(x + 10, y + 8, ILI9341_YELLOW);
    tft.drawPixel(x + 10, y + 9, ILI9341_YELLOW);
    tft.drawPixel(x + 11, y + 14, ILI9341_YELLOW);
    tft.drawPixel(x + 11, y + 15, ILI9341_YELLOW);
    tft.drawPixel(x + 11, y + 1, ILI9341_YELLOW);
    tft.drawPixel(x + 11, y + 2, ILI9341_YELLOW);
    tft.drawPixel(x + 11, y + 6, ILI9341_YELLOW);
    tft.drawPixel(x + 11, y + 7, ILI9341_YELLOW);
    tft.drawPixel(x + 11, y + 9, ILI9341_YELLOW);
    tft.drawPixel(x + 11, y + 10, ILI9341_YELLOW);
    tft.drawPixel(x + 12, y + 2, ILI9341_YELLOW);
    tft.drawPixel(x + 12, y + 3, ILI9341_YELLOW);
    tft.drawPixel(x + 12, y + 5, ILI9341_YELLOW);
    tft.drawPixel(x + 12, y + 6, ILI9341_YELLOW);
    tft.drawPixel(x + 12, y + 9, ILI9341_YELLOW);
    tft.drawPixel(x + 12, y + 10, ILI9341_YELLOW);
    tft.drawPixel(x + 12, y + 14, ILI9341_YELLOW);
    tft.drawPixel(x + 12, y + 15, ILI9341_YELLOW);
    tft.drawPixel(x + 13, y + 4, ILI9341_YELLOW);
    tft.drawPixel(x + 13, y + 5, ILI9341_YELLOW);
    tft.drawPixel(x + 13, y + 11, ILI9341_YELLOW);
    tft.drawPixel(x + 13, y + 12, ILI9341_YELLOW);
    for (uint16_t i = 6; i <= 10; i++) {
        tft.drawPixel(x + 13, y + i, ILI9341_WHITE);
    }
    for (uint16_t i = 7; i <= 9; i++) {
        tft.drawPixel(x + 12, y + i, ILI9341_WHITE);
    }
    tft.drawPixel(x + 11, y + 8, ILI9341_WHITE);
    delay(500);
    for (uint16_t i = y + 5; i < y + 13; i++) {
        tft.drawPixel(x, i, ILI9341_WHITE);
    }
    for (uint16_t i = y + 4; i < y + 14; i++) {
        tft.drawPixel(x + 1, i, ILI9341_WHITE);
    }
    tft.drawPixel(x + 2, y + 3, ILI9341_WHITE);
    tft.drawPixel(x + 2, y + 4, ILI9341_WHITE);
    tft.drawPixel(x + 2, y + 5, ILI9341_WHITE);
    tft.drawPixel(x + 2, y + 13, ILI9341_WHITE);
    tft.drawPixel(x + 2, y + 14, ILI9341_WHITE);
    tft.drawPixel(x + 3, y + 2, ILI9341_WHITE);
    tft.drawPixel(x + 3, y + 3, ILI9341_WHITE);
    tft.drawPixel(x + 3, y + 4, ILI9341_WHITE);
    tft.drawPixel(x + 3, y + 14, ILI9341_WHITE);
    tft.drawPixel(x + 3, y + 15, ILI9341_WHITE);
    tft.drawPixel(x + 4, y + 1, ILI9341_WHITE);
    tft.drawPixel(x + 4, y + 2, ILI9341_WHITE);
    tft.drawPixel(x + 4, y + 14, ILI9341_WHITE);
    tft.drawPixel(x + 4, y + 15, ILI9341_WHITE);
    tft.drawPixel(x + 5, y + 0, ILI9341_WHITE);
    tft.drawPixel(x + 5, y + 1, ILI9341_WHITE);
    tft.drawPixel(x + 5, y + 14, ILI9341_WHITE);
    tft.drawPixel(x + 5, y + 15, ILI9341_WHITE);
    tft.drawPixel(x + 6, y + 0, ILI9341_WHITE);
    tft.drawPixel(x + 6, y + 1, ILI9341_WHITE);
    tft.drawPixel(x + 6, y + 14, ILI9341_WHITE);
    tft.drawPixel(x + 6, y + 15, ILI9341_WHITE);
    tft.drawPixel(x + 7, y + 0, ILI9341_WHITE);
    tft.drawPixel(x + 7, y + 1, ILI9341_WHITE);
    tft.drawPixel(x + 7, y + 14, ILI9341_WHITE);
    tft.drawPixel(x + 7, y + 15, ILI9341_WHITE);
    tft.drawPixel(x + 8, y + 0, ILI9341_WHITE);
    tft.drawPixel(x + 8, y + 1, ILI9341_WHITE);
    tft.drawPixel(x + 8, y + 14, ILI9341_WHITE);
    tft.drawPixel(x + 8, y + 15, ILI9341_WHITE);
    tft.drawPixel(x + 8, y + 4, ILI9341_WHITE);
    tft.drawPixel(x + 9, y + 0, ILI9341_WHITE);
    tft.drawPixel(x + 9, y + 1, ILI9341_WHITE);
    tft.drawPixel(x + 9, y + 14, ILI9341_WHITE);
    tft.drawPixel(x + 9, y + 15, ILI9341_WHITE);
    tft.drawPixel(x + 9, y + 4, ILI9341_WHITE);
    tft.drawPixel(x + 9, y + 8, ILI9341_WHITE);
    tft.drawPixel(x + 10, y + 0, ILI9341_WHITE);
    tft.drawPixel(x + 10, y + 1, ILI9341_WHITE);
    tft.drawPixel(x + 10, y + 14, ILI9341_WHITE);
    tft.drawPixel(x + 10, y + 15, ILI9341_WHITE);
    tft.drawPixel(x + 10, y + 7, ILI9341_WHITE);
    tft.drawPixel(x + 10, y + 8, ILI9341_WHITE);
    tft.drawPixel(x + 10, y + 9, ILI9341_WHITE);
    tft.drawPixel(x + 11, y + 14, ILI9341_WHITE);
    tft.drawPixel(x + 11, y + 15, ILI9341_WHITE);
    tft.drawPixel(x + 11, y + 1, ILI9341_WHITE);
    tft.drawPixel(x + 11, y + 2, ILI9341_WHITE);
    tft.drawPixel(x + 11, y + 6, ILI9341_WHITE);
    tft.drawPixel(x + 11, y + 7, ILI9341_WHITE);
    tft.drawPixel(x + 11, y + 9, ILI9341_WHITE);
    tft.drawPixel(x + 11, y + 10, ILI9341_WHITE);
    tft.drawPixel(x + 12, y + 2, ILI9341_WHITE);
    tft.drawPixel(x + 12, y + 3, ILI9341_WHITE);
    tft.drawPixel(x + 12, y + 5, ILI9341_WHITE);
    tft.drawPixel(x + 12, y + 6, ILI9341_WHITE);
    tft.drawPixel(x + 12, y + 9, ILI9341_WHITE);
    tft.drawPixel(x + 12, y + 10, ILI9341_WHITE);
    tft.drawPixel(x + 12, y + 14, ILI9341_WHITE);
    tft.drawPixel(x + 12, y + 15, ILI9341_WHITE);
    tft.drawPixel(x + 13, y + 4, ILI9341_WHITE);
    tft.drawPixel(x + 13, y + 5, ILI9341_WHITE);
    tft.drawPixel(x + 13, y + 11, ILI9341_WHITE);
    tft.drawPixel(x + 13, y + 12, ILI9341_WHITE);
}

void setup() {
    Serial.begin(9600);
    // put your setup code here, to run once:
    randomSeed(analogRead(A0));
    Display::begin();
    // Make screen white, set up text
    tft.fillScreen(0xFFFF);
    tft.setTextColor(0x0000, 0xFFFF);
    tft.setTextSize(2);
    tft.setCursor(0, 0);
    tft.println("Think of a number from 0 to 999. I'll try to guess it.");
    tft.setCursor(0, 60);
    // No space between less and than because of text wrapping
    tft.println(String("Greater than or lessthan ") + String(displayNumber) + "?");
    // Draw greater than or less than buttons (NOTE: y0 = 120)
    tft.fillRoundRect(5, 120, 110, 40, 7, ILI9341_GREEN);
    tft.fillRoundRect(125, 120, 110, 40, 7, ILI9341_GREEN);
    tft.fillRoundRect(65, 170, 110, 40, 7, ILI9341_GREEN);
    // make text bigger and change color
    tft.setTextSize(3);
    tft.setTextColor(0xFFFF);
    // set to correct position and print > and <
    tft.setCursor(50, 128);
    tft.print(">");
    tft.setCursor(170, 128);
    tft.print("<");
    tft.setCursor(110, 178);
    tft.print("=");
    // clean up
    tft.setTextSize(2);
    tft.setTextColor(0x0000, 0xFFFF);
    // Ask the Task library to invoke touched each time a touch is detected.
    // The screen is sampled 50 times per second. The millisecond parameter
    // can be adjusted to find the happy medium between getting multiple touches
    // or missing a touch. The 200ms number is just a starting point.
    Task::begin();
    Display::registerForTouch(touched, 200);
    uint8_t taskid = Task::NO_TASKID;
    // Schedule a task to run every 10 times a second to eat the screen up!
    Task::scheduleTask(pacMan, 100, true, taskid);
    // Control never returns from run(). Instead, touched is called every time
    // a touch is detected and the, um, display consumption is called 500
    // times per second.
    Task::run();
}

void loop() {
}
