/*
 * LabKitGraph.ino -- Graphing on the LabKit TFT!
 *
 * This program graphs a .csv file on the SD card.
 * You select the CSV file to graph.
 *
 * Copyright (c) 2017-2018 Ben Goldberg
 * MIT License (see LICENSE in this repository)
 * CHANGES:
 *   4/3/18 Updated header comment
 * TODO:
 *
 * BUGS:
 *
 */
#include <SPI.h>
#include <SD.h>
#include <Adafruit_GFX.h>
#include <SeeedTouchScreen.h>
#include <Adafruit_ILI9341.h>

/* Below code copied from touchScreen.ino, an example from the Seeed Touch Screen library */

/* Here's the license for that library:

  The MIT License (MIT)

  Copyright (c) 2013 Seeed Technology Inc.

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

// Correct pins
#define YP A2
#define XM A1
#define YM 14
#define XP 17

//Measured ADC values for (0,0) and (210-1,320-1)
//TS_MINX corresponds to ADC value when X = 0
//TS_MINY corresponds to ADC value when Y = 0
//TS_MAXX corresponds to ADC value when X = 240 -1
//TS_MAXY corresponds to ADC value when Y = 320 -1

#define TS_MINX 116*2
#define TS_MAXX 890*2
#define TS_MINY 83*2
#define TS_MAXY 913*2

TouchScreen ts = TouchScreen(XP, YP, XM, YM);

/* End of copied code */

/* Below code copied from spitftbitmap.ino, an example from the Adafruit ILI9341 Library */


/***************************************************
  This is our Bitmap drawing example for the Adafruit ILI9341 Breakout and Shield
  ----> http://www.adafruit.com/products/1651
  Check out the links above for our tutorials and wiring diagrams
  These displays use SPI to communicate, 4 or 5 pins are required to
  interface (RST is optional)
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!
  Written by Limor Fried/Ladyada for Adafruit Industries.
  MIT license, all text above must be included in any redistribution
 ****************************************************/

#define TFT_DC 6
#define TFT_CS 5
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

#define SD_CS 4

/* End of copied code */

int xMax = 0;
int yMax = 0;
int xMin = 32766;
int yMin = 32766;
int lastPoint[2] = {0, 0};

String file = "";

bool listenForTouches() {
    // 60x16 px for "Select" in font size 2
    Point p = ts.getPoint();
    while (p.z < __PRESSURE) {
        p = ts.getPoint();
    }
    int x = map(p.x, TS_MINX, TS_MAXX, 0, 240);
    int y = map(p.y, TS_MINY, TS_MAXY, 0, 320);
    Serial.println(x);
    Serial.println(y);
    if ((x >= 120 and x <= 180) and (y >= 160 and y <= 176)) {
        return true;
    } else {
        return false;
    }
}

void selectFile(File root) {
    bool found = false;
    while (1) {
        File next = root.openNextFile();
        if (! next) {
            tft.fillScreen(ILI9341_WHITE);
            tft.setCursor(0, 0);
            tft.setTextSize(2);
            tft.setTextColor(ILI9341_BLACK);
            tft.println("No .csv files found on SD card.");
            tft.println("If you have a .csv file on the SD card, it probably doesn't have an 8.3 filename.");
            while (1);
        }
        Serial.println(next.name());
        if (String(next.name()).indexOf(".CSV") != -1) {
            found = true;
            tft.fillScreen(ILI9341_WHITE);
            tft.setCursor(0, 0);
            tft.setTextSize(2);
            tft.setTextColor(ILI9341_BLACK);
            tft.println(next.name());

            tft.setCursor(120, 160);
            tft.setTextSize(2);
            tft.println("Select");
            bool touched = listenForTouches();

            if (touched) {
                file = next.name();
                break;
            }
        }
        next.close();
    }
    if (! found) {
        tft.fillScreen(ILI9341_WHITE);
        tft.setCursor(0, 0);
        tft.setTextSize(2);
        tft.setTextColor(ILI9341_BLACK);
        tft.println("No .csv files found on SD card.");
        tft.println("If you have a .csv file on the SD card, it probably doesn't have an 8.3 filename.");
        while (1);
    }
}

void computeMaxMin() {
    File f = SD.open(file);
    f.seek(0);
    while (f.read() != '\n');
    while (f.available()) {
        int x = f.parseFloat() + 0.5; // +0.5 to round
        int y = f.parseFloat() + 0.5;
        if (x < xMin) {
            xMin = x;
        }
        if (x > xMax) {
            xMax = x;
        }

        if (y < yMin) {
            yMin = y;
        }
        if (y > yMax) {
            yMax = y;
        }
    }
    f.close();
}

void drawAxes() {
    tft.fillScreen(tft.color565(0, 0, 0));
    tft.drawFastVLine(20, 20, 200, tft.color565(255, 255, 255));
    tft.drawFastHLine(20, 220, 200, ILI9341_WHITE);
    tft.setTextSize(1);
    tft.setTextColor(ILI9341_WHITE);
    tft.setCursor(20, 225);
    // print xmin
    tft.print(xMin);
    tft.setCursor(205, 225);
    // print xmax
    tft.print(xMax);
    tft.setCursor(20, 5);
    // print ymax
    tft.print(yMax);
    tft.setCursor(5, 210);
    // print ymin
    tft.print(yMin);
    tft.setTextSize(2);
    tft.setCursor(0, 240);
    tft.print(F("x axis = "));
    File f = SD.open(file);
    f.seek(0);
    while (f.peek() != ',') {
        tft.write(f.read());
    }
    f.read();
    tft.println();
    tft.print(F("y axis = "));
    while (f.peek() != '\n') {
        tft.write(f.read());
    }
    f.close();
}

void plotPoint(int x, int y) {
    int xpix = 20.0 + ((x - xMin) * (200.0 / (xMax - xMin))) + 0.5; // +0.5 to round
    int ypix = 220.0 - ((y - yMin) * (200.0 / (yMax - yMin))) + 0.5; // +0.5 to round
    if ((lastPoint[0] == 0) && (lastPoint[1] == 0)) {
        lastPoint[0] = xpix;
        lastPoint[1] = ypix;
    }
    Serial.print("Point: ");
    Serial.print(xpix);
    Serial.print(",");
    Serial.println(ypix);
    tft.drawLine(lastPoint[0], lastPoint[1], xpix, ypix, ILI9341_BLUE);
    tft.drawPixel(lastPoint[0], lastPoint[1], ILI9341_RED);
    lastPoint[0] = xpix;
    lastPoint[1] = ypix;
    tft.drawPixel(xpix, ypix, ILI9341_RED);
}

void plotPoints() {
    File f = SD.open(file);
    f.seek(0);
    while (f.read() != '\n');
    while (f.available()) {
        int x = f.parseFloat();
        int y = f.parseFloat();
        Serial.println(x);
        Serial.println(y);
        plotPoint(x, y);
    }
    f.close();
}

void setup() {
    // put your setup code here, to run once:
    Serial.begin(9600);
    tft.begin();
    delay(50);
    Serial.print("Initializing SD card");
    while (!SD.begin(SD_CS)) {
        Serial.print(".");
    }
    Serial.println();
    Serial.println("Done!");
    File root = SD.open("/");
    selectFile(root);
    root.close();
    computeMaxMin();
    Serial.println(F("xMin, yMin, xMax, yMax: "));
    Serial.println(xMin);
    Serial.println(yMin);
    Serial.println(xMax);
    Serial.println(yMax);
    Serial.println();
    drawAxes();
    plotPoints();
}

void loop() {
    // put your main code here, to run repeatedly:

}
