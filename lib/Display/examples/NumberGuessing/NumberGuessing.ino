/*
 * NumberGuessing.ino
 * Game for the LabKit where the Arduino tries to guess your number.
 * Have fun!
 * Copyright (c) 2018 Ben Goldberg
 * MIT License (see LICENSE in this repository).
 * CHANGES:
 *   4/3/18 Updated header comment
 * TODO:
 *
 * BUGS:
 *
 */
#include <NRingBuffer.h>
#include <Task.h>
#include <SPI.h>
#include <SD.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>
#include <SeeedTouchScreen.h>
#include <MsTimer2.h>
#include <Display.h>

uint16_t displayNumber = 500;
uint16_t lower = 0;
uint16_t upper = 1000;
uint8_t guesses = 1;

void touched(uint8_t x, uint16_t y) {
    if (y >= 120 && y <= 160) {
        // button pressed.
        guesses += 1;
        if (x > 125) {
            upper = displayNumber;
        } else {
            lower = displayNumber;
        }
        displayNumber = lower + (upper - lower) / 2;
        tft.setCursor(0, 60);
        tft.println(String("Greater than or lessthan ") + String(displayNumber) + "?");
    } else if (y >= 170 && y <= 210) {
        // equal
        tft.fillScreen(0xFFFF);
        tft.setCursor(0, 0);
        // no spaces 'cause of text wrapping
        tft.println("Your number was " + String(displayNumber) + ".I guessed your number in " + String(guesses) + " guesses. Havea good day!");
        while (1);
    }
}

void setup() {
    // put your setup code here, to run once:
    Display::begin();
    // Make screen white, set up text
    tft.fillScreen(0xFFFF);
    tft.setTextColor(0x0000, 0xFFFF);
    tft.setTextSize(2);
    tft.setCursor(0, 0);
    tft.println("Think of a number from 0 to 999. I'll try to guess it.");
    tft.setCursor(0, 60);
    // No space between less and than because of text wrapping
    tft.println(String("Greater than or lessthan ") + String(displayNumber) + "?");
    // Draw greater than or less than buttons (NOTE: y0 = 120)
    tft.fillRoundRect(5, 120, 110, 40, 7, ILI9341_GREEN);
    tft.fillRoundRect(125, 120, 110, 40, 7, ILI9341_GREEN);
    tft.fillRoundRect(65, 170, 110, 40, 7, ILI9341_GREEN);
    // make text bigger and change color
    tft.setTextSize(3);
    tft.setTextColor(0xFFFF);
    // set to correct position and print > and <
    tft.setCursor(50, 128);
    tft.print(">");
    tft.setCursor(170, 128);
    tft.print("<");
    tft.setCursor(110, 178);
    tft.print("=");
    // clean up
    tft.setTextSize(2);
    tft.setTextColor(0x0000, 0xFFFF);
    // setup done!
}

void loop() {
    // put your main code here, to run repeatedly:
    uint16_t x, y;
    Display::waitForTouch(x, y);
    touched(x, y);
}
