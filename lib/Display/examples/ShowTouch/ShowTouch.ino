/*
   Capture and display touch screen data and display the position and duration
   of the touch. A periodic interrupt samples the touchscreen Z axis
   measurement and captures NUM_SAMPLES Z values each time a press is detected.
   A simple state machine controls the interrupt handler. The handler signals
   to the mainline code when a new set of samples are available to process. A
   mark is shown on the screen corresponding to the saved X and Y values along
   with the milliseconds duration and average Z value.

   Copyright (c) 2017 Pete Soper
   MIT License (see LICENSE.txt in this repo)
*/

#include <SPI.h>
#include <SD.h>
#include <Adafruit_GFX.h>
#include <NRingBuffer.h>
#include <Task.h>
#include <LabKit.h>

// TODO: Shift these into Display or LabKit as defaults

#define TS_MINX 116*2
#define TS_MAXX 890*2
#define TS_MINY 83*2
#define TS_MAXY 913*2

#include <Display.h>
#include <SeeedTouchScreen.h>
#include <Adafruit_ILI9341.h>
#include <MsTimer2.h>

const int16_t THRESHOLD = 0;          // Minimum Z value to call a "touch"

const uint32_t MILLISECONDS_PER_TICK = 10;// Interval between tick interrupts

const uint8_t IDLE = 0;           // Waiting for press state
const uint8_t CAPTURE = 1;        // Gathering press samples
const uint8_t WAITING = 2;        // Waiting for samples to be taken

const uint8_t NUM_SAMPLES = 20;       // Size of sample buffer
const uint8_t MAX_ROWS = 10;              // Max sample rows to print

// Touchscreen interface object. Pin numbers defined in Display.h

TouchScreen myts = TouchScreen(XP, YP, XM, YM);

// The TFT display control object
// Adafruit_ILI9341 tft = Adafruit_ILI9341(LabKit.TFT_CS, LabKit.TFT_DC);

// Shared between tick interrupt handler and mainline code

static volatile bool triggered = false;   // True when samples available
typedef struct {
    int16_t x;
    int16_t y;
    int16_t z;
} sample_data;
static sample_data samples[NUM_SAMPLES];  // Sample buffer

/*
 * Periodic interrupt handler called very MILLISECONDS_PER_TICK.
 * Use a simple state machine to wait until minimal touchscreen press is
 * detected, then gather a set of samples and signal their availability with
 * shared variable "triggered". Then wait until the mainline code sees and
 * handles the samples and then resets trigger.
 */

void tick() {
    static uint8_t sample_index;      // Next sample buffer index
    static uint8_t state = IDLE;      // Current state

    Point p = myts.getPoint();        // Fetch touchscreen x, y, z values

    if (state == IDLE) {          // Waiting for first press?
        if (p.z > THRESHOLD) {      // Yes, check Z: high enough?
            sample_index = 0;         // Yes, start sample index
            state = CAPTURE;          // Set state and FALL THROUGH
        }
    }

    if (state == CAPTURE) {       // Capturing samples?
        samples[sample_index].x = p.x;  // Yes, save current X
        samples[sample_index].y = p.y;  // Yes, save current Y
        samples[sample_index].z = p.z;  // Yes, save current Z
        sample_index += 1;
        if (sample_index == NUM_SAMPLES) {  // Buffer full?
            triggered = true;         // Yes: signal mainline code
            state = WAITING;
        }
    } else if (state == WAITING) {
        if (! triggered) {          // Samples consumed yet?
            state = IDLE;         // Yes, look for new press
        }
    }
}

/*
 * Set up the hardware
 */

void setup() {
    LabKit.begin(9600);           // Initialize the lab kit hardware
    Serial.begin(9600);
    Serial.println("A");
    tft.begin(400000);

    MsTimer2::set(MILLISECONDS_PER_TICK, tick);// Periodic timer interrupt
    MsTimer2::start();
}

/*
 * Look for available samples and print them when found, omitting trailing
 * zero samples.
 */

void loop() {
    tft.fillCircle(100, 100, 30, ILI9341_RED);
    tft.startWrite();
    if (triggered) {          // Set by interrupt handler
        int16_t x, y;
        uint8_t nonzero_elements = 1;   // number of nonzero sample values
        for (int i = NUM_SAMPLES - 1; i > 0; i--) {
            if (samples[i].z != 0) {
                nonzero_elements = i + 1;
                break;
            }
        }

        // This code is worthless, as the Z values do not actually vary during
        // a press.

#if 0
        uint32_t sum = 0;
        for (int i = 0; i < NUM_SAMPLES; i++) {
            sum += samples[i].z;
            samples[i].z = 0;
        }

        uint32_t average_z = sum / nonzero_elements;
#endif

        uint32_t milliseconds = nonzero_elements * MILLISECONDS_PER_TICK;

        /*
           Draw a filled circle at the point of the touch.
         */

        tft.fillScreen(ILI9341_WHITE);
        x = map(samples[0].x, TS_MINX, TS_MAXX, 0, 240);
        y = map(samples[0].y, TS_MINY, TS_MAXY, 0, 320);
        tft.fillCircle(x, y, 30, ILI9341_RED);
        tft.startWrite();
        Serial.println("3");
        Serial.println("B");
        Serial.println("1");
        tft.setAddrWindow(0, 0, 240, 320);
        Serial.println("2");
        tft.writeCommand(ILI9341_RAMRD);
        Serial.println("4");
        unsigned long i = 0U;
        unsigned long limit = 240;
        limit *= 320;
        limit *= 4;
        Serial.print("limit: ");
        Serial.println(limit);
        do {
            uint8_t temp = 0;
            SPDR = temp;
            while (!(SPSR & _BV(SPIF)));
            temp = SPDR;
            if (temp != 0) {
                Serial.print(i, HEX);
                Serial.print(" ");
                Serial.println(temp, HEX);
            } else if ((i % 10000U) == 0) {
                Serial.println(i);
            }
            i += 1;
        } while (i < limit);
        tft.endWrite();
        Serial.println("fini");

#if 0
        tft.setCursor(x - 30, y - 30);
        tft.setTextSize(1);
        tft.setTextColor(ILI9341_BLACK);
        tft.print(milliseconds);
#endif

        triggered = false;          // Tell handler to get new samples
    } // if triggered
}
