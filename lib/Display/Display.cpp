/*
 * Lab Kit Display Library
 *
 * This code provides an application programming interface (API) to the
 * lab kit display/touchscreen/sd card not appropriate for the more specific
 * hardware interface libraries.
 *
 * Copyright (c) 2017 Pete Soper and Ben Goldberg
 * MIT License (see LICENSE in this repository)
 */

#include <MsTimer2.h>
#include "Display.h"

Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

TouchScreen ts = TouchScreen(XP, YP, XM, YM);

/*
 * Initialize the display system. The ILI9341 init should probably be done
 * here
 */

void Display::begin() {
    // ILI9341 init:
    tft.begin();
}

/*
 * Sample the touch screen until a touch is detected and set the X and Y
 * coordinate position of the touch.
 */

void Display::waitForTouch(uint16_t &x, uint16_t &y) {
    Point p;
    do {
        p = ts.getPoint();
    } while (!(p.z > __PRESSURE));
    x = map(p.x, TS_MINX, TS_MAXX, 0, 240);
    y = map(p.y, TS_MINY, TS_MAXY, 0, 320);
    delay(100);
}

static uint8_t touch_taskid = Task::NO_TASKID;
static uint16_t touch_x, touch_y;

static void touchTask() {
    touch_callback_pointer func = (touch_callback_pointer) Task::getLocal(touch_taskid);
    func(touch_x, touch_y);
}

static void tick() {
    Point p;
    p = ts.getPoint();
    if (p.z > __PRESSURE) {
        touch_x = map(p.x, TS_MINX, TS_MAXX, 0, 240);
        touch_y = map(p.y, TS_MINY, TS_MAXY, 0, 320);
        Task::event(touch_taskid);
    }
}

/*
 * Register a function to be called with the X and Y coordinates of a
 * detected screen touch. Depends on MsTimer2. This code only supports a
 * single instance.
 * TODO: How to avoid this failing because some other code has done a start?
 */
static void Display::registerForTouch(touch_callback_pointer func,
                                      uint32_t sample_ms) {
#if 0
    Task::registerTask(touchTask, true, touch_taskid, func);
#endif
    MsTimer2::set(sample_ms, tick);
    MsTimer2::start();
}
