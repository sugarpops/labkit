/*
 * Display Library - Thin API for ILI9341 display and resistive touch screen
 *
 * This API defines GPIO pin assignments and touch screen calibration values
 * as well as an initialization function and two experimental functions related
 * to touch detection.
 *
 * See public interfaces below for details.
 *
 * Copyright (c) 2017 Pete Soper and Ben Goldberg
 * MIT License (see LICENSE in this repository)
 *
 * API CHANGES:
 * API TODO:
 *   REFACTOR OTHER LIBS TO CONSISTENTLY USE THIS API
 * API BUGS AND FEATURE REQUESTS:
 */

// Arduino specific defines

#ifndef LABKIT_DISPLAY_H

#define LABKIT_DISPLAY_H

#define __LABKIT_DISPLAY_VERSION 1

#include <SPI.h>
#include <SD.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>
#include <SeeedTouchScreen.h>
#include <Task.h>

extern Adafruit_ILI9341 tft;

#define YP A2       // Analog input for Screen
#define XM A1       // Analog input for Screen
#define YM 14       // Analog input for Screen
#define XP 17       // Analog input for Screen

//Measured ADC values for (0,0) and (210-1,320-1)
//TS_MINX corresponds to ADC value when X = 0
//TS_MINY corresponds to ADC value when Y = 0
//TS_MAXX corresponds to ADC value when X = 240 -1
//TS_MAXY corresponds to ADC value when Y = 320 -1

// TODO: how much variablity is there in displays? How
// easy could on the fly calibration be made? Maybe
// eventually detect something like "swipe upper right to
// lower left to trigger calibration" as a feature in
// applications?

#define TS_MINX 116*2   // Empirical values
#define TS_MAXX 890*2
#define TS_MINY 83*2
#define TS_MAXY 913*2

// Callback function type for passing touch coordinates to application

typedef void (*touch_callback_pointer)(uint16_t x, uint16_t y);

extern TouchScreen ts;

// This is the collection of functions and data to be contained within the
// Display namespace. This is like a class declaration, except there are no
// class or instance objects involved: its just a scope for encapsulation.

namespace Display {
    /*
     * Initialize the Display system
     */
    void begin();

    /*
     * Sample the touch screen over and over until a touch detected and copy
     * out it's x/y location.
     */
    void waitForTouch(uint16_t &x, uint16_t &y);

    /*
     * Sample the touch screen every check_ms milliseconds and call the given
     * function with the x and y coordinates of the touch when one is detected.
     * Expects void func(uint16_t&, uint16_t&) signature.
     */
    void registerForTouch(touch_callback_pointer func, uint32_t check_ms);
};

#endif // LABKIT_DISPLAY_H
