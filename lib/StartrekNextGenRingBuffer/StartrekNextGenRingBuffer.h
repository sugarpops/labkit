/*
 * StartrekNextGenRingBuffer.h - RingBuffer: the Fourth Generation
 *
 * This fourth planned ring buffer type will have as its buffer element type a
 * union of some fixed set of fields (that could, in turn, be structures or
 * even uions, etc). Each union instance will be an element of a dynamically
 * allocated array. The declaration of the buffer element union
 * and any user-defined types within it will be (somehow) defined by the
 * application using the API
 *
 * See the public declarations for details. But ring buffer element details
 * will be application-defined (somehow).
 *
 * Copyright (c) 2018 Pete Soper
 * License TBD
 */
