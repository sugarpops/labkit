/*
 * Test the ring buffer class.
 *
 *  Copyright (c) 2017 Peter J. Soper
 *  MIT License (see "LICENSE" in this repository)
 */

#include <LabKit.h>

#include <NRingBuffer.h>

#define SIZE 8

#define TEST_VALUE1 123
#define TEST_VALUE2 122
#define TEST_VALUE3 121

NRingBuffer buf = NRingBuffer(SIZE);

void setup() {
    LabKit.begin();
}


void loop() {
    int a1 = TEST_VALUE1;
    int *p1 = &a1;
    int a2 = TEST_VALUE2;
    int *p2 = &a2;
    int a3 = TEST_VALUE3;
    int *p3 = &a3;

    if (buf.putData(p1)) {
        if (buf.available() != 1) {
            Serial.println("FAIL 1A");
        }
        p1 = (int *) buf.getData();
        if (p1 && (*p1 == TEST_VALUE1)) {
            Serial.println("PASS 1");
        } else {
            Serial.println("FAIL 1B");
        }
    } else {
        Serial.println("FAIL 1C");
    }

    if (!buf.putData(p1)) {
        Serial.println("FAIL 2X");
    }
    if (!buf.putData(p2)) {
        Serial.println("FAIL 2Y");
    }
    if (!buf.putData(p3)) {
        Serial.println("FAIL 2Z");
    }
    if (buf.available() != 3) {
        Serial.println("FAIL 2A");
    }
    if (DEREF(buf.peekNewest(), int) != TEST_VALUE3) {
        Serial.println("FAIL 2B");
    }
    if (DEREF(buf.peekOldest(), int) != TEST_VALUE1) {
        Serial.println("FAIL 2C");
    }
    p1 = (int *) buf.getData();
    if (!p1 || (*p1 != TEST_VALUE1)) {
        Serial.print(*p1);
        Serial.println("FAIL 2D");
    }
    if (buf.available() != 2) {
        Serial.println("FAIL 2E");
    }
    buf.showData();
    p2 = (int *) buf.getData();
    if (!p2 || (*p2 != TEST_VALUE2)) {
        Serial.print(*p2);
        Serial.println("FAIL 2F");
    }
    if (buf.available() != 1) {
        Serial.println("FAIL 2G");
    }
    buf.init();
    if (buf.available() != 0) {
        Serial.println("FAIL 2H");
    }
    if (buf.getData()) {
        Serial.println("FAIL 2I");
    }
    for (uint8_t i = 0; i < SIZE; i++) {
        if (!buf.putData(p1)) {
            Serial.println("Fail 3J");
        }
    }
    if (buf.available() != SIZE) {
        Serial.println("Fail 3K");
    }
    if (buf.putData(p1)) {
        Serial.println("Fail 3L");
    }
    Serial.println("Finished");
    while (1) {
    }
}
