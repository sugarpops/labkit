/*
 * Ring Buffer Library - Implementation of NRingBuffer.h API
 *
 * See NRingBuffer.h for API details.
 *
 * Copyright (c) 2017 Pete Soper
 * MIT License (see LICENSE in this repository)
 *
 * IMPLEMENTATION CHANGES:
 * IMPLEMENTATION TODO:
 *  Exercise with stress tests to expose reentrancy issues
 *  Use the Task macros for detecting if inside an interrupt
 * IMPLEMENTATION BUGS AND FEATURE REQUESTS:
 *  4/25/2018 - Use conditional compilation to eliminate code irrelevant to
 *              the Task library (everything to do with waiting!)
 */

#include <NRingBuffer.h>

/*
 * Create a new buffer instance.
 */

NRingBuffer::NRingBuffer(uint8_t size) {
    buffer = (void **) malloc(size * sizeof(void *));
    capacity = size;
    mask = capacity - 1;
    entries = head = tail = 0;
}

/*
 * Delete a buffer instance.
 */

NRingBuffer::~NRingBuffer() {
    free(buffer);
}

/*
 * Return elements in use.
 */

uint8_t NRingBuffer::available() {
    return entries;
}

/*
 * Empty a buffer.
 */

void NRingBuffer::init() {
    uint8_t saved_sreg = SREG;
    cli();
    entries = head = tail = 0;
    SREG = saved_sreg;
}

/*
 * Return the oldest element or null.
 */

void *NRingBuffer::peekOldest() {
    uint8_t saved_sreg = SREG;
    cli();
    if (entries) {
        void *temp = buffer[(tail - 1) & mask];
        SREG = saved_sreg;
        return temp;
    } else {
        SREG = saved_sreg;
        return NULL;
    }
}

/*
 * Return the newest element or null.
 */

void *NRingBuffer::peekNewest() {
    uint8_t saved_sreg = SREG;
    cli();
    if (entries) {
        void *temp = buffer[(head - 1) & mask];
        SREG = saved_sreg;
        return temp;
    } else {
        SREG = saved_sreg;
        return NULL;
    }
}

/*
 * Return the next element or NULL.
 */

void *NRingBuffer::getData(bool wait_if_empty) {
    /*
     * Note for "young players".
     * If variable "entries" is not declared "volatile" the compiler is allowed
     * to make code that only loads entries into a register once, making the code
     * not see changes an interrupt handler makes to the the value of the
     * variable (i.e. it's memory contents). This will cause very confusing
     * program failures as this code will loop forever, even though some other
     * test of the variable clearly shows it has a different value than it seems
     * to in this loop. The "volatile" keyword tells the compiler to make
     * code to load entries from memory *every* time there is a reference, so
     * a change that an interrupt handler made will be seen.
     */

    while (!entries) {
        if (wait_if_empty) {
            // TODO Look up proper name for the interrupt enable bit mask
            if (! (SREG & SREG_I)) {
                return NULL;
            } else {
                continue;
            }
        } else {
            return NULL;
        }
    }

    // Don't allow interruption. This is harmless if already inside an
    // interrupt handler because the previous state is preserved. This is
    // THE BIG REASON why this sequence is used for critical sections instead
    // of just cli()/sei().

    uint8_t saved_sreg = SREG;
    cli();

    void *temp = buffer[tail++];
    entries -= 1;
    tail &= mask;

    // end of critical section
    SREG = saved_sreg;

    return temp;
}

/*
 * Store a new element.
 */

void *NRingBuffer::putData(void *data, bool wait_if_full) {
    // This code is not only safe with interrupts enabled, it's depending on
    // one!

    while (entries == capacity) {
        if (wait_if_full) {
            continue;
        } else {
            return NULL;
        }
    }

    uint8_t saved_sreg = SREG;
    cli();

    buffer[head++] = data;
    head &= mask;
    entries += 1;

    SREG = saved_sreg;

    return data;
}

/*
 * Print the ring buffer contents.
 */

void NRingBuffer::showData() {
#ifdef DEBUG
    // Disable interrupts and make a copy of everything needed.

    uint8_t saved_sreg = SREG;
    cli();

    uint8_t copied_entries = entries;
    uint8_t copied_head = head;
    uint8_t copied_tail = tail;
    void **copied_buffer = (void **) malloc(capacity * sizeof(void *));
    for (uint8_t i = 0; i < capacity; i++) {
        copied_buffer[i] = buffer[i];
    }

    // End of critical section
    SREG = saved_sreg;

    Serial.println(entries);
    for (uint8_t i = 0; i < capacity; i++) {

        // subscript
        Serial.print(i);
        Serial.print(" ");

        // data
        Serial.print((uint16_t) copied_buffer[i], HEX);
        Serial.print(" ");

        // Append characters to show next insertion or read position

        if ((copied_entries < capacity) && (i == copied_head)) {
            Serial.print(" <=");
        }

        if ((copied_entries > 0) && (i == copied_tail)) {
            Serial.print(" =>");
        }

        Serial.println();
    }
    free(copied_buffer);
#endif
}

