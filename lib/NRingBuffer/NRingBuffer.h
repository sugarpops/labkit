/*
 * NRingBuffer.h - Ring buffer of untyped pointers with type coercion macros
 *
 * This code provides an application programming interface (API) to a
 * ring buffer class. The functions are intended to be reentrant and safe to
 * use inside and outside interrupt handlers. (incomplete)
 * planned)
 *
 * Buffer items are pointers to void that a user is expected to prudently
 * cast to a specific pointer type with the "DEREF" and "PTR_COPY" macros below.
 *
 * This API is for TINY microcontrollers with miniscule RAM size and the
 * implementation is optimized for this. Buffer sizes are restricted to
 * powers of 2 from 2 to 128.
 *
 * See the public interfaces below for details.
 *
 * Copyright (c) 2017 Pete Soper
 * MIT License (see LICENSE in this repository)
 *
 * API CHANGES
 *  4/2/2018 - conform to Ben and Pete's header conventions
 * API TODO
 * API BUGS AND FEATURE REQUESTS
 */

// Arduino specific defines

#ifndef NRINGBUFFER_H
#define NRINGBUFFER_H

/*
 * Increment this with any backward-incompatible change and each significant
 * addition of new features after the initial release that has been used by
 * the "public"
 */

#define __NRINGBUFFER_VERSION 1

#include <Arduino.h>

/*
 * Type conversion macros.  This is how you swear in C.
 *
 * But at least if these macros are used consistently the search for a bug
 * can start at easy to spot places!
 */

// Dereference a pointer as if it points to a variable of the specified type.
// Use this to reference or assign to what the ringbuffer entry points to.

// Scalar type example:   int x = DEREF(my_ring_buffer.getData(), int);
//
// Struct/union field example:
//  struct my_struct {
//   int a;
//   int b;
//  };
// int x = DEREF(my_ring_buffer.getData(), struct my_struct*)->a;

#define DEREF(p,type) ((type) (*(type *)p))

// Force a void pointer to be a pointer to some other type. Use this to copy
// to/from a pointer of the target type.
// Example:
//  my_complex_structure *ptr =
//      PTR_COPY(my_ring_buffer.getData(), my_complex_structure);

#define PTR_COPY(p,type) ((type *)p)

class NRingBuffer {

    public:

        // Create a buffer object with capacity elements. The capacity actual
        // parameter must be a power of two and between 2 and 128.
        NRingBuffer(uint8_t capacity);

        // Delete a buffer object and free storage.
        ~NRingBuffer();

        // Return the current number of buffer elements occupied
        uint8_t available();

        // Return the next buffer element or null (wait false) or busy-wait
        // until an element is available (wait true)
        void *getData(bool wait = false);

        // Return the most recently added buffer element or null if buffer empty
        void *peekNewest();

        // Return least recently added buffer element or null if buffer empty
        void *peekOldest();

        // Add the given pointer to the buffer and return it or null
        // if buffer full and wait is false. If wait is true and buffer is full
        // control will stay in the function until an element is removed from
        // the buffer.
        void *putData(void *data, bool wait = false);

        // Empty the buffer
        void init();

        // Output the buffer contents, number of entries, and head and tail
        // This is a null operation if preprocessor var DEBUG not defined when
        // the library is compiled.
        void showData();

    private:
        void **buffer;             // the buffer data
        volatile uint8_t capacity; // buffer elements
        volatile uint8_t mask;     // capacity - 1 as mask for modulus
        volatile uint8_t entries;  // resident data elements
        volatile uint8_t head;     // next insertion index if not full
        volatile uint8_t tail;     // next extraction index if not empty
};

#endif // NRINGBUFFER_H
