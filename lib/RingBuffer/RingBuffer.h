/*
 * RingBuffer.h - RingBuffer (FIFO) of structures
 *
 * This code provides an application programming interface (API) to a
 * simple ring buffer class defining buffer elements as explicit structures.
 * See other parts of the repo for alternate versions using templates and
 * void pointers. A fourth type designed for character logging is planned.
 *
 * See public interfaces below for details.
 *
 * Copyright (c) 2017 Pete Soper
 * MIT License (see LICENSE in this repository)
 *
 * CHANGES (both interface and implementation)
 *  10/5/2017 - Moved short member function bodies out of the header file.
 *              Fixed getSize member function.
 *              Changed getData formal parameter from pointer to reference.
 *              Added wait parameter to accessor and mutator (default false)
 *              Added assertion check for unguarded multibyte references
 *  10/17/2017 - Switched to index names to next_read and next_write, changed
 *              peekLast to peekNewest and added peekOldest with bool returns
 *              and data transparency.
 *  10/26/2017 - Temporarily jack up size for the sake of the "morse sender"
 *              example. This should handle "type ahead" of three to six
 *              characters.
 */

// Arduino specific defines

#ifndef RINGBUFFER_H

#define RINGBUFFER_H

#define __RINGBUFFER_VERSION 2

#include <Arduino.h>

class RingBufferClass {
    public:
        /*
         * The type of a buffer index. If this becomes multibyte and larger than
         * the host machine word size then volatility won't be sufficient:
         * accesses must be guarded by critical sections. For Arduino this
         * means larger than eight bits (i.e. sizeof this type is > 1).
         */
        typedef uint8_t RBIndex;
        static const RBIndex RB_SIZE = 32; // 160 bytes of RAM
    private:
        struct rb_element_struct {
            uint8_t data;
            uint32_t timestamp;
        };
    public:
        typedef struct rb_element_struct RBElement;
    private:
        volatile RBElement buffer[RB_SIZE];
        volatile RBIndex entries;
        volatile RBIndex next_write;
        volatile RBIndex next_read;
    public:
        RBIndex available();
        bool getData(RBElement &p, bool wait = false);
        bool peekNewest(RBElement &buf);
        bool peekOldest(RBElement &buf);
        bool putData(uint8_t data, uint32_t timestamp, bool wait = false);
        void init();
        void showData();
};

// Reference the global instance defined in the implementation (cpp) file

extern RingBufferClass RingBuffer;

#endif // RINGBUFFER_H
