/*
 * Test2.ino -- This program counts from 0 to 7 using the ring buffer
 * It also is an example of using structs (data structures) and typedefs (type defines)
 *
 * Copyright (c) 2017-2018 Ben Goldberg
 * MIT License (see LICENSE in this repository)
 *
 * CHANGES:
 *   4/30/18 updated this header comment
 * TODO:
 *
 * BUGS AND FEATURE REQUESTS:
 *
 */

// library includes
#include <RingBuffer.h>
#include <LabKit.h>
// typedef for a struct
typedef struct bufferPart {
    byte data; // 8 bits
    long timestamp; // 32 bits
    unsigned int counter: 3; // 3 bits?? Yes, this makes the counter so that it can only store numbers from 0 to 7
} BufferPart;

BufferPart part; // using the typedef/struct

void setup() {
    // put your setup code here, to run once:
    LabKit.begin(); // initialize the lab kit
    part.counter = 0; // set the counter to 0
}

void loop() {
    // put your main code here, to run repeatedly:
    if (part.counter == 0) { // if the counter is 0
        RingBuffer.init(); // clear the ring buffer
    }
    part.timestamp = millis(); // set the timestamp to the number of milliseconds since the Arduino started up
    RingBuffer.putData(part.counter, part.timestamp); // put the data in the ring buffer
    part.counter++; // increase the counter by 1
    RingBuffer.showData(); // show what's in the ring buffer
    Serial.println(); // make an empty space on the Serial Monitor
    delay(1000); // wait a second
}
