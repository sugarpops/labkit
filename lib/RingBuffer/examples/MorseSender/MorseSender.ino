/*
 * MorseSender.ino -- This program sends Morse code when you send it over USB.
 *
 * If you send '+', the Morse code speed increases by MILLISECONDS_PER_INCREMENT.
 * If you send '-', the Morse code speed decreases by MILLISECONDS_PER_INCREMENT.
 *
 * Copyright (c) 2017-2018 Ben Goldberg
 * MIT License (see LICENSE in this repository)
 * CHANGES:
 *   4/3/18 Updated header comment
 * TODO:
 *
 * BUGS:
 *
 */
#include <LabKit.h>
#include <MsTimer2.h>
#include <RingBuffer.h>

// global variables
#define MILLISECONDS_PER_INCREMENT 10
#define MILLISECONDS_PER_TICK 1
// make below variables lowercase
unsigned int MILLISECONDS_PER_DIT = 120;
unsigned int MILLISECONDS_PER_DAH = MILLISECONDS_PER_DIT * 3;
unsigned int MILLISECONDS_PER_SILENCE = MILLISECONDS_PER_DIT;
unsigned int MILLISECONDS_PER_CHARACTER_SILENCE = MILLISECONDS_PER_DAH;

#define DIT 0
#define DAH 1
#define SILENCE 0
#define SOUND 1

typedef struct letter {
    char ch; // character that the below code maps to
    unsigned int length; // number of DITs and/or DAHs
    unsigned int bits; // DIT or DAH in each bit position
} Letter;

Letter morseData[] = { {'A', 2, 0b10},
    {'B', 4, 0b0001},
    {'C', 4, 0b0101},
    {'D', 3, 0b001},
    {'E', 1, 0b0},
    {'F', 4, 0b0100},
    {'G', 3, 0b011},
    {'H', 4, 0b0000},
    {'I', 2, 0b00},
    {'J', 4, 0b1110},
    {'K', 3, 0b101},
    {'L', 4, 0b0010},
    {'M', 2, 0b11},
    {'N', 2, 0b01},
    {'O', 3, 0b111},
    {'P', 4, 0b0110},
    {'Q', 4, 0b1011},
    {'R', 3, 0b010},
    {'S', 3, 0b000},
    {'T', 1, 0b1},
    {'U', 3, 0b100},
    {'V', 4, 0b1000},
    {'W', 3, 0b110},
    {'X', 4, 0b1001},
    {'Y', 4, 0b1011},
    {'Z', 4, 0b0011},
    /* Numbers Start and Letters End Here */
    {'1', 5, 0b11110},
    {'2', 5, 0b11100},
    {'3', 5, 0b11000},
    {'4', 5, 0b10000},
    {'5', 5, 0b00000},
    {'6', 5, 0b00001},
    {'7', 5, 0b00011},
    {'8', 5, 0b00111},
    {'9', 5, 0b01111},
    {'0', 5, 0b11111},
    /* Special Characters Start and Numbers End Here */
    {'.', 6, 0b101010},
    {',', 6, 0b110011},
    {'?', 6, 0b001100},
    {'\'', 6, 0b011110},
    {'!', 6, 0b110101},
    {'/', 5, 0b01001},
    {':', 6, 0b000111},
    {';', 6, 0b010101},
    {'=', 5, 0b10001},
    {'+', 5, 0b01010},
    {'-', 6, 0b100001},
    {'_', 6, 0b101100},
    {'"', 6, 0b010010},
    {'@', 6, 0b010110}
};


/*
   Return 1 if the given bit is set, 0 if it is not set

   Bit 0 is the least significant bit, bit 7 is the most.
   So with the value 0b1 bit 0 is set. With 0b10000000 bit 7 is set.
*/
bool testBit(uint8_t data, uint8_t bit_number) {
    return (data >> bit_number) & 1;
}

void tick() { // This function gets called every MILLISECONDS_PER_TICK. It checks to see if there's any data in the ring buffer.
    // If there is data, this function sets the counter and enables or disables the output.
    // After that, it toggles the piezo element.
    // global variables
    RingBufferClass::RBElement rb;
    static bool enable_output, output = 0;
    static unsigned long counter = 0;
    // main function code
    if (counter != 0) {
        counter--;
    } else {
        if (RingBuffer.available()) { // Is there ring buffer data?
            RingBuffer.getData(rb); // If so, get it.
            output = HIGH; // set the output to on,
            counter = rb.timestamp; // set the counter,
            if (rb.data == SILENCE) { // and enable or disable the piezo output
                enable_output = LOW;
            } else {
                enable_output = HIGH;
            }
        }
    }
    digitalWrite(LabKit.PIEZO, output); // send the output to the piezo
    if (counter != 0) {
        output = output ^ enable_output; // output = output XOR enable_output
    }
}

void recompute() { // recompute ms per dah, ms per silence, and ms per silence after a character based on ms per dit
    MILLISECONDS_PER_DAH = MILLISECONDS_PER_DIT * 3;
    MILLISECONDS_PER_SILENCE = MILLISECONDS_PER_DIT;
    MILLISECONDS_PER_CHARACTER_SILENCE = MILLISECONDS_PER_DAH;
}

void setup() {
    LabKit.begin(921600); // initialize pins and start serial communication at 921,600 baud
    pinMode(LabKit.PIEZO, OUTPUT); // initialize piezo buzzer
    // Initialize "tick timer"
    MsTimer2::set(MILLISECONDS_PER_TICK, tick);
    MsTimer2::start();
}

void loop() {
    if (Serial.available()) { // is there any USB data available?
        char ch = Serial.read(); // if so, collect it.
        ch = toupper(ch); // Make the received character uppercase.
        Serial.print((char)ch); // Echo the received character.
        // change + and - control to \+ and \-
        if (ch == '+') {
            // faster speed
            MILLISECONDS_PER_DIT > 0 ? MILLISECONDS_PER_DIT -= MILLISECONDS_PER_INCREMENT : Serial.println("Can't do that, overflow!");
            recompute();
            Serial.println((String)", " + (String)MILLISECONDS_PER_DIT + "ms");
            return;
        }
        if (ch == '-') {
            // slower speed
            MILLISECONDS_PER_DIT < 65535 ? MILLISECONDS_PER_DIT += MILLISECONDS_PER_INCREMENT : Serial.println("Can't do that, overflow!");
            recompute();
            Serial.println((String)", " + (String)MILLISECONDS_PER_DIT + "ms");
            return;
        }
        if (ch == ' ') { // if the character is a space,
            // then make silence for 2 character silences
            RingBuffer.putData(SILENCE, MILLISECONDS_PER_CHARACTER_SILENCE, true);
            RingBuffer.putData(SILENCE, MILLISECONDS_PER_CHARACTER_SILENCE, true);
        }
        for (uint8_t i = 0; i < (sizeof(morseData) / sizeof(Letter)); i++) {
            if (morseData[i].ch == ch) {
                // the current morseData character matches the received character
                for (int q = 0; q < morseData[i].length; q++) { // so for every dit and dah in the character,
                    // put sound then silence into the ring buffer
                    RingBuffer.putData(SOUND, testBit(morseData[i].bits, q) == DIT ? MILLISECONDS_PER_DIT : MILLISECONDS_PER_DAH, true);
                    RingBuffer.putData(SILENCE, MILLISECONDS_PER_SILENCE, true);
                }
                RingBuffer.putData(SILENCE, MILLISECONDS_PER_CHARACTER_SILENCE -
                                   MILLISECONDS_PER_SILENCE, true); // put the character silence in
            } else {
                /* no match */
                continue;
            }
        }
    }
}
