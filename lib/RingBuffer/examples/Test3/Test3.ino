/*
   MorseSender.ino
   This program sends Morse code.
   Copyright (c) 2017 Ben Goldberg

   Modified by Pete Soper to demonstrate data hiding and buffering
*/
#include <LabKit.h>
#include <MsTimer2.h>
#include <RingBuffer.h>

#define MILLISECONDS_PER_TICK 1
#define MILLISECONDS_PER_DIT 125
#define MILLISECONDS_PER_DAH MILLISECONDS_PER_DIT*3
#define MILLISECONDS_PER_SILENCE MILLISECONDS_PER_DIT
#define MILLISECONDS_PER_CHARACTER_SILENCE MILLISECONDS_PER_DAH

#define DIT 0
#define DAH 1
#define SILENCE 0
#define SOUND 1

typedef struct letter {
    char ch;
    unsigned int length;
    unsigned int bits;
} Letter;

static Letter morseData[] = { {'A', 2, 0b10},
    {'B', 4, 0b0001},
    {'C', 4, 0b0101},
    {'D', 3, 0b001},
    {'E', 1, 0b0},
    {'F', 4, 0b0100},
    {'G', 3, 0b011},
    {'H', 4, 0b0000},
    {'I', 2, 0b00},
    {'J', 4, 0b1110},
    {'K', 3, 0b101},
    {'L', 4, 0b0010},
    {'M', 2, 0b11},
    {'N', 2, 0b01},
    {'O', 3, 0b111},
    {'P', 4, 0b0110},
    {'Q', 4, 0b1011},
    {'R', 3, 0b010},
    {'S', 3, 0b000},
    {'T', 1, 0b1},
    {'U', 3, 0b100},
    {'V', 4, 0b1000},
    {'W', 3, 0b110},
    {'X', 4, 0b1001},
    {'Y', 4, 0b1011},
    {'Z', 4, 0b0011},
    /* Numbers Start and Letters End Here */
    {'1', 5, 0b11110},
    {'2', 5, 0b11100},
    {'3', 5, 0b11000},
    {'4', 5, 0b10000},
    {'5', 5, 0b00000},
    {'6', 5, 0b00001},
    {'7', 5, 0b00011},
    {'8', 5, 0b00111},
    {'9', 5, 0b01111},
    {'0', 5, 0b11111},
    /* Special Characters Start and Numbers End Here */
    {'.', 6, 0b101010},
    {',', 6, 0b110011},
    {'?', 6, 0b001100},
    {'\'', 6, 0b011110},
    {'!', 6, 0b110101},
    {'/', 5, 0b01001},
    {':', 6, 0b000111},
    {';', 6, 0b010101},
    {'=', 5, 0b10001},
    {'+', 5, 0b01010},
    {'-', 6, 0b100001},
    {'_', 6, 0b101100},
    {'"', 6, 0b010010},
    {'@', 6, 0b010110}
};

/*
   Return 1 if the given bit is set, 0 if it is not set

   Bit 0 is the least significant bit, bit 7 is the most.
   So with the value 0b1 bit 0 is set. With 0b10000000 bit 7 is set.
*/
bool testBit(uint8_t data, uint8_t bit_number) {
    return (data >> bit_number) & 1;
}

/*
   This function called once per MILLISECONDS_PER_TICK milliseconds
*/

void tick() {
    static uint8_t enable_output;
    static uint8_t output;
    static unsigned long counter;

    // If the current counter has a count left, decrement it
    if (counter != 0) {
        counter--;
    }

    // Now if the counter is exhausted, go find a new "command"
    if (counter == 0) {
        RingBufferClass::RBElement rb;

        if (RingBuffer.getData(rb, false)) {

            // Found a new command: load it up
            enable_output = rb.data;
            counter = rb.timestamp;
        } else {

            // No command available. Stop output and get it set so it will reliably
            // start on the correct half cycle.

            // Extra credit: Is this code going to put out exactly 'counter' full
            // cycles, or is it going to be off by a half cycle (or two or three)?
            enable_output = 0;
            output = 0;
        }
    }

    // Update the output and send it to the pin.
    output = output ^ enable_output;
    digitalWrite(LabKit.PIEZO, output);
}


void setup() {
    LabKit.begin(9600);
    pinMode(LabKit.PIEZO, OUTPUT);
    RingBuffer.init();
    // Initialize "tick timer"
    MsTimer2::set(MILLISECONDS_PER_TICK, tick);
    MsTimer2::start();
}

void loop() {
    if (Serial.available()) {
        char ch = Serial.read();
        ch = toupper(ch);
        Serial.write(ch);

        if (ch == ' ') {
            // silence after completed word
            RingBuffer.putData(SILENCE, MILLISECONDS_PER_CHARACTER_SILENCE, true);
            RingBuffer.putData(SILENCE, MILLISECONDS_PER_CHARACTER_SILENCE, true);
            return;
        }

        // TODO: if '+' then increase speed, if '-' decrease speed

        // C++ 2011 feature, I cannot use this: for (Letter i : morseData) {
        for (uint8_t i = 0; i < (sizeof(morseData) / sizeof(Letter)); i++) {
            if (morseData[i].ch == ch) {

                /* Match: create sound and silence "commands" to communicate to the
                   interrupt handler. If the ring buffer gets full, just wait
                   until it isn't.  */
                for (uint8_t q = 0; q < morseData[i].length; q++) {
                    RingBuffer.putData(SOUND, testBit(morseData[i].bits, q) == DIT ?
                                       MILLISECONDS_PER_DIT : MILLISECONDS_PER_DAH, true);
                    RingBuffer.putData(SILENCE, MILLISECONDS_PER_SILENCE, true);
                }

                // Last dit or dah has part of the silence and this is the remainder
                // for the character
                RingBuffer.putData(SILENCE, MILLISECONDS_PER_CHARACTER_SILENCE -
                                   MILLISECONDS_PER_SILENCE, true);
            }
        }
    }
}
