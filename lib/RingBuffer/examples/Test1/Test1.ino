/*
 * Test the ring buffer class. This is a first, rough test with the
 * "experimental" ring buffer class. Button press interrupts are buffered
 * and then unbuffered and reported. The state of the buffer is reported
 * twice and then the program hangs to allow inspection of serial output.
 *
 * Pressing buttons 1-3 up to RBSIZE times should show the button
 * numbers held in the ring buffer with a five second pause between the
 * two sets of serial output. The elapsed milliseconds of the presses are
 * also shown relative to first insertion. The next insertion point in the
 * buffer is shown by "<==" and the next extration point by "==>".
 *
 * This code also has the beginnings of a "switch press" debouncing library.
 *
 *  Copyright (c) 2017 Peter J. Soper
 *  MIT License (see "LICENSE" in this repository)
 */

#define LABKIT_DEBUG 1

#include <LabKit.h>
#include <RingBuffer.h>
#include <MsTimer2.h>
#include <SimplePinChange.h>

// Elapsed milliseconds at start of program execution

uint32_t startMillis;

// Move to LabKit class?

static const uint8_t BUTTON_1_ORD = 0;
static const uint8_t BUTTON_2_ORD = 1;
static const uint8_t BUTTON_3_ORD = 2;
static const uint8_t BUTTON_4_ORD = 3; // Not used by the code below

// Current state of the button pins
static uint8_t button_state[3];

// Mapping of buttons to pins. Should this go into the library with support
// for all four buttons? This is at least the second example program to use
// this kind of data.

static uint8_t button_pins[3] = {LabKit.BUTTON_1, LabKit.BUTTON_2,
                                 LabKit.BUTTON_3
                                };

// Switch states. An interrupt moves from the idle state, then after two
// "tick" time periods the switch pin is sampled again, and a valid switch
// press is detected or "noise" is ignored.

static const uint8_t BSTATE_IDLE = 0;  // nothing happening
static const uint8_t BSTATE_INITIAL_INTERRUPT = 1; // Initial interrupt
static const uint8_t BSTATE_TIMING_COUNT1 = 2; // first timing period
static const uint8_t BSTATE_TIMING_COUNT2 = 3; // second timing period

// Switch press interrupt handlers. These interrupts start the steps needed
// to put a switch press into the ring buffer.

void buttonOneInterrupt() {
    button_state[BUTTON_1_ORD] = BSTATE_INITIAL_INTERRUPT;
    detachInterrupt(digitalPinToInterrupt(LabKit.BUTTON_1));
}

void buttonTwoInterrupt() {
    button_state[BUTTON_2_ORD] = BSTATE_INITIAL_INTERRUPT;
    detachInterrupt(digitalPinToInterrupt(LabKit.BUTTON_2));
}

// With the pin change interrupt ignore low to high transitions

void buttonThreeInterrupt() {
    button_state[BUTTON_3_ORD] = BSTATE_INITIAL_INTERRUPT;
    SimplePinChange.detach(LabKit.BUTTON_3);
}

// Periodic timer interrupt to manage the switch press state machine

void tick() {

    // For all three buttons
    for (uint8_t i = 0; i < 3; i++) {

        // Determine current and next states with appropriate actions
        switch (button_state[i]) {

        // Nothing happening: keep doing nothing
        case BSTATE_IDLE:
            break;

        // Switch interrupt happened: start first timing period
        case BSTATE_INITIAL_INTERRUPT:
            button_state[i] = BSTATE_TIMING_COUNT1;
            break;

        // First timing period happened: start second timing period
        case BSTATE_TIMING_COUNT1:
            button_state[i] = BSTATE_TIMING_COUNT2;
            break;

        // Second timing period happened: determine debounced switch state
        case BSTATE_TIMING_COUNT2:

            // Buffer switch if its pin is (active) low as a bonafide press
            if (!digitalRead(button_pins[i])) {

                // There is a valid, debounced switch press detected. Add it to the
                // buffer with a 1-origin button number to match the switches
                // labeling.

                RingBuffer.putData(i + 1, millis() - startMillis);
            }

            // Next state is idle
            button_state[i] = BSTATE_IDLE;

            // Reenable interrupts for the button's pin. Ugh!
            switch (i) {
            case 0:
                attachInterrupt(digitalPinToInterrupt(LabKit.BUTTON_1),
                                buttonOneInterrupt, FALLING);
                break;
            case 1:
                attachInterrupt(digitalPinToInterrupt(LabKit.BUTTON_2),
                                buttonTwoInterrupt, FALLING);
                break;
            case 2:
                SimplePinChange.attach(LabKit.BUTTON_3, buttonThreeInterrupt);
                break;
            }
            break;
        }
    }
}

// Set up the hardware

void setup() {
    // Set up kit hardware
    LabKit.begin();

    // Make timer 2 cause the tick function to be called every 10 milliseconds
    MsTimer2::set(10, tick);
    MsTimer2::start();

    // Put an interrupt on each of the first three buttons. The interrupt
    // handler functions will insert button numbers and millisecond time
    // stamps into the ring buffer.

    attachInterrupt(digitalPinToInterrupt(LabKit.BUTTON_1), buttonOneInterrupt,

                    CHANGE);
    attachInterrupt(digitalPinToInterrupt(LabKit.BUTTON_2), buttonTwoInterrupt,

                    CHANGE);
    SimplePinChange.attach(LabKit.BUTTON_3, buttonThreeInterrupt);

    // record the starting time in elapsed milliseconds

    startMillis = millis();
}

// Display the ring buffer contents, insertion and extraction points twice
// and then hang to allow inspection.

void loop() {
    int count = 1;
    do {
        Serial.println();
        RingBuffer.showData();
        delay(5000);
    } while (count--);
    while (1)
        ;
}
