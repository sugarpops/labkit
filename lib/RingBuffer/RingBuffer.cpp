/*
 * RingBuffer.cpp - Implementation of RingBuffer.h API
 *
 * See RingBuffer.h for API details.
 *
 * Copyright (c) 2017 Pete Soper
 * MIT License (see LICENSE in this repository)
 *
 * IMPLEMENTATION CHANGES:
 *   <change date> <change commit message>
 * IMPLEMENTATON TODO:
 *   <entry date> <todo item description and/or issue reference>
 * IMPLEMENTATON BUGS AND FEATURE REQUESTS:
 *   <report date> <bug/feature description or issue reference>
 */

#include <RingBuffer.h>

RingBufferClass RingBuffer;

/*
 * Return the size (number of elements) of the buffer
 */

RingBufferClass::RBIndex RingBufferClass::available() {
    return entries;
}

/*
 * Empty a buffer. This throws away/"flushes" any contents and
 * prepares the buffer for use. It would be useful, for example, if a game
 * needs to be started and any "old" switch presses need to be forgotten.
 */

void RingBufferClass::init() {
    entries = next_write = next_read = 0;
}

/*
 * Copy the least recently inserted item into the buffer referenced and
 * return true or else false to indicate the ring buffer is empty.
 */

bool RingBufferClass::peekOldest(RBElement &buf) {
    uint8_t saved_sreg = SREG;
    cli();
    if (entries) {
        uint8_t temp = (next_read - 1) % RB_SIZE;
        buf.data = buffer[temp].data;
        buf.timestamp = buffer[temp].timestamp;
        SREG = saved_sreg;
        return true;
    } else {
        SREG = saved_sreg;
        return false;
    }
}

/*
 * Copy the most recently inserted item into the buffer referenced and
 * return true or else false to indicate the ring buffer is empty.
 */

bool RingBufferClass::peekNewest(RBElement &buf) {
    uint8_t saved_sreg = SREG;
    cli();
    if (entries) {
        uint8_t temp = (next_write - 1) % RB_SIZE;
        buf.data = buffer[temp].data;
        buf.timestamp = buffer[temp].timestamp;
        SREG = saved_sreg;
        return true;
    } else {
        SREG = saved_sreg;
        return false;
    }
}

/*
 * Get the next available ring buffer data structure items and return true.
 * If no data is available then if wait_if_empty is true the program will
 * wait in a loop for entries to become nonzero. If no data is available and
 * wait is false then return false. The "available" function can be used
 * to see if this function might wait to avoid having the program getting
 * "stuck" waiting for something that may never happen.
 */

bool RingBufferClass::getData(RBElement &p, bool wait_if_empty) {
    /*
     * If variable "entries" is not declared "volatile" the compiler is allowed
     * to make code that only loads entries into a register once, making the code
     * not see changes an interrupt handler makes to the the value of the
     * variable (i.e. it's memory contents). This will cause very confusing
     * program failures as this code will loop forever, even though some other
     * test of the variable clearly shows it has a different value than it seems
     * to in this loop. The "volatile" keyword tells the compiler to make
     * code to load entries from memory *every* time there is a reference, so
     * a change that an interrupt handler made will be seen.
     */

    while (!entries) {
        if (wait_if_empty) {
            continue;
        } else {
            return false;
        }
    }

// Don't allow interruption. This is harmless if already inside an
    // interrupt handler because the previous state is preserved. This is
    // THE BIG REASON why this sequence is used for critical sections instead
    // of just cli()/sei().

    uint8_t saved_sreg = SREG;
    cli();

    p.data = buffer[next_read].data;
    p.timestamp = buffer[next_read++].timestamp;
    entries -= 1;
    next_read %= RB_SIZE;

    // end of critical section
    SREG = saved_sreg;

    return true;
}

/*
 * Store the given data into the next element of the ring buffer
 * and return true. If no room for next element wait until there is
 * room or return false.
 */

bool RingBufferClass::putData(uint8_t data, uint32_t timestamp,
                              bool wait_if_full) {
    while (entries == RB_SIZE) {
        if (wait_if_full) {
            continue;
        } else {
            return false;
        }
    }

    // Don't allow interruption.

    uint8_t saved_sreg = SREG;
    cli();

    buffer[next_write].data = data;
    buffer[next_write++].timestamp = timestamp;
    next_write %= RB_SIZE;
    entries += 1;

    // end of critical section
    SREG = saved_sreg;

    return false;
}

/*
 * Print the ring buffer contents.
 */

void RingBufferClass::showData() {
    // Disable interrupts and make a copy of everything needed.

    uint8_t saved_sreg = SREG;
    cli();

    uint8_t copied_entries = entries;
    uint8_t copied_next_write = next_write;
    uint8_t copied_next_read = next_read;
    RBElement copied_buffer[RB_SIZE];
    for (RBIndex i = 0; i < RB_SIZE; i++) {
        copied_buffer[i].data = buffer[i].data;
        copied_buffer[i].timestamp = buffer[i].timestamp;
    }

    // End of critical section
    SREG = saved_sreg;

    for (uint8_t i = 0; i < RB_SIZE; i++) {

        // subscript
        Serial.print(i);
        Serial.print(" ");

        // data
        Serial.print(copied_buffer[i].data);
        Serial.print(" ");

        // milliseconds timestamp
        Serial.print(copied_buffer[i].timestamp);

        // Append characters to show next insertion or read position

        if ((copied_entries < RB_SIZE) && (i == copied_next_write)) {
            Serial.print(" <=");
        }

        if ((copied_entries > 0) && (i == copied_next_read)) {
            Serial.print(" =>");
        }

        Serial.println();
    }
}

