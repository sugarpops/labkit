/*
 * Tick.cpp - Implementation of Tick.h API
 *
 * This is an initial implementaton of a periodic callback mechanism using
 * either a clock chip or Timer2. This code is NOT FULLY TESTED.
 *
 * See Tick.h for API details.
 *
 * Copyright (c) 2017 Pete Soper
 * MIT License (see LICENSE in this repository)
 *
 * IMPLEMENTATION CHANGES:
 *   <change date> <change commit message>
 * IMPLEMENTATON TODO:
 *   <entry date> <todo item description and/or issue reference>
 * IMPLEMENTATON BUGS AND FEATURE REQUESTS:
 *   <report date> <bug/feature description or issue reference>
 */

#include <Tick.h>

#ifdef USE_RTC
#include <Wire.h>

M41T6X rtc;
#endif

/*
 * Start the tick clock
 */

void Tick::begin(voidFuncPtr func) {
#ifdef USE_RTC
    Wire.begin();
    // 1/512 or .00195312 seconds between interrupts
    rtc.setSquareWaveFrequency(8);
    rtc.begin();
    SimplePinChange.attach(A2, func)
#else
    // 1/500 or seconds between interrupts
    MsTimer2.set(2, func);
    MsTimer2.start();
#endif
}
