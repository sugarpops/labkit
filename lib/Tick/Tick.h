/*
 * Tick.h - Periodic interrupt callback API
 *
 * This code provides a very simple periodic interrupt. It is starting life
 * as a wrapper for MsTimer2 but will change to a layer on the M41T62 RTC
 * driver's squarewave source. NOT FULLY TESTED.
 *
 * In operation the periodic interrupt makes a single callback to the Task
 * interface which can then be used for various individually timed or
 * periodic intervals and actions tied to them.
 * See public interfaces below for details.
 *
 * Copyright (c) 2017 Pete Soper
 *
 * MIT License (see LICENSE in this repository)
 *
 * API CHANGES:
 *   <change date> <API interface syntax/semantic change commit message>
 * API TODO:
 *   <todo entry date> <planned interface change description and/or issue ref>
 * API BUGS AND FEATURE REQUESTS:
 *   <report date> <API bug description and/or issue reference>
 */

// Arduino specific defines

#ifndef TICK_H

#define TICK_H

#ifdef USE_RTC
#include <M41T6X.h>
#include <SimplePinChange.h>
#else
#include <MsTimer2.h>
#endif

typedef void (*voidFuncPtr)();

namespace Tick {
// Initialize: Flush any old state and prepare for new scheduling,
// calling func with each interrupt.
    void begin(voidFuncPtr func);
};

extern TickClass Tick;

#endif // TICK_H
