/*
 * Write a page of flash memory, returning zero (OK) if it worked, a nonzero
 * error code if it didn't. It is up to the caller to ensure interrupts are
 * disabled while this function is executing.
 *
 * Copyright 2017 Pete Soper
 * MIT license (see LICENSE in this repo)
 */

#define FLASH_PAGE_SIZE 128// Atmega328 flash page size in bytes

#define __FLASH_OK 0       // normal completion
#define __FLASH_ALIGN 1    // flash address not page aligned
#define __FLASH_ERASE 2    // erase failure
#define __FLASH_INVALID 3  // flash doesn't match buffer contents

unsigned char flashPage(unsigned char buffer[], unsigned int address);
