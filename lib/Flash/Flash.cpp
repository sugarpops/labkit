/*
 * Write a page of flash memory, returning zero (OK) if it worked, a nonzero
 * error code if it didn't. It is up to the caller to ensure interrupts are
 * disabled while this function is executing.
 *
 * This code has to be linked into the bootloader section of flash memory.
 * The BOOTSZ0 an BOOTSZ1 fuse bits set the size of the section and this
 * function has to reside within whatever size is set.
 *
 * Copyright 2017 Pete Soper
 * MIT license (see LICENSE in this repo)
 */


#include <Flash.h>
#include <avr/boot.h>
#include <avr/pgmspace.h>

__attribute__ ((section (".shellflash"))) unsigned char flashPage(unsigned char buffer[], unsigned int address) {

    // Erase the page

    __boot_page_erase_normal(address);

    // Wait for SPM to finish

    boot_spm_busy_wait();

    // Confirm page is erased

    for (unsigned int offset = 0; offset < FLASH_PAGE_SIZE; offset++) {
        if (pgm_read_byte_near(address + offset) != 0xff) {
            return __FLASH_ERASE;
        }
    }

    // Copy bytes into the AVR chip's programming buffer

    // The fill routine takes a byte address pointing to a word

    unsigned int *word_p = (unsigned int *) address;

    for (unsigned int offset = 0; offset < FLASH_PAGE_SIZE; offset += 2) {
        unsigned int temp;
        temp = buffer[offset];
        temp |= buffer[offset + 1] << 8;
        __boot_page_fill_normal((unsigned int)(void *)word_p++, temp);
    }

    // Now write the internal buffer to flash and wait for it to finish

    __boot_page_write_normal((unsigned int)(void *)address);
    boot_spm_busy_wait();

    // Make sure access to the app section is enabled
    __boot_rww_enable();

    // Check to see if the bytes are correct.

    for (unsigned int offset = 0; offset < FLASH_PAGE_SIZE; offset++) {
        if (buffer[offset] != pgm_read_byte_near(address + offset)) {
            return __FLASH_INVALID;
        }
    }

    return __FLASH_OK;
}
