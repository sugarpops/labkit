/*
 * Lab Kit Library
 *
 * This code provides an application programming interface (API) to the
 * lab kit hardware as well as utility routines.
 *
 * Copyright (c) 2017 Pete Soper & Ben Goldberg
 * MIT License (see LICENSE in this repository)
 *
 * CHANGES
 *  10/9/2017 - Rev 2 hardware
 *  10/13/2017 - More utility functions
 *  10/23/2017 - Rev 3 hardware: TFT display/touchscreen
 *  11/30/2017 - Added helpers
 * NOTES
 *  This should stay hardware-centric, but general helper functions don't have
 *  a better place to live yet.
 */

// Arduino specific defines

#ifndef LABKIT_H

#define LABKIT_H

#define __LABKIT_VERSION 1

#include <Arduino.h>

class LabKitClass {
    public:
        // The "1-4" pushbutton switch pin numbers (note: active low)

        static const uint8_t BUTTON_1 = 2;
        static const uint8_t BUTTON_2 = 3;
        static const uint8_t BUTTON_3 = 7;
        static const uint8_t BUTTON_4 = 8;

        // An array to map an ordinal to a button's pin

        static const uint8_t button_to_pin[4];

        // The ordinal value names that index into the array above.

        static const uint8_t BUTTON_1_ORD = 0;
        static const uint8_t BUTTON_2_ORD = 1;
        static const uint8_t BUTTON_3_ORD = 2;
        static const uint8_t BUTTON_4_ORD = 3;

        // Accessors for the pins: returns current pin value (HIGH, or LOW)

        uint8_t getButton(uint8_t button_ordinal);

        uint8_t getButton1();
        uint8_t getButton2();
        uint8_t getButton3();
        uint8_t getButton4();

        // The TFT Display overloads the LED pins
        // TODO: clean up this mess

        static const uint8_t TF_CS = 4;
        static const uint8_t SD_CS = TF_CS;
        static const uint8_t TFT_CS = 5;
        static const uint8_t TFT_DC = 6;

        // The LED pin numbers

        static const uint8_t RED_LED = 6;
        static const uint8_t GREEN_LED = 5;
        static const uint8_t BLUE_LED = 4;

        // An array to map an ordinal to an LED's pin

        static const uint8_t led_to_pin[3];

        // The ordinal value names that index into the array above.

        static const uint8_t RED_LED_ORD = 0;
        static const uint8_t GREEN_LED_ORD = 1;
        static const uint8_t BLUE_LED_ORD = 2;

        // Set LED by ordinal to value passed

        void setLEDByOrdinal(uint8_t ordinal, uint8_t value);

        // Flash all LEDs

        void flashLEDs(uint8_t number_of_flashes = 3,
                       uint16_t milliseconds_between_flashes = 100);

        // The Piezo speaker pin

        static const uint8_t PIEZO = 9;

        // The three SPI clock and data pins are defined
        // here and reserved for use with an SPI peripheral.

        static const uint8_t SPI_MOSI = 11;
        static const uint8_t SPI_MISO = 12;
        static const uint8_t SPI_SCK = 13;

        // The resistive touchscreen uses four analog pins
        // TODO straighten out this mess

        static const uint8_t TS_ONE = A0;
#define YM 14
        static const uint8_t TS_TWO = A1;
#define XM A1
        static const uint8_t TS_THREE = A2;
#define YP A2
        static const uint8_t TS_FOUR = A3;
#define XP 17


        // RTC pin number for square wave input

        static const uint8_t RTC_SQW = 10;

        // Serial parameters

        static const uint32_t DEFAULT_SERIAL_RATE = 9600;

        // Hardware initialization

        void begin(uint32_t serial_speed = DEFAULT_SERIAL_RATE);

        // Convenience functions

        // Beep the piezo speaker. NOTE: this is currently dependent on tone()
        // and that function is dependent on the Arduino chip's timer2 function
        // for the types of Arduino this kit is designed for. So if, for example
        // MsTimer2 is used, this function will not work.

        void beep(uint16_t frequency = 1000, uint16_t milliseconds = 250);

};

extern LabKitClass LabKit;

// Helper functions

// Set a pin's PWM hardware clock frequency. See comments in .cpp file.

extern void setPwmFrequency(int pin, int divisor);

// Print bytes as hex and ascii. The address parameter is prefixed as a
// four digit hex value. Nonprintable characters are shown as periods.
// By default print newlines or always after each 16 characters.

void printBytes(uint16_t address, uint8_t array[], uint16_t start,
                uint16_t end, bool print_newline = true);

// Like printBytes above, but for an entire array of the given length.

void printBytes(uint16_t address, uint8_t array[], uint16_t length,
                bool print_newline = true);

// Older versions of the IDE don't have this. The <= 100 is a guess.

#ifndef digitalPinToInterrupt

// TODO: CURRENTLY ONLY WORKS FOR SOME ATMEGA CHIPS!
#define digitalPinToInterrupt(p) (p == 2 ? 0 : 1)

#endif

#endif // LABKIT_H
