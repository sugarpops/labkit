/*
 * Test2.ino -- This program does the same thing as Test1.ino,
 * but it has no loop() code and relies on interrupts.
 *
 * Copyright (c) 2017-2018 Ben Goldberg
 * MIT License (see LICENSE in this repository)
 * CHANGES:
 *   4/3/18 Updated header comment
*/
// library includes
#include <LabKit.h>
#include <SimplePinChange.h>

// Called when any button changes state. Make LEDs reflect button presses:
// pressed means "ON".

void isr() { // This runs when buttons 1, 2, or 3 are pressed.
    uint8_t index = 0;
    while (index < sizeof(LabKit.led_to_pin)) {
        // Switch inputs are active low (pulled high when switch open)
        digitalWrite(LabKit.led_to_pin[index],
                     ! digitalRead(LabKit.button_to_pin[index]));
        index = (index + 1);
    }
}

void setup() {
    // Configure hardware
    LabKit.begin();

    // Set up interrupt handlers for all three buttons used in this program
    attachInterrupt(digitalPinToInterrupt(LabKit.BUTTON_1), isr, CHANGE);
    attachInterrupt(digitalPinToInterrupt(LabKit.BUTTON_2), isr, CHANGE);
    SimplePinChange.attach(LabKit.BUTTON_3, isr);
}

void loop() {
    // put your main code here, to run repeatedly:

}
