/*
 * LabKitShell.ino
 * A basic shell for the Lab Kit.
 * Copyright (c) 2017 Ben Goldberg
 * Experimental features by Pete Soper
 */
#include <SPI.h>
#include <SD.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>

/* Below code copied from spitftbitmap.ino, an example from the Adafruit ILI9341 Library */

__attribute__ ((section (".eeprom"))) const char *trythis = "This is a test";

/***************************************************
  This is our Bitmap drawing example for the Adafruit ILI9341 Breakout and Shield
  ----> http://www.adafruit.com/products/1651
  Check out the links above for our tutorials and wiring diagrams
  These displays use SPI to communicate, 4 or 5 pins are required to
  interface (RST is optional)
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!
  Written by Limor Fried/Ladyada for Adafruit Industries.
  MIT license, all text above must be included in any redistribution
 ****************************************************/

#define TFT_DC 6
#define TFT_CS 5
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

#define SD_CS 4

/* End of copied code */

#define MAX_COMMAND_STRING_LENGTH 7 // ONE MORE THAN ONE WOULD EXPECT
const char *cmds[] = {"ls", "cd", "cat", "edit", "mkdir", "draw", "rm"};

#define MAX_ARG_STRING_LENGTH 15 // 8.3 filename plus null + 1: no pathnames yet!
char cmd_temp[MAX_COMMAND_STRING_LENGTH];
char arg_temp[MAX_ARG_STRING_LENGTH];
#define ILLEGAL_COMMAND 255

#define NUMBER_OF_COMMANDS 7
#define SD_CS 4

String currentDir = "/";

void ls() {
    Serial.println("Name\tSize");
    File dir = SD.open(currentDir);
    dir.seek(0);
    while (1) {
        File f = dir.openNextFile();
        if (!f) {
            break;
        }
        Serial.print(f.name());
        if (f.isDirectory()) {
            Serial.println("\tDIRECTORY");
        } else {
            Serial.print("\t");
            Serial.println(f.size(), DEC);
        }
        f.close();
    }
    dir.seek(0);
    dir.close();
}

void cd(String arg) {
    // Todo: support ".." to move back out and "empty cd goes back to /"
    if (arg == "") {
        Serial.println("Correct usage: cd <folder path>");
        return;
    }
    if (SD.exists(currentDir + arg)) {
        currentDir = currentDir + arg;
    } else {
        currentDir = arg;
    }
    if (currentDir[currentDir.length() - 1] != '/') {
        currentDir += "/";
    }
    Serial.println("Changed directories to " + currentDir);
}

void cat(String arg) {
    String fname = currentDir + arg;
    if (!(SD.exists(fname))) {
        Serial.println("Correct usage: cat <file name>");
        return;
    } else {
        File f = SD.open(fname);
        if (f) {
            while (f.available()) {
                Serial.write(f.read());
            }
            f.close();
        }
    }
}

void edit(String arg) { // <backslash> to stop editing
    File f = SD.open(currentDir + arg, FILE_WRITE);
    if (!f) {
        return;
    }
    if (f.read()) {
        f.seek(0);
        while (f.available()) {
            Serial.write(f.read());
        }
    }
    f.seek(0);
    while (1) {
        while (!(Serial.available()));
        char ch = Serial.read();
        if (ch == '\\') {
            f.close();
            return;
        } else {
            Serial.write(ch);
            f.write(ch);
        }
    }
}

void mkdir(String arg) {
    String path = currentDir + arg;
    SD.mkdir(path);
}

void draw(String arg) {
    char argy[arg.length() + 1];
    arg.toCharArray(argy, arg.length() + 1);
    if (SD.exists(argy)) {
        tft.fillScreen(ILI9341_WHITE);
        tft.bmpDraw((char *)argy, 0, 0);
    } else {
        String path = currentDir + argy;
        char arg[path.length() + 1];
        path.toCharArray(arg, path.length() + 1);
        if (SD.exists(arg)) {
            tft.fillScreen(ILI9341_WHITE);
            tft.bmpDraw((char *)arg, 0, 0);
        } else {
            Serial.println("File not found.");
        }
    }
}

void rm(String arg) {
    String f = currentDir + arg;
    SD.remove(f);
}

void execFromSD(String cmd, String arg) {
    File f = SD.open(currentDir + cmd + ".HEX");
    Serial.println(String("Executing ") + f.name());
    if (SD.exists(f.name())) {
        while (f.available()) {
            /*
             * TODO:
             *  Copy arg(s) to EEPROM for the application to collect them.
             *  Flash the memory "under us" with the binary derived from the hex
             *  file, EXCEPT merge/register interrupt vectors as they're seen.
             *  Call into the "application" entrypoint (computed from vector 1).
             *  The application can jump to 0 to return to the shell (now) or
             *  call "BIOS function exit" (later).
             */
            Serial.write(f.read()); // replace this with writing f.read() to flash
        }
    } else {
        Serial.println("File not found");
    }
    f.close();
}

void execCMD(int cmdNum, String args) {
    switch (cmdNum) {
    case 0:
        ls();
        break;
    case 1:
        cd(args);
        break;
    case 2:
        cat(args);
        break;
    case 3:
        edit(args);
        break;
    case 4:
        mkdir(args);
        break;
    case 5:
        draw(args);
        break;
    case 6:
        rm(args);
        break;
    }
}

void printPrompt() {
    Serial.print("arduino@LabKit ");
    Serial.print(currentDir);
    Serial.print(" $");
}

void setup() {
    // put your setup code here, to run once:
    tft.begin();
    Serial.begin(9600); // blazing fast for UART
    Serial.print("Initializing SD card");
    while (!SD.begin(SD_CS)) {
        Serial.print(".");
    }
    Serial.println();
    Serial.println("Done!");
}

/*
 * Read a line of characters from serial input until "enter" is seen.
 * Recognizes "backspace" to erase a character and "control C" to abort
 * the line and repeat the command line prompt. Only buffers printable
 * characters. Support for a single command line argument. The command and
 * argument buffer addresses and max lengths have to be passed in, as this
 * code is headed for one of the libraries.
 *
 * When a space is seen it is considered the delimiter between a command and
 *
 */

void readLine(char *cmd_buffer, uint8_t cmd_buffer_length,
              char *arg_buffer, uint8_t arg_buffer_length, void (*prompt)()) {

    char *p = cmd_buffer;         // Start with command
    uint8_t buffer_length = cmd_buffer_length;// Start with command buffer length
    uint8_t index = 0;            // Next buffer index to store to
    uint8_t blank_count = 0;      // K L U D G E ! !
    char ch;              // Current input character

    *p = 0;               // Make sure empty strings are returned
    arg_buffer[0] = 0;

    /*
     * Loop until enter is seen
     */

    do {
        while (! Serial.available()) {  // Avoids idiotic -1 int return by read
        }
        ch = Serial.read();         // next character
#if 0
        Serial.println();           // debug code
        Serial.print("read ");
        if (isprint(ch)) {
            Serial.print(ch);
        } else {
            Serial.print('.');
        }
        Serial.print(" ");
        Serial.println(ch, HEX);
#endif

        // Handle line editing characters
        switch (ch) {

        case ' ':             // space: switch to arg buffer
            Serial.write(' ');
            blank_count += 1;
            if (buffer_length != arg_buffer_length) {  // ignore redundant space
                cmd_buffer[index] = 0; // terminate command string
                buffer_length = arg_buffer_length; // switch buffers
                p = arg_buffer;
                index = 0;         // restart insertion point
            }
            break;

        case 0x7f:            // backspace
            if (index > 0) {     // Nothing to do if at line start
                Serial.write('\r');
                prompt();                  // BRUTE FORCE: blanks better, except
                index = index - 1;         // we don't know how many to put out
                if (p == arg_buffer) {
                    Serial.print(cmd_buffer);
                    for (int i = 0; i < blank_count; i++) {
                        Serial.write(' ');
                    }
                }
                for (uint8_t i = 0; i < index; i++) {
                    Serial.write(p[i]);
                }
                Serial.print(" ");         // Erase the "deleted character"
                Serial.write('\r');        // Now do it all again
                prompt();
                if (p == arg_buffer) {
                    Serial.print(cmd_buffer);
                    for (int i = 0; i < blank_count; i++) {
                        Serial.write(' ');
                    }
                }
                for (uint8_t i = 0; i < index; i++) {
                    Serial.write(p[i]);
                }
            }
            break;

        case 3:               // CONTROL C to "abort" line
            Serial.println();     // for some reason '\c' doesn't work!
            prompt();
            index = 0;
            break;

        case '\r':            // enter (carriage return)
            Serial.println();
            break;

        case '?':             // Give me the cheat sheet!
            Serial.println();
            for (uint8_t i = 0; i < NUMBER_OF_COMMANDS; i++) {
                Serial.println(cmds[i]);
            }
            index = 0;
            break;

        default:              // Default stored if printable
            if (isprint(ch)) {
                p[index++] = ch;
                Serial.write(ch);
            }
        }

        // A too long line is truncated with extreme prejudice
    } while (index < (buffer_length - 2) && (ch != '\r'));

    p[index] = 0;
}

#define MAX_LINE_LENGTH 32
void loop() {
    while (true) {
        uint8_t cmd_number = ILLEGAL_COMMAND;
        printPrompt();

        /*
         * Use a local block here so the array memory is released before
         * a command is executed.
         */

        readLine(cmd_temp, MAX_COMMAND_STRING_LENGTH, arg_temp,
                 MAX_ARG_STRING_LENGTH, printPrompt);
        for (int i = 0; i < NUMBER_OF_COMMANDS; i++) {
            if (strcmp(cmds[i], cmd_temp) == 0) {
                cmd_number = i;
                break;
            }
        }

        String cmd = cmd_temp; // WHAT IS THIS ACTUALLY DOING?? Feels evil.
        String arg = arg_temp; // WHAT IS THIS ACTUALLY DOING?? Feels evil.
        if (cmd_number != ILLEGAL_COMMAND) {
            execCMD(cmd_number, arg);
        } else {
            execFromSD(cmd, arg);
        }
    }
}
