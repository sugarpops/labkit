/***************************************************************************************************************************
   This program runs the memory game Simon on the Lab Kit.

   Button    LED
     4       Blue
     3       Green
     2       Red

   How to play:
   Simon is a fun game where the LEDs light up in a sequence and you have to repeat that sequence using the buttons. (See above for buttons to LEDs.)

   When you press a button, the corresponding LED lights up.

   To check your answer after you think you have matched the sequence, press Button 1.

   If you repeated the sequence correctly, the program increases the sequence length by one new color.

   If you made a mistake, the LEDs flash the number of times that you got the sequence right.

   If you type in a sequence longer than 15, the arrays overflow because they can only hold 15 items.

   Also, you are timed in this version.

   Have fun playing!

   Copyright (c) 2017 Ben Goldberg

   MIT License (see LICENSE in this repository)

   CHANGES
    10/9/2017 - Rev 2 hardware, use the beeper (Pete)
**************************************************************************************************************************/
// library include
#include <LabKit.h>
const uint8_t red = 0; // a 0 signifies red
const uint8_t green = 1; // a 1 signifies green
const uint8_t blue = 2; // a 2 signifies blue
bool pressed = LOW; // the buttons are set up as INPUT_PULLUP
uint8_t sequence[15] = { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5}; // can only hold fifteen items
uint8_t guessed[15] = { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5}; // can only hold fifteen items
uint8_t counter = 0; // counters for adding to arrays
uint8_t counterSeq = 0;
uint8_t score = 0; // your score
unsigned long start = 0;
uint8_t secondsPerSeq = 3; // how many seconds you get per item in sequence

void addToSequence() { // add a random # to the sequence to be displayed
    sequence[counterSeq] = random(0, 3);
    counterSeq++;
}

void displaySequence() { // display the sequence
    for (int i = 0; i < 15; i++) {
        switch (sequence[i]) {
        case red:
            digitalWrite(LabKit.BLUE_LED, HIGH);
            delay(500);
            digitalWrite(LabKit.BLUE_LED, LOW);
            delay(500);
            break;
        case green:
            digitalWrite(LabKit.GREEN_LED, HIGH);
            delay(500);
            digitalWrite(LabKit.GREEN_LED, LOW);
            delay(500);
            break;
        case blue:
            digitalWrite(LabKit.RED_LED, HIGH);
            delay(500);
            digitalWrite(LabKit.RED_LED, LOW);
            delay(500);
            break;
        default:
            digitalWrite(LabKit.RED_LED, LOW);
            digitalWrite(LabKit.GREEN_LED, LOW);
            digitalWrite(LabKit.BLUE_LED, LOW);
            break;
        }
    }
    start = millis(); // reset timer
}

void compare() { // check if your guess and the sequence are equal
    while (LabKit.getButton1() == pressed);
    bool ok = true;
    for (int seq = 0; seq < 15; seq++) {
        Serial.println(sequence[seq]);
        Serial.println(guessed[seq]);
        if ((sequence[seq] == guessed[seq]) and (guessed[seq] != 5) and (sequence[seq] != 5)) {
            //ok = true;
        } else if (sequence[seq] != guessed[seq]) {
            ok = false;
        } else {

        }
    }
    if (ok) { // you got it right. good job.
        Serial.println("okay");
        LabKit.beep();
        score += 1; // up the score
        for (int i = 0; i < counter; i++) {
            guessed[i] = 5;
            counter = 0;
        }
        digitalWrite(LabKit.RED_LED, LOW);
        digitalWrite(LabKit.GREEN_LED, LOW);
        digitalWrite(LabKit.BLUE_LED, LOW);
        addToSequence();
        displaySequence();
    } else {
        Serial.println("no");
        LabKit.beep();
        delay(250);
        LabKit.beep();
        delay(250);
        LabKit.beep();
        for (int i = 1; i <= score; i++) {
            digitalWrite(LabKit.RED_LED, HIGH);
            digitalWrite(LabKit.GREEN_LED, HIGH);
            digitalWrite(LabKit.BLUE_LED, HIGH);
            delay(500);
            digitalWrite(LabKit.RED_LED, LOW);
            digitalWrite(LabKit.GREEN_LED, LOW);
            digitalWrite(LabKit.BLUE_LED, LOW);
            delay(500);
        }
        while (1);
    }
    start = millis(); // reset timer
}

void checkButtons() { // poll the buttons
    if (start != 0 and millis() - start > ((counterSeq - 1) * secondsPerSeq * 1000) - 1) { // for every item in sequence, you get secondsPerSeq seconds to press the button
        for (int i = 0; i < 15; i++) {
            Serial.println("no");
            for (int i = 1; i <= score; i++) {
                digitalWrite(LabKit.RED_LED, HIGH);
                digitalWrite(LabKit.GREEN_LED, HIGH);
                digitalWrite(LabKit.BLUE_LED, HIGH);
                delay(500);
                digitalWrite(LabKit.RED_LED, LOW);
                digitalWrite(LabKit.GREEN_LED, LOW);
                digitalWrite(LabKit.BLUE_LED, LOW);
                delay(500);
            }
            while (1);
        }
    }
    if (LabKit.getButton4() == pressed) { // red
        Serial.println("red");
        digitalWrite(LabKit.RED_LED, HIGH);
        delay(500);
        digitalWrite(LabKit.RED_LED, LOW);
        guessed[counter] = red; // add red to your guess
        counter++;
        while (LabKit.getButton4() == pressed); // wait till button is released
    }
    if (LabKit.getButton3() == pressed) { // green
        Serial.println("green");
        digitalWrite(LabKit.GREEN_LED, HIGH);
        delay(500);
        digitalWrite(LabKit.GREEN_LED, LOW);
        guessed[counter] = green; // add green to your guess
        counter++;
        while (LabKit.getButton3() == pressed); // wait till button is released
    }
    if (LabKit.getButton2() == pressed) { // blue
        Serial.println("blue");
        digitalWrite(LabKit.BLUE_LED, HIGH);
        delay(500);
        digitalWrite(LabKit.BLUE_LED, LOW);
        guessed[counter] = blue; // add blue to your guess
        counter++;
        while (LabKit.getButton2() == pressed); // wait till button is released
    }
    if (LabKit.getButton1() == pressed) { // check
        Serial.println("check");
        compare();
        while (LabKit.getButton1() == pressed); // wait till button is released
    }
}

void setup() {
    // put your setup code here, to run once:
    LabKit.begin(); // initialize hardware
    randomSeed(micros() + analogRead(A0)); // make the random numbers actually random
    addToSequence(); // start off by having one thing in the sequence
    displaySequence(); // and displaying the sequence
}

void loop() {
    // put your main code here, to run repeatedly:
    checkButtons(); // poll the buttons
    if (start != 0 and millis() - start > ((counterSeq - 1) * secondsPerSeq * 1000) - 1) { // for every item in sequence, you get secondsPerSeq seconds to press the button
        Serial.println("no");
        for (int i = 1; i <= score; i++) {
            digitalWrite(LabKit.RED_LED, HIGH);
            digitalWrite(LabKit.GREEN_LED, HIGH);
            digitalWrite(LabKit.BLUE_LED, HIGH);
            delay(500);
            digitalWrite(LabKit.RED_LED, LOW);
            digitalWrite(LabKit.GREEN_LED, LOW);
            digitalWrite(LabKit.BLUE_LED, LOW);
            delay(500);
        }
        while (1);
    }
}
