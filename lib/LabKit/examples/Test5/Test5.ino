/*
 * Lab Kit Test Program 5
 *
 * This program exercises the hardware.
 * Incomplete (not even 'initial')
 *
 * Expected behavior:
 *
 *  * After reset, no LEDs lit
 *  * If button '1" pressed, the red LED lights
 *  * If button '2" pressed, the green LED lights
 *  * If button '3" pressed, the blue LED lights
 *
 * Any combination of button presses should be supported
 *
 *
 * Copyright (c) 2017 Pete Soper
 * MIT License (See LICENSE in this repository)
 *
 * CHANGES
 */

// Arduino specific defines

#include <LabKit.h>

void setup() {
    // Enable hardware
    LabKit.begin();
}

// The "main program" endlessly samples the switches and lights the LED
// corresponding to any switch found to be pressed.

void loop() {
    for (uint8_t i = 0; i < 4; i++) {
        LabKit.beep(1000 + (i * 200), 200);
    }
    LabKit.flashLEDs(3, 250);
}

