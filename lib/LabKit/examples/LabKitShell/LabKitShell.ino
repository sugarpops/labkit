/*
 * LabKitShell.ino -- A basic shell for the Lab Kit.
 *
 * Type in commands through USB.
 * Commands: ls, cd, cat, edit, mkdir, draw, rm
 *
 * Copyright (c) 2017-2018 Ben Goldberg
 * MIT License (see LICENSE in this repository)
 * CHANGES:
 *   4/3/18 Updated header comment
 * TODO:
 *
 * BUGS:
 *
 */
#include <SPI.h>
#include <SD.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>

/* Below code copied from spitftbitmap.ino, an example from the Adafruit ILI9341 Library */


/***************************************************
  This is our Bitmap drawing example for the Adafruit ILI9341 Breakout and Shield
  ----> http://www.adafruit.com/products/1651
  Check out the links above for our tutorials and wiring diagrams
  These displays use SPI to communicate, 4 or 5 pins are required to
  interface (RST is optional)
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!
  Written by Limor Fried/Ladyada for Adafruit Industries.
  MIT license, all text above must be included in any redistribution
 ****************************************************/

#define TFT_DC 6
#define TFT_CS 5
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

#define SD_CS 4

/* End of copied code */

const String cmds[] = {"ls", "cd", "cat", "edit", "mkdir", "draw", "rm"};
#define NUMBER_OF_COMMANDS 7
#define SD_CS 4
String currentDir = "/";

void ls() {
    Serial.println("Name\tSize");
    File dir = SD.open(currentDir);
    dir.seek(0);
    while (1) {
        File f = dir.openNextFile();
        if (!f) {
            break;
        }
        Serial.print(f.name());
        if (f.isDirectory()) {
            Serial.println("\tDIRECTORY");
        } else {
            Serial.print("\t");
            Serial.println(f.size(), DEC);
        }
        f.close();
    }
    dir.seek(0);
    dir.close();
}

void cd(String arg) {
    char buf[32];
    currentDir.toCharArray(buf, 32);
    if (arg == "") {
        Serial.println("Correct usage: cd <folder path>");
        return;
    }
    if (SD.exists(currentDir + arg)) {
        currentDir = currentDir + arg;
    } else {
        currentDir = arg;
    }
    if (arg == "..") {
        String path;
        int numSlash = 0;
        for (int i = 0; i < 32; i++) {
            if (buf[i] == '/') {
                numSlash++;
            }
        }
        for (int i = 0; i < numSlash - 2; i++) {
            if (i == 0) {
                path += strtok(buf, "/");
            } else {
                path += "/";
                path += strtok(NULL, "/");
            }
        }
        currentDir = "/";
        currentDir += path;
    }
    if (currentDir[currentDir.length() - 1] != '/') {
        currentDir += "/";
    }
    Serial.println("Changed directories to " + currentDir);
}

void cat(String arg) {
    String fname = currentDir + arg;
    if (!(SD.exists(fname))) {
        Serial.println("Correct usage: cat <file name>");
        return;
    } else {
        File f = SD.open(fname);
        while (f.available()) {
            Serial.write(f.read());
        }
        f.close();
    }
}

void edit(String arg) { // <backslash> to stop editing
    File f = SD.open(currentDir + arg, FILE_WRITE);
    if (f.read()) {
        f.seek(0);
        while (f.available()) {
            Serial.write(f.read());
        }
    }
    f.seek(0);
    while (1) {
        while (!(Serial.available()));
        char ch = Serial.read();
        if (ch == '\\') {
            f.close();
            return;
        } else {
            Serial.write(ch);
            f.write(ch);
        }
    }
}

void mkdir(String arg) {
    String path = currentDir + arg;
    SD.mkdir(path);
}

void draw(String arg) {
    char argy[arg.length() + 1];
    arg.toCharArray(argy, arg.length() + 1);
    if (SD.exists(argy)) {
        tft.fillScreen(ILI9341_WHITE);
        tft.bmpDraw((char *)argy, 0, 0);
    } else {
        String path = currentDir + argy;
        char arg[path.length() + 1];
        path.toCharArray(arg, path.length() + 1);
        if (SD.exists(arg)) {
            tft.fillScreen(ILI9341_WHITE);
            tft.bmpDraw((char *)arg, 0, 0);
        } else {
            Serial.println("File not found.");
        }
    }
}

void rm(String arg) {
    String f = currentDir + arg;
    SD.remove(f);
}

void execFromSD(String cmd) {
    File f = SD.open(currentDir + cmd + ".HEX");
    Serial.println(String("Executing ") + f.name());
    if (SD.exists(f.name())) {
        while (f.available()) {
            Serial.write(f.read()); // replace this with writing f.read() to flash
        }
    } else {
        Serial.println("File not found");
    }
    f.close();
}

void execCMD(int cmdNum, String args) {
    switch (cmdNum) {
    case 0:
        ls();
        break;
    case 1:
        cd(args);
        break;
    case 2:
        cat(args);
        break;
    case 3:
        edit(args);
        break;
    case 4:
        mkdir(args);
        break;
    case 5:
        draw(args);
        break;
    case 6:
        rm(args);
        break;
    }
}

void printPrompt() {
    Serial.print("arduino@LabKit ");
    Serial.print(currentDir);
    Serial.print(" $");
}

void setup() {
    // put your setup code here, to run once:
    tft.begin();
    Serial.begin(921600); // blazing fast for UART
    Serial.print("Initializing SD card");
    while (!SD.begin(SD_CS)) {
        Serial.print(".");
    }
    Serial.println();
    Serial.println("Done!");
    printPrompt();
}

void loop() {
    // put your main code here, to run repeatedly:
    String cmd = "";
    while (Serial.available()) {
        char ch = Serial.read();
        cmd += ch;
    }
    if (cmd != "") {
        Serial.println(cmd);
        bool cmdFound = false;
        for (int i = 0; i < NUMBER_OF_COMMANDS; i++) {
            if (cmd.indexOf(cmds[i]) != -1) {
                cmdFound = true;
                String arg = cmd;
                arg.replace(cmds[i] + " ", "");
                execCMD(i, arg);
            }
        }
        if (!cmdFound) {
            execFromSD(cmd);
        }
        printPrompt();
    }
}
