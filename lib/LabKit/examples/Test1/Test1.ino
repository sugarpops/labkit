/*
 * Lab Kit Test Program 1
 *
 * This program lights one of the lab kit LEDs corresponding to one of the
 * kit's pushbutton switches.
 *
 * Expected behavior:
 *
 *  * After reset, no LEDs lit
 *  * If button '1" pressed, the red LED lights
 *  * If button '2" pressed, the green LED lights
 *  * If button '3" pressed, the blue LED lights
 *
 * Any combination of button presses should be supported
 *
 *
 * Copyright (c) 2017 Pete Soper
 * MIT License (See LICENSE in this repository)
 *
 * CHANGES
 *  10/4/2017 - Refactored to use HAL (Pete)
 *  10/13/2017 - Eliminated use of onboard LED
 */

// Arduino specific defines

#include <LabKit.h>

void setup() {
    // Enable hardware
    LabKit.begin();
}

// The "main program" endlessly samples the switches and lights the LED
// corresponding to any switch found to be pressed.

void loop() {
    static uint8_t index = 0;
    LabKit.setLEDByOrdinal(index, ! LabKit.getButton(index));
    index = (index + 1) % sizeof(LabKit.led_to_pin);
}

