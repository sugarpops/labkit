/*
 * Test M41T6X driver
 *
 *  Copyright (c) 2014, 2015, 2017 Peter J. Soper
 *  MIT License (see "LICENSE" in this repository)
 */

#include <Wire.h>
#include <M41T6X.h>
#include <LabKit.h>
#include <SimplePinChange.h>

M41T6X rtc;

volatile static uint32_t int0, int1, last_int0, last_int1,
         wdcount0, wdcount1, ofcount0, ofcount1, wavecount, intcount;

void cRead(const char *format, ...) {
    char line[73];
    uint16_t i = -1;
    va_list args;

    do {
        i += 1;
        if (i > (sizeof(line) - 1)) {
            rtc.cPrintP(F("\r\nLINE TOO LONG!\r\n"));
            break;
        }
        line[i] = Serial.read();
        Serial.write(line[i]);
    } while (line[i] != 0xa);
    line[i] = 0;
    va_start(args, format);
    sscanf(line, format, args);
    va_end(args);
}

void sqwCallback() {
    int0 += 1;
}

void irqCallback() {
    if (rtc.getWDF() == 1) {
        wdcount1 += 1;
        rtc.resetWDF();
    } else if (rtc.getOF() == 1) {
        ofcount1 += 1;
        rtc.restart();
    } else {
        int1 += 1;
    }
}

uint8_t keyboardChar() {
    while (Serial.available() == 0)
        ;
    return Serial.read();
}

/*
 * To do: probe the chip and detect:
 *   I2C failure
 *   broken/insane clock chip behavior
 */

void setup() {
    LabKit.begin();
    Wire.begin();
    rtc.begin();
    rtc.restart();
    rtc.setSquareWaveFrequency(8);
    rtc.resetWDF();
    pinMode(LabKit.RTC_SQW, INPUT);
    SimplePinChange.attach(LabKit.RTC_SQW, sqwCallback);
}

void printInfo() {
    rtc.printDateTime(rtc.getT());
    rtc.printAlarmDateTime();
    rtc.printRegs();
    rtc.cPrintP(F("Int0: %d Int1: %d wdcount0 %d "), int0, int1, wdcount0);
    rtc.cPrintP(F("wdcount1 %d ofcount0 %d ofcount1 %d\r\n"), wdcount1, ofcount0, ofcount1);
    rtc.cPrintP(F("wavecount: %d intcount %d\r\n"), wavecount, intcount);
}

static const char commands[] = "qmrfsdhinwzeoa";

void printPrompt() {

#define C_QUIT 0
#define C_MONITOR 1
#define C_RESET 2
#define C_CAL_FASTER 3
#define C_CAL_SLOWER 4
#define C_SET_DATE_TIME 5
#define C_HEX_DUMP 6
#define C_IA 7
#define C_ID 8
#define C_WD_ENABLE 9
#define C_WD_DISABLE 10
#define C_SQW_ENABLE 11
#define C_SQW_DISABLE 12
#define C_RESTART 13

    rtc.cPrintP(F("a - restart\r\n"));
    rtc.cPrintP(F("d - date and time set\r\n"));
    rtc.cPrintP(F("e - square wave enable\r\n"));
    rtc.cPrintP(F("f - faster\r\n"));
    rtc.cPrintP(F("h - hex register dump\r\n"));
    rtc.cPrintP(F("i - interrupt enable\r\n"));
    rtc.cPrintP(F("m - monitor\r\n"));
    rtc.cPrintP(F("n - no interrupts (disable)\r\n"));
    rtc.cPrintP(F("o - square wave disable\r\n"));
    rtc.cPrintP(F("q - quit\r\n"));
    rtc.cPrintP(F("r - reset\r\n"));
    rtc.cPrintP(F("s - slower\r\n"));
    rtc.cPrintP(F("w - watchdog enable\r\n"));
    rtc.cPrintP(F("z - zero watchdog\r\n"));
    rtc.cPrintP(F("Enter command:"));
    Serial.flush();
}

static int delayValue = 5000;

void loop() {
    char ch;
    uint8_t command_index = 0;
    printInfo();
    printPrompt();
    Serial.flush();
    do {
        ch = keyboardChar();
    } while (ch <= ' ');
    while (commands[command_index]) {
        if (commands[command_index] == ch) {
            rtc.cPrintP(F("command: %d\r\n"), command_index);
            break;
        } else {
            command_index += 1;
        }
    }
    if (command_index >= strlen(commands)) {
        rtc.cPrintP(F("Command not found\r\n"));
        return;
    }
    switch (command_index) {
    case C_QUIT:
        while (1)
            ;
        break;
    case C_MONITOR: {
        rtc.cPrintP(F("Enter x to stop monitor %d\r\n"), delayValue);
        while (1) {
            printInfo();
            Serial.flush();
            char c = 0;
            if (Serial.available() != 0) {
                c = Serial.read();
            }
            if (c == 'f') {
                delayValue = delayValue / 2;
                if (delayValue <= 0) {
                    delayValue = 1;
                }
                rtc.cPrintP(F("(%d)\r\n"), delayValue);
                Serial.flush();
            } else if (c == 's') {
                delayValue = delayValue * 2;
                rtc.cPrintP(F("(%d)\r\n"), delayValue);
                Serial.flush();
            } else if (c == 'x') {
                break;
            }
            rtc.cPrintP(F("Monitor (%d)\r\n"), delayValue);
            delay(delayValue);
        }
    }
    break;
    case C_RESET:
        rtc.reset(1, 1);
        break;
    case C_CAL_FASTER:
        rtc.bumpCalibration(1);
        break;
    case C_CAL_SLOWER:
        rtc.bumpCalibration(-1);
        break;
    case C_SET_DATE_TIME:
        /* 2017 October 10th TUESDAY(day 2) 19:29:00 */
        // NOTICE 4th parameter is DAY OF WEEK (0=Sunday)
        rtc.setDateTime(2017, 10, 10, 2, 19, 31, 0);
        break;
    case C_HEX_DUMP:
        rtc.printHexRegs();
        rtc.cPrintP(F("Enter x to resume\r\n"));
        while (Serial.available() == 0) {
            char c = Serial.read();
            if (c == 'x') {
                break;
            }
        }
        break;
    case C_IA:
        //SimplePinChange.attach(LabKit.RTC_SQW, sqwCallback);
        break;
    case C_ID:
        //SimplePinChange.detach(LabKit.RTC_SQW);
        break;
    case C_WD_ENABLE:
        rtc.setByte(M41T6X_BMB_INDEX, 6);
        break;
    case C_WD_DISABLE:
        rtc.setByte(M41T6X_BMB_INDEX, 0);
        break;
    case C_SQW_ENABLE:
        rtc.setSQWE(1);
        break;
    case C_SQW_DISABLE:
        rtc.setSQWE(0);
        break;
    case C_RESTART:
        rtc.restart();
        break;
    default:
        Serial.println(F("Unknown command %d\r\n"));
    }
    rtc.cPrintP(F("BOTTOM OF LOOP\r\n"));
    Serial.flush();
}
