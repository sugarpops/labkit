/*
 * Test3.ino - This program counts the number of interrupts per second a PWM signal is generating
 * And prints the number of interrupts per second to the serial monitor
 * You only need a wire from pin 9 to pin 4 (BUTTON_4)
 *
 * Copyright (c) 2017-2018 Ben Goldberg
 * MIT License (see LICENSE in this repository)
 * CHANGES:
 *   10/4/2017 - Shortened license verbiage (Pete)
 *   4/3/18 Updated header comment
 * TODO:
 *
 * BUGS:
 *
 */
// library includes
#include <LabKit.h>
#include <SimplePinChange.h>

unsigned long counter = 0; // I tried the int datatype here, but it overflowed at the fastest PWM rate... :-(
float interruptsPerSec = 0;
const uint8_t pwm = 9;

void isr() { // this is called when BUTTON_4's state changes
    counter += 1; // add one to the counter
}

void setup() {
    LabKit.begin(); // Set up the Lab Kit
    SimplePinChange.attach(LabKit.BUTTON_4, isr); // Set up the interrupt
    setPwmFrequency(pwm, 1); // PWM frequency: 31,250 Hz
}

void loop() {
    analogWrite(pwm, 127); // PWM frequency: 31,250 Hz
    uint8_t saved_sreg;

    // Use a critical section
    saved_sreg = SREG;
    cli(); // stop interrupts
    counter = 0; // Reset counter
    SREG = saved_sreg; // start interrupts back up

    delay(1000); // Wait a second

    // Use another critical section
    saved_sreg = SREG;
    cli(); // stop interrupts
    interruptsPerSec = counter / 2.0; // CHANGING interrupt counts RISING and FALLING, so we have to divide by two
    counter = 0; // Reset counter
    Serial.println(interruptsPerSec); // Print PWM rate
    SREG = saved_sreg; // start interrupts back up
}
