/*
 * This program demonstrates critical sections and minimal overhead interrupt
 * functions as well as using a high rate PWM signal to time serial output.
 * If button 3 is pressed critical sections are disabled allowing the
 * mainline code to have an incoherrent view of the interrupt counter. When
 * an out of range baud rate is detected the program freezes until reset.
 *
 * The code handles 62500 interrupts per second. An attempt was made to
 * trigger an interrupt on both the rising and falling edge of the PWM
 * square wave from pin 6 so the timing was done in units of 1/125000 seconds,
 * but the result was so much interrupt overhead that the mainline code
 * doesn't make any progress. Considering how short the interrupt handler
 * code is, this tells us something of the absolute power of a 16Mhz
 * Atmega 328.
 *
 * To run this code jumper pin 11 to pin 2 (BUTTON_2). The pin 11 PWM
 * output uses timer 2 in the Arduino MCU.
 *
 * Copyright (c) 2017 Pete Soper
 * MIT License (see LICENSE in this repository)
 *
 * This code borrows from Test3 written by Ben Goldberg.
 */

#include <SimplePinChange.h>
#include <LabKit.h>

// The I/O pin used for high speed PWM output

const uint8_t PWM_OUTPUT = 11;

// The number of cycles per second of the PWM port with the configuration
// below. Update this to track the pwm config call in setup().

const float PWM_CYCLES_PER_SECOND = 31250.0;

/*
 * Current count of interrupts caused by the Button_2 connection to a PWM
 * output. BE SURE what's measured does not take so much time that this
 * counter overflows. At 62500Hz this comes out to about one second.
 */

uint16_t interrupt_count = 0;

// this gets called by an interrupt vector handler when Button 2's state
// changes. This function is called every 1/(PWM_CYCLES_PER_SECOND*2) seconds.

void pwmInterrupt() {
    interrupt_count += 1;
}

// Code executed once

void setup() {

    // Set up the lab kit hardware. See LabKit.h

    LabKit.begin();

    // Set the PWM to max. For the current pin 11 this is 32500 Hz

    setPwmFrequency(PWM_OUTPUT, 1);
    analogWrite(PWM_OUTPUT, 127);

    // Interrupt on the both edges of the PWM square wave for an interrupt
    // rate of 62500 Hz

    attachInterrupt(digitalPinToInterrupt(LabKit.BUTTON_2), pwmInterrupt,
                    CHANGE);
}

/*
 * Main line code executed repeatedly:
 *  - Reset interrupt counter
 *  - Write text to the serial port: wait until it's completely "on the wire"
 *  - Fetch the interrupt counter and compute how long the serial output
 *    took and the estimated baud rate (10 bits per character).
 *
 * The output should be very close to DEFAULT_SERIAL_RATE (currently 9600).
 */

void loop() {

    /*
     * Reset the interrupt counter.
     * If button 3 held down, don't guard access. This will cause wonky
     * behavior in a matter of seconds due to the interrupt level code
     * causing this (and especially the other counter reference below) to
     * have an incoherrent view of the memory holding the variable.
     */

    uint8_t saved_sreg;

    if (LabKit.getButton3()) {
        saved_sreg = SREG;
        cli();
        interrupt_count = 0;
        SREG = saved_sreg;
    } else {
        interrupt_count = 0;
    }

    // Output 32 characters (the last two are "carriage return" and "linefeed")

    Serial.println("Testing, one, two, three, four");

    // IMPORTANT: Wait until all characters are actually sent out.

    Serial.flush();

    // Get the current interrupt count

    uint16_t interrupt_count_copy;

    if (LabKit.getButton3()) {
        saved_sreg = SREG;
        cli();
        interrupt_count_copy = interrupt_count;
        SREG = saved_sreg;
    } else {
        interrupt_count_copy = interrupt_count;
    }

    // Compute the baud rate and output it.

    float tick_time = 1.0 / (PWM_CYCLES_PER_SECOND * 2.0);

    float elapsed_time = tick_time * (float) interrupt_count_copy;

    float bits_transmitted = 32.0 * 10.0;

    float baud_rate = bits_transmitted / elapsed_time;

    if ((baud_rate >= 9500) || (baud_rate < 9600)) {
        Serial.print("Measured baud rate: ");
    }

    Serial.println(baud_rate);

    if ((baud_rate < 9500) || (baud_rate >= 9600)) {
        Serial.print("Measured baud rate: ");
        while (1)
            ;
    }

    // The pause that refreshes

    if (LabKit.getButton3()) {
        delay(5000);
    }
}
