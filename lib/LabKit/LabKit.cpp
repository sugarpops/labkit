/*
 * Lab Kit Library
 *
 * This code provides an application programming interface (API) to the
 * lab kit hardware as well as utility routines.
 *
 * Copyright (c) 2017 Pete Soper & Ben Goldberg
 * MIT License (see LICENSE in this repository)
 */

#define DEBUG 1

#include <LabKit.h>

LabKitClass LabKit;

/*
 * Map led ordinal number to its GPIO pin number
 */

const uint8_t LabKitClass::led_to_pin[] = {
    LabKitClass::RED_LED, LabKitClass::GREEN_LED,
    LabKitClass::BLUE_LED
};

/*
 * Map button ordinal number to its GPIO pin number
 */

const uint8_t LabKitClass::button_to_pin[] = {
    LabKitClass::BUTTON_1, LabKitClass::BUTTON_2,
    LabKitClass::BUTTON_3, LabKitClass::BUTTON_4
};

void LabKitClass::begin(uint32_t serial_speed) {

    // Initialize serial I/O

    Serial.begin(serial_speed);

    // Have to set the display SPI chip select high
    // to keep it from hosing up the SD card interface.

    pinMode(TFT_CS, OUTPUT);
    digitalWrite(TFT_CS, HIGH);

    // Enable LED pins

    for (uint8_t i = 0; i < sizeof(led_to_pin); i++) {
        pinMode(led_to_pin[i], OUTPUT);
    }

    // Switch pins

    for (uint8_t i = 0; i < sizeof(button_to_pin); i++) {
        pinMode(button_to_pin[i], INPUT_PULLUP);
    }

    // When piezo speaker is driven by a periodic interrupt vs tone()
    pinMode(PIEZO, OUTPUT);

    // The lab kit RTC square wave
    pinMode(RTC_SQW, INPUT);
} // begin

// Button pin accessors

/*
 * Return the value of a button's input pin given the button ordinal value.
 */

uint8_t LabKitClass::getButton(uint8_t ordinal) {
    return digitalRead(button_to_pin[ordinal]);
}

/*
 * Return the value of a button's input pin
 */

uint8_t LabKitClass::getButton1() {
    return digitalRead(BUTTON_1);
}

uint8_t LabKitClass::getButton2() {
    return digitalRead(BUTTON_2);
}

uint8_t LabKitClass::getButton3() {
    return digitalRead(BUTTON_3);
}

uint8_t LabKitClass::getButton4() {
    return digitalRead(BUTTON_4);
}

/*
 * Set an LED by it's ordinal to the indicated value.
 */

void LabKitClass::setLEDByOrdinal(uint8_t ordinal, uint8_t value) {
    digitalWrite(led_to_pin[ordinal], value);
}

/*
 * Flash all LEDs the given number of times with the given milliseconds
 * between on and off.
 */

void LabKitClass::flashLEDs(uint8_t number_of_flashes,
                            uint16_t milliseconds_between_flashes) {
    for (uint8_t i = 0; i < number_of_flashes; i++) {
        for (uint8_t o = 0; o < sizeof(led_to_pin); o++) {
            setLEDByOrdinal(o, HIGH);
        }
        delay(milliseconds_between_flashes);
        for (uint8_t o = 0; o < sizeof(led_to_pin); o++) {
            setLEDByOrdinal(o, LOW);
        }
        delay(milliseconds_between_flashes);
    }
}

/*
 * Beep the piezo speaker
 */

void LabKitClass::beep(uint16_t frequency, uint16_t duration) {
    tone(LabKit.PIEZO, frequency, duration);
}

// Helper functions

/**
 * Divides a given PWM pin frequency by a divisor.
 *
 * This code is from https://playground.arduino.cc/Code/PwmFrequency
 *
 * The resulting frequency is equal to the base frequency divided by
 * the given divisor:
 *   - Base frequencies:
 *      o The base frequency for pins 3, 9, 10, and 11 is 31250 Hz.
 *      o The base frequency for pins 5 and 6 is 62500 Hz.
 *   - Divisors:
 *      o The divisors available on pins 5, 6, 9 and 10 are: 1, 8, 64,
 *        256, and 1024.
 *      o The divisors available on pins 3 and 11 are: 1, 8, 32, 64,
 *        128, 256, and 1024.
 *
 * PWM frequencies are tied together in pairs of pins. If one in a
 * pair is changed, the other is also changed to match:
 *   - Pins 5 and 6 are paired on timer0
 *   - Pins 9 and 10 are paired on timer1
 *   - Pins 3 and 11 are paired on timer2
 *
 * Note that this function will have side effects on anything else
 * that uses timers:
 *   - Changes on pins 3, 5, 6, or 11 may cause the delay() and
 *     millis() functions to stop working. Other timing-related
 *     functions may also be affected.
 *   - Changes on pins 9 or 10 will cause the Servo library to function
 *     incorrectly.
 *
 * Thanks to macegr of the Arduino forums for his documentation of the
 * PWM frequency divisors. His post can be viewed at:
 *   http://forum.arduino.cc/index.php?topic=16612#msg121031
 */

void setPwmFrequency(int pin, int divisor) {
    byte mode;
    if (pin == 5 || pin == 6 || pin == 9 || pin == 10) {
        switch (divisor) {
        case 1: mode = 0x01; break;
        case 8: mode = 0x02; break;
        case 64: mode = 0x03; break;
        case 256: mode = 0x04; break;
        case 1024: mode = 0x05; break;
        default: return;
        }
        if (pin == 5 || pin == 6) {
            TCCR0B = (TCCR0B & 0b11111000) | mode;
        } else {
            TCCR1B = (TCCR1B & 0b11111000) | mode;
        }
    } else if (pin == 3 || pin == 11) {
        switch (divisor) {
        case 1: mode = 0x01; break;
        case 8: mode = 0x02; break;
        case 32: mode = 0x03; break;
        case 64: mode = 0x04; break;
        case 128: mode = 0x05; break;
        case 256: mode = 0x06; break;
        case 1024: mode = 0x07; break;
        default: return;
        }
        TCCR2B = (TCCR2B & 0b11111000) | mode;
    }
}

/*
 * Print up to 2^16 bytes as hex and ascii. The address parameter is prefixed
 * as a four digit hex value. Nonprintable characters are shown as periods. A
 * newline is printed by default or always after each 16 bytes if there ar
 * more than 16 bytes.
 */

void printBytes(uint16_t address, uint8_t array[], uint16_t start,
                uint16_t end, bool print_newline) {
    uint16_t count = end - start + 1;
    if (count > 16) {
        print_newline = true;
    }
    for (uint16_t offset = 0; offset < count; offset = offset + 16) {
        char temp_string[6];
        sprintf(temp_string, "%04x ", address + offset);
        Serial.print(temp_string);
        for (uint16_t i = 0; i < 16; i++) {
            if ((offset + i) >= count) {
                break;
            }
            sprintf(temp_string, "%02x", array[offset + i]);
            Serial.print(temp_string);
        }
        Serial.print(" ");
        for (uint16_t i = 0; i < 16; i++) {
            if ((offset + i) >= count) {
                break;
            }
            if (isprint(array[offset + i])) {
                Serial.write(array[offset + i]);
            } else {
                Serial.write('.');
            }
        }
        if (print_newline) {
            Serial.println();
            count += 1;
        }
    }
}

/*
 * As above, but an array of given length (starting with first element).
 */

void printBytes(uint16_t address, uint8_t array[], uint16_t length,
                bool print_newline) {

    printBytes(address, array, 0, length - 1, print_newline);

}
