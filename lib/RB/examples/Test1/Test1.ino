/*
 * Test the ring buffer class.
 *
 * Jumper Arduino pin 10 to pin 2 and pin 6 to pin 3 for this test.
 *
 * The PWM rate for pin 6 is left alone, while for 10 it's set to 32KHz.
 * Incremental values are put into the first ring buffer by an interrupt
 * handler. The non-interrupt code empties that buffer and keeps
 * track of the highest latency of storing items. The opposite is done with a
 * second ring buffer: the non-interrupt code inserts into the second ring
 * buffer and the interrupt handler empties it.
 *
 * When button three is pressed results as well as the ring buffer contents
 * are sent to Serial output and the program hangs until reset.
 *
 *  Copyright (c) 2017 Peter J. Soper
 *  MIT License (see "LICENSE" in this repository)
 */

#include <LabKit.h>
#include <MsTimer2.h>
#include <SimplePinChange.h>

#include <RB.h>

// The ring data

#define RB_CAPACITY 8

struct ring_data {
    uint8_t data;
    uint32_t timestamp;
};

typedef struct ring_data RING_DATA;

#include <RB.tpp>

RB<RING_DATA, RB_CAPACITY> ring_buffer1, ring_buffer2;

static uint32_t greatest_microseconds1, greatest_microseconds2;
static uint32_t count1, count2;
static uint32_t last_microseconds1, last_microseconds2;
static uint32_t int2, int3;

/*
 * Add data to ring buffer one.  Called by pin 2 interrupts
 */

void putData1() {
    static uint8_t counter1;
    RING_DATA buf;
    int2 += 1;
    buf.data = counter1++;
    buf.timestamp = micros();
    if (ring_buffer1.putData(buf, false)) {
        if ((buf.timestamp - last_microseconds1) > greatest_microseconds1) {
            greatest_microseconds1 = buf.timestamp - last_microseconds1;
        }
        last_microseconds1 = buf.timestamp;
    }
}

/*
 * Remove data from ring buffer one.  Called by loop()
 */

void getData1() {
    RING_DATA buf;
    if (ring_buffer1.getData(buf, false)) {
        count1 += 1;
    }
}

/*
 * Put data into ring buffer two. Called by loop()
 */

void putData2() {
    static uint8_t counter2;
    RING_DATA buf;
    if (ring_buffer2.available() < RB_CAPACITY) {
        buf.data = counter2++;
        buf.timestamp = micros();
        if (ring_buffer2.putData(buf, false)) {
            count2 += 1;
            if ((buf.timestamp - last_microseconds2) > greatest_microseconds2) {
                greatest_microseconds2 = buf.timestamp - last_microseconds2;
            }
            last_microseconds2 = buf.timestamp;
        }
    }
}

/*
 * Get data from ring buffer two.  Invoked with pin 3 interrupts
 */

void getData2() {
    RING_DATA buf;
    int3 += 1;
    if (ring_buffer2.getData(buf, false)) {
    }
}

void setup() {
    // Set up kit hardware
    LabKit.begin();

    // Jack up the PWM speed for faster pin 2 interrupts
    setPwmFrequency(10, 1);

    pinMode(6, OUTPUT);
    analogWrite(6, 127);

    pinMode(10, OUTPUT);
    analogWrite(10, 127);

    // Define the pin 2 and 3 interrupt handlers
    // Atmega 328 does not have enough juice to support interrupts on both
    // edges.

    last_microseconds1 = last_microseconds2 = micros();
    attachInterrupt(digitalPinToInterrupt(2), putData1, RISING);
    attachInterrupt(digitalPinToInterrupt(3), getData2, CHANGE);
}

/*
 * Output the fields of one ring buffer element.
 */

void showRingData(RING_DATA &d) {

    Serial.print(" d: ");
    Serial.print(d.data);
    Serial.print(" ");
    Serial.print(d.timestamp);
}

/*
 * Output state variables and ring buffer contents for each.
 */

void show() {
    Serial.print("One ints: ");
    Serial.print(int2);
    Serial.print(" > micros: ");
    Serial.print(greatest_microseconds1);
    Serial.print(" count: ");
    Serial.print(count1);
    Serial.print(" contents: ");
    Serial.println(ring_buffer1.available());
    ring_buffer1.showData(showRingData);
    Serial.print("Two ints: ");
    Serial.print(int3);
    Serial.print(" > micros: ");
    Serial.print(greatest_microseconds2);
    Serial.print(" count: ");
    Serial.print(count2);
    Serial.print(" contents: ");
    Serial.println(ring_buffer2.available());
    ring_buffer2.showData(showRingData);
}

// Exercise the buffers until button three pressed.

void loop() {
    if (! LabKit.getButton3()) {
        detachInterrupt(0);
        detachInterrupt(1);
        show();
        while (1);
    } else {
        getData1();
        putData2();
    }
}
