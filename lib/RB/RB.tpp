/*
 * RB.tpp - Implementation of RB.h API

 * The Ring Buffer implementation. 
 * See RB.h for API details.
 * 
 * Copyright (c) 2017 Pete Soper
 * MIT License (see LICENSE in this repository)
 *
 * IMPLEMENTATION CHANGES:
 *   <change date> <change commit message>
 * IMPLEMENTATON TODO:
 *   5/2/2018 - Test the speed/size of this implementation and compare with
 *              the void* implementation.
 * IMPLEMENTATON BUGS AND FEATURE REQUESTS:
 *   <report date> <bug/feature description or issue reference>
 */

#include <RB.h>

/*
 * RB object constructor.
 */

template <class DATA_TYPE, uint8_t RB_SIZE> RB<DATA_TYPE, RB_SIZE>::RB()
{
    entries = next_read = next_write = 0;
}

/*
 * Return number of active elements
 */

template <class DATA_TYPE, uint8_t RB_SIZE> uint8_t RB<DATA_TYPE, RB_SIZE>::available()
{
    return entries;
}

/*
 * Empty a buffer.
 */

template <class DATA_TYPE, uint8_t RB_SIZE> void RB<DATA_TYPE, RB_SIZE>::init()
{
    // Don't allow interruption, but save current state in case we're in an
    // interrupt handler (i.e. cli/sei would be BIG MISTAKE).

    uint8_t saved_sreg = SREG;
    cli();
    entries = next_write = next_read = 0;
    SREG = saved_sreg;
}

/*
 * Return the least recently inserted item
 */

template <class DATA_TYPE, uint8_t RB_SIZE> bool RB<DATA_TYPE, RB_SIZE>::peekOldest(DATA_TYPE &copy)
{
    uint8_t saved_sreg = SREG;
    cli();
    if (entries) {
        uint8_t index = (next_read - 1) % RB_SIZE;
        copy = buffer[index];
        SREG = saved_sreg;
        return true;
    } else {
        SREG = saved_sreg;
        return false;
    }
}

/*
 * Return the most recently inserted item
 */

template <class DATA_TYPE, uint8_t RB_SIZE> bool RB<DATA_TYPE, RB_SIZE>::peekNewest(DATA_TYPE &copy)
{
    uint8_t saved_sreg = SREG;
    cli();
    if (entries) {
        uint8_t index = (next_write - 1) % RB_SIZE;
        // NOTE this is copying all of the arbitrary fields of a buffer entry
        copy = buffer[index];
        SREG = saved_sreg;
        return true;
    } else {
        SREG = saved_sreg;
        return false;
    }
}

/*
 * Get the next available ring buffer entry
 */

template <class DATA_TYPE, uint8_t RB_SIZE> bool RB<DATA_TYPE, RB_SIZE>::getData(DATA_TYPE &copy, bool wait_if_empty)
{
    /*
     * If variable "entries" is not declared "volatile" the compiler is allowed
     * to make code that only loads entries into a register once, making the code
     * not see changes an interrupt handler makes to the the value of the
     * variable (i.e. it's memory contents). This will cause very confusing
     * program failures as this code will loop forever, even though some other
     * test of the variable clearly shows it has a different value than it seems
     * to in this loop. The "volatile" keyword tells the compiler to make
     * code to load entries from memory *every* time there is a reference, so
     * a change that an interrupt handler made will be seen.
     */

    // Important that member variable 'entries' is no bigger than the machine
    // word size and volatile. If it has to be larger than the word size its
    // accesses must be guarded with a critical section

    while (!entries) {
        if (wait_if_empty) {
            continue;
        } else {
            return false;
        }
    }

    uint8_t saved_sreg = SREG;
    cli();

    copy = buffer[next_read++];
    next_read %= RB_SIZE;
    entries -= 1;

    // end of critical section
    SREG = saved_sreg;

    return true;
}

/*
 * Add a new item
 */

template <class DATA_TYPE, uint8_t RB_SIZE> bool RB<DATA_TYPE, RB_SIZE>::putData(DATA_TYPE &copy, bool wait_if_full)
{
    while (entries == RB_SIZE) {
        if (wait_if_full) {
            continue;
        } else {
            return false;
        }
    }

    uint8_t saved_sreg = SREG;
    cli();

    buffer[next_write++] = copy;
    next_write %= RB_SIZE;
    entries += 1;

    SREG = saved_sreg;

    return true;
}

/*
 * Print the ring buffer contents.
 */

template <class DATA_TYPE, uint8_t RB_SIZE> void RB<DATA_TYPE, RB_SIZE>::showData(void (*func)(DATA_TYPE &))
{
#ifdef DEBUG
    // Stop the world and take a snapshot of the ring buffer

    uint8_t saved_sreg = SREG;
    cli();

    uint8_t copied_entries = entries;
    uint8_t copied_next_write = next_write;
    uint8_t copied_next_read = next_read;
    DATA_TYPE copied_buffer[RB_SIZE];
    for (uint8_t i = 0; i < RB_SIZE; i++) {
        copied_buffer[i] = buffer[i];
    }

    // End of critical section
    SREG = saved_sreg;

    // Now print the details of the snapshot

    for (uint8_t i = 0; i < RB_SIZE; i++) {
        // subscript
        Serial.print(i);
        Serial.print(" ");

        // call out to the user function to show values of the buffer entry

        func(copied_buffer[i]);

        // Append characters to show next insertion or read position

        if ((copied_entries < RB_SIZE) && (i == copied_next_write)) {
            Serial.print(" <=");
        }

        if ((copied_entries > 0) && (i == copied_next_read)) {
            Serial.print(" =>");
        }

        Serial.println();
    }
#endif
}
