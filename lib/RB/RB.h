/*
 * RB.h - Ring Buffer templates
 *
 * This code provides an application programming interface (API) to a
 * simple ring buffer via templates. It costs about 500 bytes of text per
 * instance.
 *
 * See public interfaces below for details.
 *
 * Copyright (c) 2017 Pete Soper
 * MIT License (see LICENSE in this repository)
 *
 * API CHANGES:
 *   <change date> <API interface syntax/semantic change commit message>
 * API TODO:
 *   <todo entry date> <planned interface change description and/or issue ref>
 * API BUGS AND FEATURE REQUESTS:
 *   <report date> <API bug description and/or issue reference>
 */

// Arduino specific defines

#ifndef RB_H

#define RB_H

#include <Arduino.h>

template <class DATA_TYPE, uint8_t RB_SIZE> class RB {
    private:
        // The ring buffer data: type is defined by the instantiation
        DATA_TYPE buffer[RB_SIZE];

        // Current number of buffer elements that contain live data
        volatile uint8_t entries;

        // Buffer array index where next item will be added if not full
        volatile uint8_t next_write;

        // Buffer array index where next item will be removed if not empty
        volatile uint8_t next_read;

    public:
        // The class constructor. Requires template arguments specifying the
        // buffer element type and buffer capacity (number of elements).
        // example:
        // RB<RING_DATA, RB_CAPACITY> ring_buffer1, ring_buffer2;

        RB();

        /*
         * Return the number of active elements currently in the ring buffer
         */
        uint8_t available();

        /*
         * Copy into the next available ring buffer entry and return true for
         * success. If the buffer is empty then if wait_if_empty is true the
         * function will wait in a loop for an item to be inserted by an
         * interrupt service routine. If no data is available and wait is
         * false then return false. The "available" function can be used
         * to see if this function might wait to avoid having an application
         * getting "stuck" waiting for something that may never happen.
         */
        bool getData(DATA_TYPE &copy, bool wait = false);

        /*
         * Copy out the most recently inserted item in the buffer passed by
         * reference and return true or else false to indicate the ring buffer
         * is empty.
         */
        bool peekNewest(DATA_TYPE &copy);

        /*
         * Copy out the least recently inserted item in the buffer and return
         * true or else false to indicate the ring buffer is empty.
         */
        bool peekOldest(DATA_TYPE &copy);

        /*
         * Copy the given data into the next element of the ring buffer
         * and return true. If there is no room for another element wait
         * until there is room or return false if wait is also false.
         */
        bool putData(DATA_TYPE &data, bool wait = false);

        /*
         * Empty a buffer. This throws away/"flushes" any contents and
         * prepares the buffer for new data. This function is not needed in
         * addition to begin.
         */
        void init();

        /*
         * DEBUG: Print the ring buffer contents. Helper function func must be
         * defined by the user and is expected to use Serial.print to output
         * the value(s) of the data in an instance of DATA_TYPE.
         */
        void showData(void (*func)(DATA_TYPE &));

        // The API version: bumped with each backward-incompatible change
        static const uint8_t RB_VERSION = 1;
};

#endif // RB_H
