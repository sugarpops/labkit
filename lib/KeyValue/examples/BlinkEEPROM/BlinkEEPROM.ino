/*
 * BlinkEEPROM.ino -- The classic Blink example, except the blink delay is the value of ?0 in EEPROM.
 *
 * This program just blinks the pin 13 LED.
 *
 * Copyright (c) 2017-2018 Ben Goldberg
 * MIT License (see LICENSE in this repository)
 * CHANGES:
 *   4/3/18 Updating to use style conventions
 * TODO:
 *
 * BUGS:
 *
 */
// Library includes
#include <EEPROM.h>
#include <KeyValue.h>
#include <LabKit.h>

// Setting up variables
unsigned long blink_delay = 1000; // default
uint16_t addr;

void setup() {
    // Initialization
    LabKit.begin();
    KeyValue.create(5, 25);
    KeyValue.begin(5, 25);
    pinMode(13, OUTPUT);
    // Get the value from EEPROM
    // you can put a KeyValue.putValue("?0", "milliseconds for blinking"); here
    String val;
    KeyValueClass::kv_return status = KeyValue.getValue("?0", val);
    if (status == KeyValueClass::__LK_KV_OK) { // Does the value exist and is a valid integer?
        blink_delay = val.toInt(); // If so, set the blink delay time to the value.
        Serial.println("Argument found, going with " + String(blink_delay) + " milliseconds between blinks.");
    } else { // Otherwise, set the blink delay to the default of 1000ms.
        Serial.println("No arguments found (or there is an error), going with " + String(blink_delay) + " milliseconds between blinks.");
    }
}

void loop() {
    // Blink forever
    digitalWrite(13, HIGH);
    delay(blink_delay);
    digitalWrite(13, LOW);
    delay(blink_delay);
}
