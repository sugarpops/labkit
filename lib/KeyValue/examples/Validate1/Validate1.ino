/*
   Validate the key/value store. These tests OVERWRITE EEPROM CONTENTS EXCEPT
   FOR THE range of addresses from 0 to __KEY_VALUE_DEFAULT_START_ADDRESS and
   __KEY_VALUE_DEFAULT_END_ADDRESS+1 to 1023. If assumptions about these two
   address limits are found to be invalid (i.e. because of a change to
   KeyValue.h not reflected in this test) the program is aborted.

   This validation suite tests the following API functions:
    create, begin, destroyDatabase, getValue, putValue, deleteKey,
    getFirstKey, getNextKey, dump

   Tests do not ordinarily produce any output unless there is a failure.
   If there is no abort of the test process then after the last test the total
   tests passed and failed is output. If all tests have passed one additional
   line of "PASSED" will be output.

   Copyright 2017, 2018 Pete Soper
   MIT license (see LICENSE in this repo)
 */


#include "LabKit.h"
//#ifdef DEBUG2
#include <EEPROM.h>
//#endif
#include "KeyValue.h"

// See get/put tests below for explanation of this number

#define SCALE 500

// Count of tests and failures

uint16_t pass_count;
uint16_t fail_count;

// Holds a key value pair

struct keyvalue {
    String key;
    String value;
};

// key/value pairs to put into the database during testing

struct keyvalue installed_values[] = {
    { String("LABKIT_FIRMWARE_REV"), String("1.21") },
    { String("?0"), String("OD") },
    { String("?1"), String("FOO.TXT") },
    { String("joeblow"), String("XYZ.TXT") },
    { String("Jim Bob"), String("works two jobs") },
    { String("LABKIT_FIRMWARE_REV"), String("1.22") }, // replaces previous value
    { String("Jim Bob"), String("") },     // gives key an empty string value
    { String("mooboo"), String("ZYX.TXT") }
};

// key/value pairs expected after values above installed

// There is no easy way to figure this out except to carefully study the
// installed_values struct array

struct keyvalue expected_values[] = {
    { String("LABKIT_FIRMWARE_REV"), String("1.22") },
    { String("?0"), String("OD") },
    { String("?1"), String("FOO.TXT") },
    { String("Jim Bob"), String("") }
};

//#if DEBUG2
/*
   Dump EEPROM as hex and ascii until two null bytes seen.
 */

void dumpEEPROM(uint16_t address, const char *msg) {
    char temp_string[10];
    bool eof_seen = false;
    uint8_t null_count = 0;
    uint16_t start_address = 0;
    Serial.println(msg);
    Serial.flush();
    sprintf(temp_string, "%04x", address);
    Serial.print(F("0x"));
    Serial.print(temp_string);
    Serial.print(F(" "));
    Serial.println(msg);
    for (uint8_t row = 0; row < (1024 / 16); row++ ) {
        sprintf(temp_string, "0x%03x", start_address);
        Serial.print(temp_string);
        Serial.print(F(" "));
        for (uint16_t column = 0; column < 16; column++) {
            uint8_t temp = EEPROM.read(start_address + column);
            if (temp == 0) {
                null_count += 1;
                if (null_count >= 2) {
                    eof_seen = true;
                }
            } else {
                null_count = 0;
            }
            sprintf(temp_string, "%02x", temp);
            Serial.print(temp_string);
        }
        Serial.print(F(" "));
        for (uint16_t column = 0; column < 16; column++) {
            uint8_t temp = EEPROM.read(start_address + column);
            if ((temp < ' ') || (temp > 0x7e)) {
                Serial.write('.');
            } else {
                Serial.write(temp);
            }
        }
        Serial.println();
        if (eof_seen) {
            return;
        } // for column
        start_address += 16;
    } // for row
}
//#endif

/*
   Print an API status value as number and descriptive label
 */

void printStatus(KeyValueClass::kv_return status) {
    switch (status) {
    case KeyValueClass::__LK_KV_OK:
        Serial.print(F("normal completion"));
        break;
    case KeyValueClass::__LK_KV_NOT_FOUND:
        Serial.print(F("key/database not found"));
        break;
    case KeyValueClass::__LK_KV_BAD_ADDRESS:
        Serial.print(F("cannot create db here"));
        break;
    case KeyValueClass::__LK_KV_NO_ROOM:
        Serial.print(F("db space exhausted"));
        break;
    case KeyValueClass::__LK_KV_CORRUPT:
        Serial.print(F("db is unusuable"));
        break;
    default:
        Serial.print(F("TEST SYSTEM FAILURE! Unknown status. Update switch!"));
    }
}

/*
   Evaluate an API function result and if it isn't an expected one report
   the test source code line, the status, and a descriptive label.
 */

void testResult(uint16_t line, const char *label,
                KeyValueClass::kv_return status,
                KeyValueClass::kv_return expected = KeyValueClass::__LK_KV_OK) {

    if (status == expected) {
        pass_count += 1;
        return;
    }

    fail_count += 1;
    Serial.print(label);
    Serial.print(F(" at line: "));
    Serial.print(line);
    Serial.print(F(" expected: "));
    printStatus(expected);
    Serial.print(F(" but got: "));
    printStatus(status);
    Serial.println();
}

/*
   Invoke a library function and check for the expected return value, reporting
   the caller's source code line number and the unexpected status if there is
   no match.
 */

void tryPut(uint16_t line, String key, String value,
            KeyValueClass::kv_return expected = KeyValueClass::__LK_KV_OK) {
    testResult(line, "put", KeyValue.putValue(key, value), expected);
}

void tryGet(uint16_t line, String key, String &value,
            KeyValueClass::kv_return expected = KeyValueClass::__LK_KV_OK) {
    testResult(line, "get", KeyValue.getValue(key, value), expected);
}

void tryGetFirst(uint16_t line, String &key, String &value,
                 KeyValueClass::kv_return expected = KeyValueClass::__LK_KV_OK) {
    testResult(line, "getFirst", KeyValue.putValue(key, value), expected);
}

void tryGetNext(uint16_t line, String &key, String &value,
                KeyValueClass::kv_return expected = KeyValueClass::__LK_KV_OK) {
    testResult(line, "getNext", KeyValue.putValue(key, value), expected);
}

void tryCreate(uint16_t line, KeyValueClass::kv_address start,
               KeyValueClass::kv_address end,
               KeyValueClass::kv_return expected = KeyValueClass::__LK_KV_OK) {
    testResult(line, "create", KeyValue.create(start, end), expected);
}

void tryDestroyDatabase(uint16_t line, KeyValueClass::kv_address start,
                        KeyValueClass::kv_address end,
                        KeyValueClass::kv_return expected = KeyValueClass::__LK_KV_OK) {
    testResult(line, "destroyDatabase", KeyValue.destroyDatabase(start, end), expected);
}

void tryBegin(uint16_t line, KeyValueClass::kv_address start,
              KeyValueClass::kv_address end,
              KeyValueClass::kv_return expected = KeyValueClass::__LK_KV_OK) {
    testResult(line, "begin", KeyValue.begin(start, end), expected);
}

/*
 This is a convenience function for showing expected vs actual strings.
 */

void thisAndThat(String key1, String value1, String key2, String value2) {
    Serial.print(F("key1 "));
    Serial.print(key1);
    Serial.print(F(" value1 "));
    Serial.print(value1);
    Serial.print(F(" key2 "));
    Serial.print(key2);
    Serial.print(F(" value2 "));
    Serial.println(value2);
}

/*
   The effective "main program". Sets up, makes one pass through the tests,
   then returns.
 */

void setup() {
    String key, value;            // values set by functions
    KeyValueClass::kv_return status;  // temp function result value

    LabKit.begin(9600);           // get labkit system initialized

    // Check memory addressing assumptions

    if (KeyValueClass::__KEY_VALUE_DEFAULT_START_ADDRESS >= 100 ||
            KeyValueClass::__KEY_VALUE_DEFAULT_END_ADDRESS <= 900) {
        Serial.println(F("TEST FAILURE: Assumed lower/upper memory limits invalid"));
        Serial.println(F("ABORTING TEST"));
        fail_count += 1;
        return;
    }

    // Check line/index scale factor assumption
    if ((UINT16_MAX / SCALE ) <
            (sizeof(installed_values) / sizeof(struct keyvalue))) {
        Serial.println(F("TEST FAILURE: SCALE not large enough"));
        Serial.println(F("ABORTING TEST"));
        fail_count += 1;
        return;
    }

    // This should succeed

    KeyValue.eraseEEPROM(0, 200);
    tryCreate(__LINE__, 0, 200);
#ifdef DEBUG
    //KeyValue.dump(Serial, 0, 200);
#endif
    tryBegin(__LINE__, 0, 200);
    // Figure out bounds to make the folllowing code more readable

    uint8_t installed_bound = sizeof(installed_values) / sizeof(struct keyvalue);
    uint8_t expected_bound = sizeof(expected_values) / sizeof(struct keyvalue);


    // These should all pass

    for (uint8_t index = 0; index < installed_bound; index++) {

        // Tricky. In order to communicate both the source line number and the
        // all important array index we scale the latter up by something much
        // greater than the max likely number of lines of code in this test
        // and add the line number. So, when a "put" (or "get" below) fails, the
        // source line number is the reported number modulo the scale value and the
        // number minus that source line number divided by the scale value is the
        // array element that was involved with the failure.

        tryPut(index * SCALE + __LINE__, installed_values[index].key,
               installed_values[index].value);

    }

    //KeyValue.analyze(Serial);
    KeyValue.deleteKey("joeblow");
    KeyValue.deleteKey("mooboo");

    // Should get all expected values eventually

    status = KeyValue.getFirstValue(key, value);

    uint8_t count = 0;

    while (status != KeyValueClass::__LK_KV_EOF) {
        uint8_t previous_count = count;
        for (uint8_t i = 0; i < expected_bound; i++) {
            if ((expected_values[i].key == key) &&
                    (expected_values[i].value == value)) {
                // key/value is in the database
                count += 1;
            }
        }
        if (count == previous_count) {
            Serial.print(F("key "));
            Serial.print(key);
            Serial.print(F(" value "));
            Serial.print(value);
            Serial.println(F(" not found by getFirstValue/getNextValue"));
            fail_count += 1;
        } else {
            pass_count += 1;
        }
        status = KeyValue.getNextValue(key, value);
    }

    if (count != expected_bound) {
        Serial.println(F("wrong number of key/value pairs found by getFirst/getNext"));
        fail_count += 1;
    }

    // These should all pass

    for (uint8_t index = 0; index < expected_bound; index++) {
        tryGet(index * SCALE + __LINE__, expected_values[index].key, value);
        if (value != expected_values[index].value) {
            Serial.print(F(" failed"));
        }
    }

    Serial.println(F("End of Tests"));
    Serial.print(F("Passed: "));
    Serial.print(pass_count);
    Serial.print(F(" Failed: "));
    Serial.println(fail_count);
    if (fail_count == 0) {
        Serial.println(F("TEST PASSED"));
    } else {
#ifdef DEBUG
        KeyValue.analyze(Serial);
#endif
    }
}

void loop() {
    // After tests have run this function is called endlessly until the next
    // chip reset.
}
