/*
 * Name/Value Store Library
 *
 * This code provides an application programming interface (API) to the
 * name/value database in EEPROM.
 *
 * Copyright (c) 2017 Ben Goldberg
 * MIT License (see LICENSE in this repository)
 */

#include <KeyValue.h>
#include <stdio.h>

/*
 * Global instance
 */

KeyValueClass KeyValue;

bool checkTheSum(KeyValueClass::kv_address start_address, 
    KeyValueClass::kv_address end_address, uint8_t &checksum) {
  checksum = 0;
  if (end_address >= 1024) {
    return false;
  }
  for (KeyValueClass::kv_address i = start_address; i < end_address; i++) {
    checksum += EEPROM.read(i);
  }
  checksum = ~(checksum) + 1;
  return (EEPROM.read(end_address) == checksum);
}

/*
  Dump EEPROM contents in hex and printalble ASCII characters.
 */
void KeyValueClass::dump(HardwareSerial s, KeyValueClass::kv_address addr,
    KeyValueClass::kv_address end) {
  if (addr == 0xffff) {
     addr = h.start;
     end = h.end;
  }
  char line[20];
  do {
    sprintf(line,"0x%04x ",addr);
    Serial.print(line);
    for (uint8_t i=0;i<16;i++) {
      sprintf(line,"%02x ",EEPROM.read(addr+i));
      Serial.print(line);
    }
    for (uint8_t i=0;i<16;i++) {
      uint8_t v = EEPROM.read(addr + i);
      if ((v < ' ') || (v > 127)) {
        line[i] = '.';
      } else {
        line[i] = v;
      }
    }
    line[16] = '\0';
    Serial.println(line);
    addr += 16;
  } while(addr <= end);
}

/*
  Show the header and key/value pairs. Assumes a valid store.
 */
void KeyValueClass::showInfo(HardwareSerial s, 
    KeyValueClass::kv_address start) {
  if (start == 0xffff) {
    start = h.start;
  }
  Serial.print("start: ");
  Serial.println(h.start, HEX);
  Serial.print("end: ");
  Serial.println(h.end, HEX);
  Serial.print("next write: ");
  Serial.println(h.next_write_address, HEX);
  Serial.print("version: ");
  Serial.println(h.version);
  kv_address addr = h.start + sizeof(Header);
  while (addr < h.next_write_address) {
    Serial.print(addr, HEX);
    Serial.print(" ");
    do {
      Serial.write(EEPROM.read(addr++));
    } while (EEPROM.read(addr) != 0);
    Serial.println();
    addr += 1;
  }
}

/*
  Analyzes all EEPROM, finding and showing info for all key/value stores
  found.
 */
void KeyValueClass::analyze(HardwareSerial s) {
  Header lh;
  kv_address addr = 0;
  while (addr < 1024) {
    EEPROM.get(addr,lh);
    if (lh.magic == __MAGIC_NUMBER) {
      uint8_t checksum;
      if (checkTheSum(lh.start, lh.end, checksum)) {
        showInfo(s, lh.start);
        addr = lh.end + 1;
      } else {
        addr += 1;
      }
    } else {
      addr += 1;
    }
  }
}

/*
This private function checks to see if the store is empty. If it is. return true.
If the store isn't empty, return false.
*/

bool KeyValueClass::isEmpty() {
  return h.next_write_address == h.start + sizeof(Header);
}

/* Local Functions */

/* This local function moves EEPROM memory from a higher address to a lower address. */
void moveMemory(KeyValueClass::kv_address addrStartFrom, KeyValueClass::kv_address addrStartTo) {
  uint16_t placesToMove = addrStartFrom - addrStartTo; // Compute the distance between the two addresses

  for (KeyValueClass::kv_address i = addrStartFrom; i < 1024; i++) { // For all the characters in EEPROM after and at addrStartFrom,
    EEPROM.update(i - placesToMove, EEPROM.read(i)); // write to the current address - placesToMove the value at the current address.
  }

}

/* This local function recomputes the checksum. */
void KeyValueClass::recomputeChecksum() {
  // make uint8_t
  uint8_t checksum;
  checkTheSum(h.start, h.end, checksum);
  EEPROM.write(h.end, checksum);
}

/* 
   This local function writes a name and value to memory at start and returns
   false if enough space, else true.
*/
bool KeyValueClass::writeVal(String key, String value, kv_address start) {
  kv_address original_start = start;
  if ((h.end - h.next_write_address + 1) < 
      (key.length() + value.length() + 2)) {
    return true;
  }
  for (KeyValueClass::kv_address i = 0; i < key.length(); i++) { // Write the name
    EEPROM.write(start + i, key[i]);
  }
  start += key.length();
  EEPROM.write(start, '='); // Write the equals sign
  start += 1;
  for (KeyValueClass::kv_address i = 0; i < value.length() + 1; i++) { // Write the value
    EEPROM.write(start + i, value[i]);
  }
  h.next_write_address = original_start + key.length() + value.length() + 2;
  EEPROM.write(h.next_write_address,0);
  return false;
}

/* This local function reads a value starting at the variable start. */
KeyValueClass::kv_address readVal(String &key, String &value, KeyValueClass::kv_address start) {
  KeyValueClass::kv_address i = start;
  key = "";
  value = "";
  for (; EEPROM.read(i) != '='; i++) {
    // Get the key
    key += (char) EEPROM.read(i);
  }
  i += 1;
  for (; EEPROM.read(i) != '\0'; i++) {
    // Get the value
    value += (char) EEPROM.read(i);
  }
  return i + 1;
}

/* Public Functions */

/*
Initializes the key/value store using an existing store.
It checks for a valid header and checksum.
*/
KeyValueClass::kv_return KeyValueClass::begin(kv_address start_address,
              kv_address end_address) {
  // check for header and make sure it contains correct values
  EEPROM.get(start_address, h);
  if (h.magic == __MAGIC_NUMBER && h.start == start_address && h.end == end_address && h.version == __KEYVALUE_VERSION) {
    next_key_address = storer_data_start = start_address + sizeof(__MAGIC_NUMBER) + sizeof(Header);
    // verify checksum:
    uint8_t checksum;
    if (checkTheSum(start_address, end_address, checksum)) {
      return KeyValueClass::__LK_KV_OK;
    }
    else {
      return KeyValueClass::__LK_KV_CORRUPT;
    }
  }
  else {
    return KeyValueClass::__LK_KV_NOT_FOUND;
  }
}
/*
This function creates a new key/value store at the specified addresses.
*/
KeyValueClass::kv_return KeyValueClass::create(
    KeyValueClass::kv_address start_address,
    KeyValueClass::kv_address end_address) {

  for (uint16_t i = start_address; i < end_address; i++) {
    EEPROM.write(i,0);
  }
  if ((end_address - start_address + 1) < MINIMUM_SIZE) {
    // Return Error: Bad Address
    return KeyValueClass::__LK_KV_BAD_ADDRESS;
  } else {
    // Make variable to hold header
    // Set header values
    h.magic = __MAGIC_NUMBER;
    h.start = start_address;
    h.end = end_address;
    h.version = __KEYVALUE_VERSION;
    h.next_write_address = next_key_address = storer_data_start = start_address + sizeof(Header);
#if 0
    EEPROM.put(h.next_write_address, "name=Kanto");
    h.next_write_address += strlen("name=Kanto");
    EEPROM.write(h.next_write_address,0);
    h.next_write_address += 1;
    EEPROM.put(h.next_write_address, "inhabitants=Pikachu");
    h.next_write_address += strlen("inhabitants=Pikachu");
    EEPROM.write(h.next_write_address,0);
    h.next_write_address += 1;
    EEPROM.put(h.next_write_address, "population=41351");
    h.next_write_address += strlen("population=41351");
    EEPROM.write(h.next_write_address,0);
    h.next_write_address += 1;
#endif
    // Write header values
    EEPROM.put(start_address, h);
    // Calculate checksum
    uint8_t checksum;
    checkTheSum(h.start, h.end, checksum);
    // Write checksum
    EEPROM.write(h.end, checksum);
    // Set variables for other functions
    storer_start = start_address;
    storer_end = end_address;
    // Return OK
    return KeyValueClass::__LK_KV_OK;
  }
}

KeyValueClass::kv_return KeyValueClass::getValue(String key, String &value) {
    String testKey;
    kv_address old_key_address = h.start + sizeof(Header);
    kv_return status = getFirstValue(testKey, value);
    if (status == __LK_KV_EOF) {
      return __LK_KV_NOT_FOUND;
    }
    if (testKey == key) {
      next_key_address = old_key_address;
      return KeyValueClass::__LK_KV_OK;
    }
    do {
      old_key_address = next_key_address;
      status = getNextValue(testKey, value);
      if (status == __LK_KV_EOF) {
        return __LK_KV_NOT_FOUND;
      }
      if (testKey == key) {
        next_key_address = old_key_address;
        return KeyValueClass::__LK_KV_OK;
      }
    } while (status != __LK_KV_EOF);
    return KeyValueClass::__LK_KV_NOT_FOUND;
}

KeyValueClass::kv_return KeyValueClass::getFirstValue(
    String &key, String &value) {
  if (!isEmpty()) {
    next_key_address = readVal(key, value, h.start + sizeof(Header));
    return KeyValueClass::__LK_KV_OK;
  }
  else {
    next_key_address = 0;
    return KeyValueClass::__LK_KV_EOF;
  }
}

KeyValueClass::kv_return KeyValueClass::getNextValue(
    String &key, String &value) {
  if (next_key_address == 0) {
    return KeyValueClass::__LK_KV_NO_FIRST;
  }
  if (next_key_address >= h.next_write_address) {
    next_key_address = 0;
    return KeyValueClass::__LK_KV_EOF;
  } else {
    // read the value
    next_key_address = readVal(key, value, next_key_address);
  }
  return KeyValueClass::__LK_KV_OK;
}

KeyValueClass::kv_return KeyValueClass::destroyDatabase(
    KeyValueClass::kv_address start_address,
    KeyValueClass::kv_address end_address) {
  // destroy the 0xFEEDBEEF
  EEPROM.write(h.start, '\0');
  return KeyValueClass::__LK_KV_OK;
}

KeyValueClass::kv_return KeyValueClass::doPut(String key, String value, bool new_value) {
  String old_value;
  kv_return status = getValue(key, old_value);
  if (status == __LK_KV_OK) {
    // Before shuffling, make sure there is room
    kv_address old_value_length = key.length() + old_value.length() + 2;
    kv_address new_value_length = key.length() + value.length() + 2 + 0;
    if (new_value_length > old_value_length) {
      if ((h.end - h.next_write_address + 1) < new_value_length) {
        return __LK_KV_NO_ROOM;
      }
    }
    kv_address shuffle_length = h.next_write_address - next_key_address  - old_value_length;
    for (kv_address i=0;i<shuffle_length;i++) {
      EEPROM.write(next_key_address + i, EEPROM.read(next_key_address + i + old_value_length));
    }
    h.next_write_address = h.next_write_address - old_value_length;
  }
  if (new_value) {
    if (writeVal(key, value, h.next_write_address)) {
      return __LK_KV_NO_ROOM;
    }
  }
  recomputeChecksum();
  return __LK_KV_OK;
}

KeyValueClass::kv_return KeyValueClass::putValue(String key, String value) {
  return doPut(key, value, true);
}

KeyValueClass::kv_return KeyValueClass::deleteKey(String key) {
  return doPut(key, "", false);
}

void KeyValueClass::eraseEEPROM(KeyValueClass::kv_address start,
    KeyValueClass::kv_address end) {
  for (kv_address addr=start; addr<=end; addr++) {
    EEPROM.write(addr, 0);
  }
}
