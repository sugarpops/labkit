/*
   Lab Kit Key/Value Store library
  
   This code provides a key/value database for saving and retrieving character
   string data in EEPROM.
  
   Keys and values are stored as strings of the form "key=value", with null
   ('\0') character termination. A single key can only have a single value
   and that value can be an empty string (i.e. "foo=" *IS* valid).
  
   Copyright (c) 2017 Ben Goldberg
   MIT License (see LICENSE in this repository)
  
   CHANGES
   NOTES
*/

// Arduino specific defines

#ifndef KEYVALUE_H

#define KEYVALUE_H

#define __KEYVALUE_VERSION 1

#include <Arduino.h>
#include <../../libraries/EEPROM/src/EEPROM.h>

class KeyValueClass {
  public:

    typedef uint8_t kv_return;  // So this can be easily changed
    typedef uint16_t kv_address; // We might use a BIG storage device later!
    static const kv_return __LK_KV_OK = 0;	 // normal completin
    static const kv_return __LK_KV_NOT_FOUND = 1;  // key/database not found
    static const kv_return __LK_KV_BAD_ADDRESS = 2;// cannot create db here
    static const kv_return __LK_KV_NO_ROOM = 3;    // db space exhausted
    static const kv_return __LK_KV_CORRUPT = 4;    // db is unusuable
    static const kv_return __LK_KV_EOF = 5; // end of db
    static const kv_return __LK_KV_UNKNOWN = 6; // unknown error.
    static const kv_return __LK_KV_NO_FIRST = 7; // getNext without getFirst

    static const kv_address __KEY_VALUE_DEFAULT_START_ADDRESS = 64;
    static const kv_address __KEY_VALUE_DEFAULT_END_ADDRESS = 959;

    /*
       Begin use of an existing key/value store, returning __LK_KV_OK if found
       or __LK_KV_NOT_FOUND if not or __LK_KV_CORRUPT if an existing, but
       unusable database is found. The start_address parameter is optional and
       may improve performance, but is required if more than one valid database
       is in memory. The begin function must be used prior to any other function
       or else the function call will return __LK_KV_NOT_FOUND.
     */

    kv_return begin(kv_address start_address =
                    __KEY_VALUE_DEFAULT_START_ADDRESS,
                  kv_address end_address = __KEY_VALUE_DEFAULT_END_ADDRESS);

    /*
       Create a key/value database using all memory locations from
       start_address to end_address, returning __LK_KV_OK for success or
       __LK_KV_BAD_ADDRESS if the ending_address is not greater than
       starting_address by at least __KEYVALUE_MINIMUM_SIZE) or either address
       is out of range. This function DOES NOT imply begin() and the latter
       must be called after a new database is created.
     */

    kv_return create(kv_address start_address =
                     __KEY_VALUE_DEFAULT_START_ADDRESS,
                     kv_address end_address = __KEY_VALUE_DEFAULT_END_ADDRESS);

    /*
       Destroy the current database and erase its storage. Use this function
       with care. __LK_KV_OK is returned if the function suceeds,
        __LK_KV_BAD_ADDRESS if the database does not exist with the exact
       limits specified.
     */
    kv_return destroyDatabase(kv_address start_address,
                               kv_address end_address);

    /*
       Set value to the character string corresponding to the given
       key and return __LK_KV_OK, or return _LK_KV_NOT_FOUND and leave
       value undefined if the key is not in the database.
     */
    kv_return getValue(String key, String &value);

    /*
       Store the key/value pair, replacing any previous value for an existing
       key. Return __LK_KV_OK for success.
     */
    kv_return putValue(String key, String value);

    /*
       Get the first key/value from the store and return __LK_KV_OK or
       __LK_KV_EOF if the database is empty.
     */
    kv_return getFirstValue(String &key, String &value);

    /*
       Get the next key and value from the store and return __LK_KV_OK or
       __LK_KV_EOF if no more exist.
     */
    kv_return getNextValue(String &key, String &value);

    /*
       Delete a key and any value it currently has from the database. Return
       __LK_KV_OK if the key existed and was deleted sucessfully,
       __LK_KV_NOT_FOUND if the key did not already exist.
     */
    kv_return deleteKey(String key);

    /*
      Dump EEPROM contents in hex and printalble ASCII characters.
     */
    void dump(HardwareSerial s, kv_address start = 0, kv_address end = 1023);

    /*
      Erase the EEPROM (sets bytes back to 0xff)
     */
    void eraseEEPROM(kv_address start, kv_address end);

    /*
      Show the header and key/value pairs. Assumes a valid store.
     */
    void showInfo(HardwareSerial s, kv_address start = 0xffff);

    /*
      Analyzes all EEPROM, finding and showing info for all key/value stores
      found.
     */
    void analyze(HardwareSerial s);

  private:
    kv_return doPut(String key, String value, bool new_value);
    bool writeVal(String key, String value, kv_address start);
    void recomputeChecksum();
    bool isEmpty();
    static const uint32_t __MAGIC_NUMBER = 0xEFBEEDFE;

    typedef struct header {
      uint32_t magic;
      kv_address start;
      kv_address end;
      kv_address next_write_address; // recompute this in begin, create, putValue
      uint8_t version;
    } Header;

    Header h;
    kv_address next_key_address = 0;
    kv_address storer_start = 0;
    kv_address storer_data_start = 0;
    kv_address storer_end = 0;
    const kv_address MINIMUM_SIZE = sizeof(Header) + 4;
}; // KeyValueClass

extern KeyValueClass KeyValue;

#endif // NAMEVALUE_H
