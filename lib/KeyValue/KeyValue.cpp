/*
 * Key/Value Store Library
 *
 * This code provides an application programming interface (API) to the
 * name/value database in EEPROM.
 *
 * Copyright (c) 2017-2018 Ben Goldberg with help from Pete Soper
 * MIT License (see LICENSE in this repository)
 * CHANGES:
 *   4/3/18 Updated header comment
 * TODO:
 *
 * BUGS:
 *
 */

#include <KeyValue.h>

/*
 * Global instance
 */

KeyValueClass KeyValue;

/* Private Functions */

/*
This private function checks to see if the store is empty. If it is. return true.
If the store isn't empty, return false.
If empty, the first two values are both null. (aka '\0')
*/

bool KeyValueClass::isEmpty() {
    // if next_write_address is equal to the minimum size of the store, empty
    return h.next_write_address == h.start + sizeof(Header);
}

/* Local Functions */

bool checkTheSum(KeyValueClass::kv_address start_address,
                 KeyValueClass::kv_address end_address, uint8_t &checksum) {
    checksum = 0; // initialize checksum variable
    if (end_address >= 1024) {
        return false; // invalid end address!
    }
    // set checksum to all of the storer bytes added up
    for (KeyValueClass::kv_address i = start_address; i < end_address; i++) {
        checksum += EEPROM.read(i);
    }
    checksum = ~(checksum) + 1; // and take the twos complement
    return (EEPROM.read(end_address) == checksum); // if checksum in EEPROM == calculated checksum,
    // then the checksum is correct
}

/* This local function recomputes the checksum. */
void KeyValueClass::recomputeChecksum() {
    uint8_t checksum;
    EEPROM.put(h.start, h); // put header in
    checkTheSum(h.start, h.end, checksum); // compute checksum
    EEPROM.write(h.end, checksum); // write new checksum
}

/*
   This local function writes a name and value to memory at start and returns
   false if enough space, else true.
*/
bool KeyValueClass::writeVal(String key, String value, kv_address start) {
    kv_address original_start = start;
    if ((h.end - h.next_write_address + 1) <
            (key.length() + value.length() + 2)) {
        return true; // not enough space!
    }
    for (KeyValueClass::kv_address i = 0; i < key.length(); i++) { // Write the key
        EEPROM.write(start + i, key[i]);
    }
    start += key.length();
    EEPROM.write(start, '='); // Write the equals sign
    start += 1;
    for (KeyValueClass::kv_address i = 0; i < value.length() + 1; i++) { // Write the value
        EEPROM.write(start + i, value[i]);
    }
    h.next_write_address = original_start + key.length() + value.length() + 2;
    EEPROM.write(h.next_write_address, 0);
    return false;
}

/* This local function reads a value starting at the variable start. */
KeyValueClass::kv_address readVal(String &key, String &value, KeyValueClass::kv_address start) {
    KeyValueClass::kv_address i = start;
    key = "";
    value = "";
    for (; EEPROM.read(i) != '='; i++) {
        // Get the key
        key += (char)EEPROM.read(i);
    }
    i += 1;
    for (; EEPROM.read(i) != '\0'; i++) {
        // Get the value
        value += (char)EEPROM.read(i);
    }
    // set next key address to read from
    return i + 1;
}

/* Public Functions */

/*
Initializes the key/value store using an existing store.
It checks for a valid header and checksum.
*/
KeyValueClass::kv_return KeyValueClass::begin(kv_address start_address,
        kv_address end_address) {
    // check for header and make sure it contains correct values
    EEPROM.get(start_address, h);
    if (h.magic == __MAGIC_NUMBER && h.start == start_address && h.end == end_address && h.version == __KEYVALUE_VERSION) {
        next_key_address = start_address + sizeof(__MAGIC_NUMBER) + sizeof(Header);
        // verify checksum:
        uint8_t checksum;
        if (checkTheSum(start_address, end_address, checksum)) {
            return KeyValueClass::__LK_KV_OK;
        } else {
            return KeyValueClass::__LK_KV_CORRUPT;
        }
    } else {
        return KeyValueClass::__LK_KV_NOT_FOUND;
    }
}
/*
This function creates a new key/value store at the specified addresses.
*/
KeyValueClass::kv_return KeyValueClass::create(KeyValueClass::kv_address start_address,
        KeyValueClass::kv_address end_address) {
    // erase section of EEPROM for store
    for (uint16_t i = start_address; i < end_address; i++) {
        EEPROM.write(i, 0);
    }

    if ((end_address - start_address) < sizeof(Header) + 3) {
        // Return Error: Bad Address
        return KeyValueClass::__LK_KV_BAD_ADDRESS;
    } else {
        // Make variable to hold header
        // Set header values
        h.magic = __MAGIC_NUMBER;
        h.start = start_address;
        h.end = end_address;
        h.version = __KEYVALUE_VERSION;
        h.next_write_address = next_key_address = start_address + sizeof(Header);
        // Write header values
        EEPROM.put(start_address, h);
        uint8_t checksum;
        checkTheSum(h.start, h.end, checksum);
        // Write checksum
        EEPROM.write(h.end, checksum);
        // Return OK
        return KeyValueClass::__LK_KV_OK;
    }
}

KeyValueClass::kv_return KeyValueClass::getValue(String key, String &value) {
    String testKey;
    kv_address old_key_address = h.start + sizeof(Header); // start of store
    kv_return status = getFirstValue(testKey, value); // get first value
    if (status == __LK_KV_EOF) {
        return __LK_KV_NOT_FOUND;
    }
    if (testKey == key) { // if we found key
        next_key_address = old_key_address;
        return KeyValueClass::__LK_KV_OK; // return OK
    }
    do {
        old_key_address = next_key_address; // set temp key address
        status = getNextValue(testKey, value); // get next value
        if (status == __LK_KV_EOF) {
            return __LK_KV_NOT_FOUND;
        }
        if (testKey == key) { // if we found key
            next_key_address = old_key_address; // set next_key_address
            return KeyValueClass::__LK_KV_OK; // return OK
        }
    } while (status != __LK_KV_EOF);
    return KeyValueClass::__LK_KV_NOT_FOUND;
}


KeyValueClass::kv_return KeyValueClass::getFirstValue(
    String &key, String &value) {
    if (!isEmpty()) { // not empty
        // so read value
        next_key_address = readVal(key, value, h.start + sizeof(Header));
        return KeyValueClass::__LK_KV_OK;
    } else {
        // empty
        next_key_address = 0;
        // so return EOF
        return KeyValueClass::__LK_KV_EOF;
    }
}

KeyValueClass::kv_return KeyValueClass::getNextValue(
    String &key, String &value) {
    if (next_key_address == 0) {
        // you didn't call getFirst before getNext
        return KeyValueClass::__LK_KV_NO_FIRST;
    }
    if (next_key_address >= h.next_write_address) {
        // eof
        next_key_address = 0;
        return __LK_KV_EOF;
    } else {
        // read the value
        next_key_address = readVal(key, value, next_key_address);
    }
    return KeyValueClass::__LK_KV_OK;
}

KeyValueClass::kv_return KeyValueClass::destroyDatabase(
    KeyValueClass::kv_address start_address,
    KeyValueClass::kv_address end_address) {
    // mess up the header so it's invalid ;-)
    EEPROM.write(start_address, '\0');
    return KeyValueClass::__LK_KV_OK;
}

KeyValueClass::kv_return KeyValueClass::doPut(String key, String value, bool new_value) {
    String old_value;
    kv_return status = getValue(key, old_value);
    if (status == __LK_KV_OK) {
        // Before shuffling, make sure there is room
        kv_address old_value_length = key.length() + old_value.length() + 2;
        kv_address new_value_length = key.length() + value.length() + 2 + 0;
        if (new_value_length > old_value_length) {
            if ((h.end - h.next_write_address + 1) < new_value_length) {
                return __LK_KV_NO_ROOM;
            }
        }
        // shuffle
        kv_address shuffle_length = h.next_write_address - next_key_address  - old_value_length;
        for (kv_address i = 0; i < shuffle_length; i++) {
            EEPROM.write(next_key_address + i, EEPROM.read(next_key_address + i + old_value_length));
        }
        h.next_write_address = h.next_write_address - old_value_length;
    }
    if (new_value) {
        // write a new value
        if (writeVal(key, value, h.next_write_address)) {
            return __LK_KV_NO_ROOM;
        }
    }
    recomputeChecksum(); // compute new checksum
    return __LK_KV_OK;
}


KeyValueClass::kv_return KeyValueClass::putValue(String key, String value) {
    return doPut(key, value, true); // true because we want to make a new value
}

KeyValueClass::kv_return KeyValueClass::deleteKey(String key) {
    return doPut(key, "", false); // false because we only want to delete
}


/*
  Erase (set to 0xff) all EEPROM for the given address range
 */

void KeyValueClass::eraseEEPROM(KeyValueClass::kv_address start,
                                KeyValueClass::kv_address end) {
    for (kv_address addr = start; addr <= end; addr++) {
        EEPROM.write(addr, 0);
    }
}

#ifdef DEBUG

/*
  Show the header and key/value pairs. Assumes a valid store.
 */

void KeyValueClass::showInfo(HardwareSerial s, kv_address addr) {
    char line[40]; // Careful to make this long enough!!
    if (addr == 0xFFFF) {
        addr = h.start;
    }
    // format line
    sprintf(line,
            "start: %x end: %x next_write_address: %x version: %d checksum: %x\n",
            h.start, h.end, h.next_write_address, h.version, EEPROM.read(h.end));
    // print it
    s.print(line);
    // get addresses before end of key/value pairs
    while (addr < h.next_write_address) {
        s.print(addr, HEX);
        s.print(" ");
        do {
            s.write(EEPROM.read(addr++));
        } while (EEPROM.read(addr) != 0);
        s.println();
        addr += 1;
    }
}

/*
  Analyzes all EEPROM, finding and showing info for all key/value stores
  found.
 */
// currently corrupts RAM :-(
void KeyValueClass::analyze(HardwareSerial s) {
    Header lh;
    kv_address addr = 0;
    // for every address
    while (addr < 1024) {
        EEPROM.get(addr, lh); // get value
        if (lh.magic == __MAGIC_NUMBER) { // if we found magic #
            uint8_t checksum;
            if (checkTheSum(lh.start, lh.end, checksum)) { // check sum
                showInfo(s, lh.start); // show info
                addr = lh.end + 1;
            } else {
                addr += 1;
            }
        } else {
            addr += 1;
        }
    }
}

/*
 Dump EEPROM contents in hex and printalble ASCII characters.
*/
// currently corrupts RAM :-(
void KeyValueClass::dump(HardwareSerial s, KeyValueClass::kv_address start,
                         KeyValueClass::kv_address end) {
    if (start == 0xffff) { // start wasn't entered
        start = h.start;
        end = h.end;
    }
    char line[20];
    do {
        // print EEPROM contents
        sprintf(line, "0x%04x ", start);
        s.print(line);
        for (uint8_t i = 0; i < 16; i++) {
            sprintf(line, "%02x ", EEPROM.read(start + i));
            s.print(line);
        }
        for (uint8_t i = 0; i < 16; i++) {
            uint8_t v = EEPROM.read(start + i);
            if ((v < ' ') || (v > 127)) {
                line[i] = '.';
            } else {
                line[i] = v;
            }
        }
        line[16] = '\0';
        s.println(line);
        start += 16;
    } while (start <= end);
}

#endif
