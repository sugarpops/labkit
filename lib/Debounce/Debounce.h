/*
 * Debounce.h - Experimental switch debouncing API
 *
 * This code provides very limited switch debouncing support for the four
 * pushbutton switches in the labkit with a mixture of regular and ad hoc
 * handling.
 *
 * See public interfaces below for details.
 *
 * Copyright (c) 2018 Pete Soper
 * MIT License (see LICENSE in this repository)
 *
 * API CHANGES:
 *   <change date> <API interface syntax/semantic change commit message>
 * API TODO:
 *   <todo entry date> <planned interface change description and/or issue ref>
 * API BUGS AND FEATURE REQUESTS:
 *   <report date> <API bug description and/or issue reference>
 */

// Arduino specific defines

#ifndef DEBOUNCE_H

#define DEBOUNCE_H

#define __DEBOUNCE_VERSION 1

#include <MsTimer2.h>
#include <SimplePinChange.h>
#include <Arduino.h>
#include <RingBuffer.h>

class DebounceClass {
    public:
        // Set up switch press and timer interrupts
        void begin();

        // Disable all interrupts
        void stop();

        // Print the state of the switch handling to Serial
        void showState();
};

// Reference the global instance defined in the implementation (cpp) file

extern DebounceClass Debounce;

#endif // DEBOUNCE_H
