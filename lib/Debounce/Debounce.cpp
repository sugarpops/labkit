/*
 * Debounce.cpp - Implementation of Debounce.h API
 *
 * See <.h source filename> for API details.
 *
 * This code isn't really useful for an application in its current state.
 *
 * Copyright (c) 2018 <author(s)>
 * MIT License (see LICENSE in this repository)
 *
 * IMPLEMENTATION CHANGES:
 *   <change date> <change commit message>
 * IMPLEMENTATON TODO:
 *  (historic) Smarten up interrupt reenable by leveraging button_to_pin
 *             (get rid of the switch statement)
 *             Don't store "i+1" instead of the actual button  ordinal value!
 *             Store the ordinal and then go fix the apps using this  library
 *             and expecting "ordinal + 1".
 *             Use separate interrupt handlers: Pins 2 and 3 can be distinct
 *             and this makes discriminating the pin 4 and 5 interrupts easier.
 * IMPLEMENTATON BUGS AND FEATURE REQUESTS:
 *   <report date> <bug/feature description or issue reference>
 */

#include <LabKit.h>
#include <RingBuffer.h>
#include <Debounce.h>

DebounceClass Debounce;

// Elapsed milliseconds at start of program execution. Expressing time in terms
// of a difference between this and millis()  makes state output easier to read.

uint32_t startMillis;

// Current state of the button pins

static uint8_t button_state[4];

// Switch states. An interrupt moves from the idle state, then after two
// "tick" time periods the switch pin is sampled again, and a valid switch
// press is detected or "noise" is ignored.

static const uint8_t BSTATE_IDLE = 0;  // nothing happening
static const uint8_t BSTATE_INITIAL_INTERRUPT = 1; // Initial interrupt
static const uint8_t BSTATE_TIMING_COUNT1 = 2; // first timing period
static const uint8_t BSTATE_TIMING_COUNT2 = 3; // second timing period

// Assume starting state is high

static uint8_t old_port_state = _BV(2) | _BV(3) | _BV(4) | _BV(5);

// Pin change enable bits

static uint8_t enable = _BV(2) | _BV(3) | _BV(4) | _BV(5);

/*
 * Previous state so showState only shows changes.
 */

static uint8_t old_state[4] = {255, 255, 255, 255};


/*
 * Brute force determination of all changes with a single ISR.
 */

void buttonInterrupt() {
    uint8_t new_state = PIND;
    if ((enable & _BV(2)) && ((new_state & _BV(2)) ^ (old_port_state & _BV(2)))) {
        // pin 2 change
        button_state[LabKit.BUTTON_2_ORD] = BSTATE_INITIAL_INTERRUPT;
        enable &= ~_BV(2);
    }
    if ((enable & _BV(3)) && ((new_state & _BV(3)) ^ (old_port_state & _BV(3)))) {
        // pin 3 change
        button_state[LabKit.BUTTON_1_ORD] = BSTATE_INITIAL_INTERRUPT;
        enable &= ~_BV(3);
    }
    if ((enable & _BV(4)) && ((new_state & _BV(4)) ^ (old_port_state & _BV(4)))) {
        // pin 4 change
        button_state[LabKit.BUTTON_4_ORD] = BSTATE_INITIAL_INTERRUPT;
        enable &= ~_BV(4);
    }
    if ((enable & _BV(5)) && ((new_state & _BV(5)) ^ (old_port_state & _BV(5)))) {
        // pin 5 change
        button_state[LabKit.BUTTON_3_ORD] = BSTATE_INITIAL_INTERRUPT;
        enable &= ~_BV(5);
    }
    old_port_state = new_state;
}

// Periodic timer interrupt to manage the switch press state machine

void tick() {

    // For all four buttons
    for (uint8_t i = 0; i < 4; i++) {

        // Determine current and next states with appropriate actions
        switch (button_state[i]) {

        // Nothing happening: keep doing nothing
        case BSTATE_IDLE:
            break;

        // Switch interrupt happened: start first timing period
        case BSTATE_INITIAL_INTERRUPT:
            button_state[i] = BSTATE_TIMING_COUNT1;
            break;

        // First timing period happened: start second timing period
        case BSTATE_TIMING_COUNT1:
            button_state[i] = BSTATE_TIMING_COUNT2;
            break;

        // Second timing period happened: determine debounced switch state
        case BSTATE_TIMING_COUNT2:

            // Buffer switch if its pin is (active) low as a bonafide press
            if (!digitalRead(LabKit.button_to_pin[i])) {

                // There is a valid, debounced switch press detected. Add it to the
                // buffer with button ordinal (0 == button 1, 1 == 2, etc)

                RingBuffer.putData(i, millis() - startMillis);
            }

            // Next state is idle
            button_state[i] = BSTATE_IDLE;

            // Reenable interrupts for the button's pin. Ugh!
            switch (i) {
            case 0:
                old_port_state = PIND;
                enable |= _BV(3);
                break;
            case 1:
                old_port_state = PIND;
                enable |= _BV(2);
                break;
            case 2:
                old_port_state = PIND;
                enable |= _BV(5);
                break;
            case 3:
                old_port_state = PIND;
                enable |= _BV(4);
                break;
            }
            break;
        }
    }
}

/*
 * Print the state table. Need to avoid pressing buttons while this code
 * is executing or else make a critical section.
 */

void DebounceClass::showState() {
    for (uint8_t i = 0; i < 4; i++) {
        if (button_state[i] != old_state[i]) {
            Serial.print(i);
            switch (button_state[i]) {
            case BSTATE_IDLE:
                Serial.println(" IDLE");
                break;
            case BSTATE_INITIAL_INTERRUPT:
                Serial.println(" INT");
                break;
            case BSTATE_TIMING_COUNT1:
                Serial.println(" C1");
                break;
            case BSTATE_TIMING_COUNT2:
                Serial.println(" C2");
                break;
            }
            old_state[i] = button_state[i];
        }
    }
}

/*
 * Initialize and start the switch debouncing support.
 *
 * It is assumed that LabKit.begin has set the four pin ports to INPUT_PULLUP
 */

void DebounceClass::begin() {
    // Make timer 2 cause the tick function to be called every 10 milliseconds

    MsTimer2::set(10, tick);
    MsTimer2::start();

    SimplePinChange.attach(LabKit.BUTTON_1, buttonInterrupt);
    SimplePinChange.attach(LabKit.BUTTON_2, buttonInterrupt);
    SimplePinChange.attach(LabKit.BUTTON_3, buttonInterrupt);
    SimplePinChange.attach(LabKit.BUTTON_4, buttonInterrupt);

    // record the starting time in elapsed milliseconds

    startMillis = millis();
}

/*
 * Stop switch debouncing
 */

void DebounceClass::stop() {
    MsTimer2::stop();
    SimplePinChange.detach(2);
    SimplePinChange.detach(3);
    SimplePinChange.detach(4);
    SimplePinChange.detach(5);
    uint8_t old_port_state = _BV(2) | _BV(3) | _BV(4) | _BV(5);
    uint8_t enable = _BV(2) | _BV(3) | _BV(4) | _BV(5);
}
