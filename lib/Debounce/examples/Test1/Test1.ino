/*
 * Test1.ino - Test the switch debouncing.
 *
 * This is an initial test of the debounce library that was never finished.
 * In particular, the library should present a "next press" method and not
 * expose a ring buffer.
 *
 * Copyright (c) 2017 Pete Soper
 * MIT License (see LICENSE in this repository)
 *
 * CHANGES:
 *   <change date> <change commit message>
 * TODO:
 *   <todo date> <todo item description and/or issue reference>
 * BUGS AND FEATURE REQUESTS:
 *   5/1/18 - Finish the library, then revisit this
 */

#define LABKIT_DEBUG 1

#include <LabKit.h>
#include <RingBuffer.h>
#include <MsTimer2.h>
#include <SimplePinChange.h>
#include <Debounce.h>

// Set up the hardware

void setup() {
    // Set up kit hardware
    LabKit.begin();
    // (This initializes the ringbuffer)
    Debounce.begin();
}

// Display the ring buffer contents, insertion and extraction points twice
// and then hang to allow inspection.

void loop() {
    //Debounce.showState();
    RingBufferClass::RBElement e;
    while (RingBuffer.available()) {
        RingBuffer.getData(e);
        Serial.print(e.data);
        Serial.print(" ");
        Serial.println(e.timestamp);
    }
}
