/*
 * Task.cpp - Implementation of Task.h API
 * See Task.h for API details.
 *
 * Big rule for the implementation: the run queue must always be able to
 * contain all the TCB pointers at the same time. There are many tests for
 * overflow to detect logic bugs that will eventualy be removed.
 *
 * Copyright (c) 2017, 2018 Pete Soper
 * MIT License (see LICENSE in this repository)
 *
 * IMPLEMENTATION CHANGES:
 *   <change date> <change commit message>
 * IMPLEMENTATON TODO:
 *   4/8/2018 - Finish moving the function header comments describing the APIs 
 *              into the .h file and leave a terse summary and implementation 
 *              details here.
 * IMPLEMENTATON BUGS:
 */

#include <Task.h>

// The maximum number of tasks. If an attempt to exceed this number is
// made the calling function gets a NO_TASKID return value instead of a
// valid task id. This limit can be overridden when the library is compiled
// by using a switch (e.g. -DMAX_TASKS=8)

#ifndef MAX_TASKS
#define MAX_TASKS 8
#endif

// Inline code macros. These are experiments to if them sitting between code 
// statements is good, bad, or indifferent.

// Macros for determining if interrupts are disabled. 

#define IN_INTERRUPT (!(SREG & SREG_I))
#define NOT_IN_INTERRUPT (SREG & SREG_I)

// Macros for disabling interrupts and restoring previous state. A KEY POINT is 
// that if these macros are used when interrupts are already disabled they are 
// nops. Currently the Task and it's ring buffer (FIFO) are very conservative
// and in some cases interrupts will be disabled three times on some paths.
// So the current state is always saved and restored vs any use of sei(). 
// It's up to the user to use local scopes to avoid duplicate declarations of
// sreg.

#define DISABLE_INTERRUPTS uint8_t sreg = SREG;cli();
#define RESTORE_INTERRUPT_STATE SREG = sreg;

// Task control block flags

// State
const uint8_t tcb_active = 0x80;    // TCB in use: this task "exists"
const uint8_t tcb_sleeping = 0x40;  // scheduled task timer counting down
const uint8_t tcb_runable = 0x20;   // on run queue

// Attribute
const uint8_t tcb_keepalive = 0x10; // eligible to run again

// Type of task (regular type is neither scheduled nor waiting)
const uint8_t tcb_scheduled = 0x8; // waiting for elapsed time when not running
const uint8_t tcb_waiting = 0x4;   // waiting for event when not running

const uint8_t tcb_spare1 = 0x2;
const uint8_t tcb_spare2 = 0x1;

// Task control block structure

typedef struct {
    volatile uint8_t flags;             // task state
    uint8_t taskid;                   // task unique id (convenience: is element #)
    uint32_t sleep_ticks;             // Milliseconds per sleep
    volatile uint32_t sleep_done_time;// runable when millis() reaches this value
    Task::void_func func;             // task function
    void *local;                          // pointer to task local data
} task_control_block;

typedef task_control_block *TCB;

// Array of TCBs holding state of active tasks

static volatile TCB tcbs[MAX_TASKS];

// The id of the currently running task or NO_TASKID when no task is running
// (i.e. inside the scheduler or an interrupt handler invoked while within
// the scheduler). It isn't clear yet if this will serve a purpose.

volatile uint8_t running_task = Task::NO_TASKID;

// The FIFO run queue

NRingBuffer run_queue = NRingBuffer(MAX_TASKS);

#ifdef DEBUG

/*
 * Dump the task control blocks for debug info.
 */

void showTCB(uint8_t taskid, const char *msg) {
    Serial.print(msg);
    Serial.print(" T");
    Serial.print(taskid);
    Serial.print("-");
    if (tcbs[taskid]->flags & tcb_active) {
        Serial.print("A");
    }
    if (tcbs[taskid]->flags & tcb_waiting) {
        Serial.print("W");
    }
    if (tcbs[taskid]->flags & tcb_scheduled) {
        Serial.print("S");
    }
    if (tcbs[taskid]->flags & tcb_keepalive) {
        Serial.print("K");
    }
    if (tcbs[taskid]->flags & tcb_runable) {
        Serial.print("R");
    }
    if (tcbs[taskid]->sleep_ticks > 0) {
        if (millis() < tcbs[taskid]->sleep_done_time) {
            Serial.print("(");
            Serial.print(tcbs[taskid]->sleep_done_time - millis());
            Serial.print(")");
        }
    }
    Serial.print("[");
    if (tcbs[taskid]->local != NULL) {
        Serial.print(*((uint8_t *)tcbs[taskid]->local), HEX);
    }
    Serial.print("]");
    Serial.print(" ");
}

void Task::dump(uint8_t taskid, const char *msg, bool show_queue) {
    if (taskid != NO_TASKID) {
        Serial.print(taskid);
        Serial.print(" ");
    }
    Serial.println(msg);
    if (show_queue) {
        Serial.print(" ");
        run_queue.showData();
        return;
    }
    if (run_queue.available() > 0) {
        Serial.print("(");
        Serial.print(run_queue.available());
        Serial.print(") ");
    }
    for (uint8_t i = 0; i < MAX_TASKS; i++) {
        showTCB(i, "");
    }
    Serial.println();
    delay(3000);
}

#endif // DEBUG

/*
 * Make the specified task runable.
 */

void Task::event(uint8_t taskid) {

    DISABLE_INTERRUPTS
    if (tcbs[taskid]->flags & tcb_runable) {
        // already on the run queue: treat as spurious (e.g. switch bounce)
        RESTORE_INTERRUPT_STATE
        return;
    }
    run_queue.putData(tcbs[taskid], false);
    tcbs[taskid]->flags |= tcb_runable;
    RESTORE_INTERRUPT_STATE
    return;
}

/*
 * Initialize the task control blocks.
 *
 */

void Task::begin() {
    for (uint8_t i = 0; i < MAX_TASKS; i++) {
        tcbs[i] = (TCB) malloc(sizeof(task_control_block));
        tcbs[i]->taskid = i;
        tcbs[i]->flags = 0;
        tcbs[i]->sleep_ticks = 0;
        tcbs[i]->local = NULL;
    }
}

/*
 * Go through the TCBs, looking for a scheduled task that needs to be put on
 * the run queue.  Then go through the runable task queue and run the next
 * one and freeing the metadata if keepalive is false. Rinse and repeat. This
 * function never returns.
 */

uint8_t Task::scheduler() {
    while (true) {
#ifdef DEBUG1
        dump(NO_TASKID, "before TCB scan");
#endif
        // Traverse the TCBs looking for ones that are scheduled
        for (uint8_t ti = 0; ti < MAX_TASKS; ti++) {
            TCB t = tcbs[ti];
            if (! (t->flags & tcb_active)) {
                // This TCB not in use
                break;
            }
            if ((t->flags & tcb_scheduled) && ! (t->flags & tcb_runable)) {
                // Is scheduled - see if finished
#ifdef DEBUG2
                Serial.println("sleep time test");
#endif
                if (millis() >= t->sleep_done_time) {
#ifdef DEBUG1
                    Serial.println("sleep time satisfied");
#endif
                    // The keepalive flag here means after the task has run it
                    // should be rescheduled. Do that now so the scheduling
                    // involves equal intervals.
                    if (t->flags & tcb_keepalive) {
                        t->sleep_done_time = t->sleep_ticks + millis();
                    } else {
                        t->flags &= ~tcb_scheduled;
                    }

                    // Add to the run queue
                    if (!run_queue.putData(t, false)) {
                        Serial.println("ring buffer already full 1");
                    }
                    t->flags |= tcb_runable;
#ifdef DEBUG1
                    dump(t->taskid, "sleep finished and task ready to run");
#endif
                } else {
#ifdef DEBUG1
                    dump(t->taskid, "still sleeping");
#endif
                }
            }
        }

        if (run_queue.available() > 0) {
            TCB t = PTR_COPY(run_queue.getData(false), task_control_block);
            if (!t) {
                Serial.println("FATAL: should not get null from getData");
            }
            if (!t) {
#ifdef DEBUG1
                dump(NO_TASKID, "runable queue exhausted");
#endif
                continue;
            } else {
#ifdef DEBUG1
                dump(NO_TASKID, "after removing item from runable");
#endif
            }

            // Keep an interrupt handler from seeing only have of these changes.
            // Not clear yet if this is necessary.
	    DISABLE_INTERRUPTS
            t->flags &= ~tcb_runable;
            running_task = t->taskid;
	    RESTORE_INTERRUPT_STATE
            t->func();
            cli(); // SREG ALREADY SAVED
            running_task = NO_TASKID;
	    RESTORE_INTERRUPT_STATE
            if (t->flags & tcb_keepalive) {
                if ((t->flags & tcb_waiting) || (t->flags & tcb_scheduled)) {
#ifdef DEBUG2
                    Serial.println("back to waiting");
#endif
                    continue;
                } else if (t->flags & tcb_waiting) {
                    // event task - nothing to do
                } else {
                    // regular task simply put back on end of queue
                    if (!run_queue.putData(t, false)) {
                        Serial.println("ring buffer full 3");
                    }
                    t->flags |= tcb_runable;
#ifdef DEBUG1
                    dump(t->taskid, "task put back on runable queue");
#endif
                }
            } else {
                // task completed - recycle TCB
                t->flags = 0;
#ifdef DEBUG1
                dump(t->taskid, "task deactivated after running");
#endif
            }
        } else {
#ifdef DEBUG1
            Serial.println("runable queue is empty");
#endif
        }
    }
}

/*
 * Get the next available task id or return NO_TASKID if none available.
 */

uint8_t getTaskid() {
    static uint8_t start_id = 0;  // Hint for finding a likely unused TCB
    // Find an id that isn't in use.
    uint8_t id = start_id++;
    do {
        if (!(tcbs[id]->flags & tcb_active)) {
            tcbs[id]->flags = tcb_active;
            return id;
        } else {
            id = (id + 1) % MAX_TASKS;
        }
    } while (id < start_id);
    // Wrapped around
    return Task::NO_TASKID;
}

/*
 * Prepare a task. Common to createTask and modifyParameters.
 */

void setupTask(uint8_t taskid, Task::task_type t, Task::void_func func,
               uint32_t wait_ms, bool keepalive, void *local) {
    if (keepalive) {
        tcbs[taskid]->flags |= tcb_keepalive;
    }
    tcbs[taskid]->func = func;
    tcbs[taskid]->local = local;
    switch (t) {
    case Task::scheduled_task:
        tcbs[taskid]->flags |= tcb_scheduled | tcb_sleeping;
        tcbs[taskid]->sleep_ticks = wait_ms;
        tcbs[taskid]->sleep_done_time = millis() + wait_ms;
        break;
    case Task::event_task:
        tcbs[taskid]->flags |= tcb_waiting;
        break;
    case Task::regular_task:
        run_queue.putData(tcbs[taskid], false);
        tcbs[taskid]->flags |= tcb_runable;
        break;
#ifdef DEBUG1
    default:
        Serial.println("TASK SYSTEM FAILURE: unknown task type");
        break;
#endif
    }
}

/*
 * Schedule a task to run after a given delay.
 */

uint8_t Task::createTask(void_func func, task_type t,
                         uint32_t wait_milliseconds, bool keepalive,
                         void *local) {
    uint8_t taskid;
    DISABLE_INTERRUPTS
    if ((taskid = getTaskid()) != NO_TASKID) {
        tcbs[taskid]->taskid = taskid;
        setupTask(taskid, t, func, wait_milliseconds, keepalive, local);
    }
    RESTORE_INTERRUPT_STATE
    return taskid;
}

/*
 * Return the task's local data pointer
 */

void *Task::getLocal(uint8_t taskid) {
    return tcbs[taskid]->local;
}

/*
 * Update the task's local data pointer
 */

void Task::setLocal(uint8_t taskid, void *p) {
    tcbs[taskid]->local = p;
}

/*
 * Get the task's ID
 */

uint8_t Task::getID() {
    return running_task;
}

/*
 * Alter a task's type and/or attributes.
 */

void Task::modifyTask(uint8_t taskid, task_type type, void_func func,
                         uint32_t sleep_ms, bool keepit, void *local) {
    DISABLE_INTERRUPTS
#ifdef DEBUG1
    showTCB(taskid, "before setParameters");
    Serial.println();
#endif
    // clear everything but active
    tcbs[taskid]->flags &= tcb_active;
    setupTask(taskid, type, func, sleep_ms, keepit, local);
    RESTORE_INTERRUPT_STATE
#ifdef DEBUG1
    showTCB(taskid, "after setParameters");
    Serial.println();
#endif
}
