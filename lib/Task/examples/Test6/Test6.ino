/*
 * Test6.ino - Self-modifying task
 *
 * A scheduled task blinks an LED twice and pauses, then turns itself into a
 * regular task that blinks the LED three times and then turns itself into a
 * scheduled task (that runs one second later, as with the original sleeper).
 *
 * Copyright (c) 2018 Pete Soper
 * MIT license (see LICENSE in this repository)
 *
 * CHANGES:
 * TODO:
 * BUGS AND FEATURE REQUESTS:
 */

#include <NRingBuffer.h>
#include <Task.h>

void blink(uint8_t count) {
    for (uint8_t i = 0; i < count; i++ ) {
        digitalWrite(13, HIGH);
        delay(100);
        digitalWrite(13, LOW);
        delay(100);
    }
    delay(1000);
}

void morphicTask() {
    static bool switch_to_regular = true;
    if (switch_to_regular) {
        blink(2);
        Serial.println("scheduled task");
        Task::modifyTask(Task::getID(), Task::regular_task, morphicTask,
                            0, true, NULL);
    } else {
        // switch to scheduled ask
        blink(3);
        Serial.println("regular task");
        Task::modifyTask(Task::getID(), Task::scheduled_task, morphicTask,
                            1000, true, NULL);
    }
    switch_to_regular = ! switch_to_regular;
}

void setup() {
    Serial.begin(9600);
    pinMode(13, OUTPUT);
    Serial.println("Task Test 6");
    Task::begin();
    if (Task::createTask(morphicTask, Task::scheduled_task, 1000, true, NULL)) {
        Serial.println("createTask failed");
    }
    if (Task::scheduler()) {
        Serial.println("impossible scheduler failure");
    }
}

void loop() {
}
