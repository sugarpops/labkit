/*
 * Test4.ino - Interrupt-driven event task
 *
 * A periodic interrupt every second signals an event to schedule
 * a task that blinks once.
 *
 * Copyright (c) Pete Soper
 * MIT license (see LICENSE in this repository)
 *
 * CHANGES:
 * TODO:
 * BUGS AND FEATURE REQUESTS:
 */

#include <NRingBuffer.h>
#include <Task.h>
#include <MsTimer2.h>

static uint8_t tid;
static int count;

void eventTask() {
    Serial.println("Event!");
    digitalWrite(13, HIGH);
    delay(100);
    digitalWrite(13, LOW);
    delay(100);
}

void tick() {
    Task::event(tid);
}

void setup() {
    Serial.begin(9600);
    pinMode(13, OUTPUT);
    Serial.println("Task Test 4");
    Task::begin();
    tid = Task::createTask(eventTask, Task::event_task, 0, true, NULL);
    if (tid == Task::NO_TASKID) {
        Serial.println("createTask failed 1");
    }
    MsTimer2::set(1000, tick);
    MsTimer2::start();
    if (Task::scheduler()) {
        Serial.println("impossible scheduler failure");
    }
}

void loop() {
}
