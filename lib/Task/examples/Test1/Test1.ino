/*
 * Test1.ino - Sequentially scheduled tasks
 *
 * Four tasks are run in round robin fashion and each prints its ID and blinks
 * the pin 13 LED ID+1 times and pauses.
 *
 * Copyright (c) Pete Soper
 * MIT license (see LICENSE in this repository)
 *
 * CHANGES:
 * TODO:
 * BUGS AND FEATURE REQUESTS:
 */

#include <NRingBuffer.h>
#include <Task.h>

void blink() {
    uint8_t tid = Task::getID();
    uint8_t state = LOW;
    uint16_t d = 100 + (100 * tid);
    Serial.print("I am task ");
    Serial.println(tid);
    for (uint8_t blinks = 0; blinks <= tid; blinks++) {
        digitalWrite(13, state = !state);
        delay(200);
        digitalWrite(13, state = !state);
        delay(200);
    }
    delay(500);
}

void setup() {
    Serial.begin(9600);
    pinMode(13, OUTPUT);
    Serial.println("Task Test 1");
    Task::begin();
    if (Task::createTask(blink, Task::regular_task, 0, true, NULL) ==
            Task::NO_TASKID) {
        Serial.println("Task 0 not created");
    }
    if (Task::createTask(blink, Task::regular_task, 0, true, NULL) ==
            Task::NO_TASKID) {
        Serial.println("Task 1 not created");
    }
    if (Task::createTask(blink, Task::regular_task, 0, true, NULL) ==
            Task::NO_TASKID) {
        Serial.println("Task 2 not created");
    }
    if (Task::createTask(blink, Task::regular_task, 0, true, NULL) ==
            Task::NO_TASKID) {
        Serial.println("Task 3 not created");
    }
    if (Task::scheduler() == Task::NO_TASKID) {
        Serial.println("Imposible failure of the scheduler");
    }
}

void loop() {
}
