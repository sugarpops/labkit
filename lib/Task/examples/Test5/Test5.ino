/*
 * Test5.ino - Periodically create a task
 *
 * A scheduled task creates a new task without "keep alive" that gets
 * scheduled to run and blink the LED.
 *
 * Copyright (c) Pete Soper
 * MIT license (see LICENSE in this repository)
 *
 * CHANGES:
 * TODO:
 * BUGS AND FEATURE REQUESTS:
 */

#include <NRingBuffer.h>
#include <Task.h>

void oneShotTask() {
    Serial.println("Bang");
    digitalWrite(13, HIGH);
    delay(100);
    digitalWrite(13, LOW);
    delay(100);
}

void scheduledTask() {
    Task::createTask(oneShotTask, Task::regular_task, 0, false, NULL);
}

void setup() {
    Serial.begin(9600);
    pinMode(13, OUTPUT);
    Serial.println("Task Test 5");
    Task::begin();
    if (Task::createTask(scheduledTask, Task::scheduled_task, 1000, true, NULL)) {
        Serial.println("createTask failed");
    }
    if (Task::scheduler()) {
        Serial.println("impossible scheduler failure");
    }
}

void loop() {
}
