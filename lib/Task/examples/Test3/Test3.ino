/*
 * Test3.ino - Event task
 *
 * One task scheduled to run each second, signaling an event to schedule
 * a second task that blinks once.
 *
 * Copyright (c) Pete Soper
 * MIT license (see LICENSE in this repository)
 *
 * CHANGES:
 * TODO:
 * BUGS AND FEATURE REQUESTS:
 */

#include <NRingBuffer.h>
#include <Task.h>

/*
 * Just announce
 */

static uint8_t tid;

void eventTask() {
    Serial.println("Event!");
    digitalWrite(13, HIGH);
    delay(100);
    digitalWrite(13, LOW);
    delay(100);
}

void scheduledTask() {
    Task::event(tid);
}

void setup() {
    Serial.begin(9600);
    pinMode(13, OUTPUT);
    Serial.println("Task Test 3");
    Task::begin();
    if (Task::createTask(scheduledTask, Task::scheduled_task, 1000, true, NULL)) {
        Serial.println("createTask failed 1");
    }
    tid = Task::createTask(eventTask, Task::event_task, 0, true, NULL);
    if (tid == Task::NO_TASKID) {
        Serial.println("createTask failed 2");
    }
    if (Task::scheduler()) {
        Serial.println("impossible scheduler failure");
    }
}

void loop() {
}
