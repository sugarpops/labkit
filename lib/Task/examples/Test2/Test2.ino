/*
 * Test2.ino - Task scheduling by time
 *
 * Test Task library scheduling functionality. A task is run every
 * second, blinking count times with count going from 1 to 4. Notice
 * the blinking occurs once per second with constant interval, because
 * each time a scheduled interval is complete, the next time to make
 * the task runable is set immediately and is insensitive to the
 * "running time" of the task unless it exceeds the scheduling interval
 * time.
 *
 * Copyright (c) Pete Soper
 * MIT license (see LICENSE in this repository)
 *
 * CHANGES:
 * TODO:
 * BUGS AND FEATURE REQUESTS:
 */

#include <NRingBuffer.h>
#include <Task.h>

void countTask() {
    static uint8_t counter = 1;
    Serial.println(counter);
    for (int i = 0; i <= counter; i++) {
        digitalWrite(13, HIGH);
        delay(100);
        digitalWrite(13, LOW);
        delay(100);
    }
    counter = (counter + 1) % 4;
}

void setup() {
    Serial.begin(9600);
    pinMode(13, OUTPUT);
    Serial.println("Task Test 2");
    Task::begin();
    if (Task::createTask(countTask, Task::scheduled_task, 2000, true, NULL)) {
        Serial.println("createTask failed");
    }
    if (Task::scheduler()) {
        Serial.println("impossible scheduler failure");
    }
}

void loop() {
}
