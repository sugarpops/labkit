/*
 * Task.h - An experimental cooperative multitasking API
 *
 * This code provides a very simple Task interface for switching between
 * different application functions.
 *
 * An application is intended to get started with the Task library as follows:
 *  call Task::begin() to initialize the task library
 *  call Task::createTask one or more times to create an initial set of tasks
 *  call Task::scheduler to start task scheduling
 *
 * Tasks come in three flavors: regular, scheduled, and event. Each
 * task has an application function associated with it and running the task 
 * consists of calling the application function. After the function returns,
 * if a task has the "keepalive" attribute specified at creation time (or
 * added before the ask is run, or even added by itself while running) then 
 * the task is eligable to run again; otherwise it is deleted from the system 
 * and its metadata is recycled. 

 * A regular task is made runable at the time it is created, with each regular
 * task being queued in a FIFO: the first one out of the queue executes, and
 * when it's function returns that task is put back on the queue (with
 * keepalive), so a group of regular tasks execute in simple round robin order.
 *
 *  A scheduled task is pended after creation and is made to wait while a 
 * specified time elapses before it is made runable.
 *
 * An event task is pended after creation and is made runable when the event()
 * function is called with its ID.
 *
 * Tasks can have local data of any type by means of a single void pointer
 * specified when the task is created or by using Task::modifyTask.
 *
 * Tasks are identified with a unique ordinal integer value and can determine
 * their own ID and get a pointer to their local data.
 *
 * Tasks can create tasks and can modify other tasks as well as themselves,
 * such as changing a regular task into a scheduled task. Caveats on use of
 * these features are TBD (and some of these features are not fully defined
 * and/or implemented yet!).
 *
 * There are no known limitations for operations within interrupt handlers
 * except for the length of time spent in the handler (e.g. creating 10 new
 * tasks with interrupts disabled).
 *
 * There is no preemption. If a tasks's function never returns the state of
 * other tasks might be changed due to interrupt handler activity, but the other
 * tasks will never get a chance to execute their functions.
 *
 * The number of tasks supported by the library is the compile time constant
 * MAX_TASKS that currently defaults to 8. It's up to the user to arrange for
 * MAX_TASKS to be defined at library compile time with a -D option: otherwise 
 * it can only be changed by editing the source.
 *
 * See the public declarations below for details.
 *
 * Copyright (c) 2017, 2018 Pete Soper
 * MIT License (See LICENSE in this repository)
 *
 * API CHANGES:
 * API TODO:
 *  4/2/18 - Need careful review of code with respect to these features:
 *           1) task changing itself to/from scheduled type and to/from event
 *              type, including via interrupt handler
 *  4/2/18 - for scheduleTask consider switching mytaskid to VALUE, not
 *           reference. Frequent cause of bugs as reference.
 * API BUGS AND FEATURE REQUESTS:
 *  4/2/18 - (for the LabKit library API) Consider a platform-dependent "It's 
 *           dead, Jim" feature triggered by an API function to indicate a 
 *           fatal state. Perhaps a pattern of blinking the pin 13 LED? 
 * 4/25/2018-A specific use case for a higher order function would be for
 *           switch debouncing. Maybe have a special create call that defines
 *           an interrupt callback to signal press (that knows how to disable
 *           the interrupt) and a second to return the debounced switch state
 *           and reenable the interrupt. 
 * 5/15/18 - A runable queue per task priority would allow for a priority
 *           scheme.
 *           Depending on a tick interrupt would allow scheduled task timing
 *           to be precise, some means of detecting a hang, etc. Patching a
 *           weak reference into the millis() implementation is one possibility.
 *           Decide what's right if a task calls event() on itself.
 */

#ifndef TASK_H

#if 0
#define DEBUG
#define DEBUG1
#define DEBUG2
#endif // 0

#define TASK_H

#define __TASK_VERSION 1

#include <avr/common.h>
#include <NRingBuffer.h>

namespace Task {
    // Task function pointer type

    typedef void (*void_func)();

    // Types of tasks

    typedef enum { regular_task = 1, event_task, scheduled_task } task_type;

    /*
     * This is a special task ID value that means "undefined"
     */

    const uint8_t NO_TASKID = 255;

    /*
     * Dump task control blocks
     */

    void dump(uint8_t taskid, const char *msg, bool show_queue = false);

    /*
     * Initialize the Task environment
     */

    void begin();

    /*
     * Make an event task runable if it isn't already.
     *
     * In theory this could be called on a scheduled or regular task and the
     * former would would be runable (if not already) 'early', but it might be
     * made runable again after executing, while calling it for a regular
     * task would be a noop.
     */

    void event(uint8_t taskid);

    /*
     * Create a task to run whenever possible (regular), after a period of time
     * or (scheduled), or in response to event() being called with its ID 
     * (event). 
     *
     * The func parameter specifies a zero parameter function to call when the
     * task is ready to execute.
     *
     * The wait_milliseconds parameters determiens the earliest delay between
     * creation time and it being made runable (first time) or from the point
     * it is made runable. That is, the interval is >= the delay time specified
     * plus any latency created by another task being ahead of it in the run
     * queue at the point the delay is completed.
     * 
     * If keepalive is true then after a task has run it will stay runable
     * (regular) and its function will be called again after any other runable 
     * tasks have had their turn or else it will be pended again (event or
     * scheduled).
     *
     * Return the new task id or NO_TASKID if there are no resources for an
     * additional task.
     */

    uint8_t createTask(void_func func, task_type type, uint32_t wait_milliseconds,
                       bool keepalive, void *local);

    /*
     * Start the tasking system. There is no return from this function. It's
     * valid for there to be no created tasks, the assumption being an
     * interrupt service routine might create one.
     */

    uint8_t scheduler();

    /*
     * Get the current task's ID
     */

    uint8_t getID();

    /*
     * Change a task's parameters. Currently there are no constraints with
     * this function, but some peculiar behavior might result, such as having
     * the same task in two places within the run queue. Necessary constraits
     * are TBD. 
     */

    void modifyTask(uint8_t taskid, task_type type, void_func func,
                       uint32_t wait_milliseconds, bool keepalive, void *local);

    /*
     * Get a tasks's local data pointer. A taskid of NO_TASKID means get the
     * current task's local data pointer.
     */

    void *getLocal(uint8_t taskid = NO_TASKID);

    /*
     * Set a tasks's local data pointer. A taskid of NO_TASKID means set the
     * current task's local data pointer.
     */

    void setLocal(uint8_t taskid, void *p);
}

#endif // TASK_H
