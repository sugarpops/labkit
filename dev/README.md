# Developer Notes

## General TODO

* implement ucp  (unsecure file copy), a standalone Python script that
    * Loads and starts a "ucp.ino" program on the Arduino
    * Parses and carries out the ucp commandline arguments to:
        * Rename (mv) a LabKit SD card file from one path to another
            * Must gracefully and informatively handle error scenarios
        * Copy (cp) files (local to remote, remote to remote, remote to local)
            * Use ":" to indicate remote (e.g. "ucp cp :some_sd_file /tmp/foo")
        * Delete (rm) an SD card file
        * List (ls) SD card file details
    * ucp may not be secure, but it must be BULLETPROOF
        * High quality hash codes and max retries to either get the data transferred correctly or else give up
        * Consider reusing existing code that implements a mature protocol such as [XYMODEM](https://playground.arduino.cc/Main/XYModem)
            * The Arduino code would be mostly Peter Turczak's XModem code
            * The Python code would mostly be a wrapper
                * Download ucp.hex (if it needs to be downloaded again)
                * Invoke a local XModem program
                * Field and communicate status
* Work around the difference in include file handling between Linux/Cygwin build environments and the Arduino IDE
    * First guess is that the Arduino IDE is smart enough to handle arbitrary include file nesting, while the Linux/Cygwin build environment (built around Suda Muthu's "Arduino-Makefile" system) is limited in this regard
    * Ideal would be to make Sudar's code smarter (or discover a config issue that's preventing it from handling nesting)
    * But first priority is to find the root cause of the problem
    * Ideal goal is to just "#include <LabKit.h>" in ALL .ino files and have things "just work". This might sidestep the need to have a protocol at all
    * Determine necessary rules for max depth (if that's the basic problem)

### Pete TODO list

* Find out how to make readPixel work with the Seeed 2.8" display or else get Seeed Studio to admit and detail the defect that prevents this from working.

### Ben TODO list


