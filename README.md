# labkit
A simple kit for studying embedded systems

This repo contains the state of materials created in conjunction with a special embedded development class taught via Google Hangouts from September, 2017 to 
May, 2018. Some of this is complete and well tested while large portions are incomplete or poorly tested. There is no plan to do anything further with this repo. Any public forks of portions of this repo should be listed below (probably starting with the task library). It was fun.


This code was written by Pete Soper and Ben Goldberg.

```
             /\       /\          __
           _/  \_____/  \        / /
          /              \      / /
         /   _        _   \    / /
        /   |_|      |_|   \   \ \
       /                    \   \ \
      /         \__/         \   \ \
      \      \        /      /    \ \
   ____\      \__/\__/      /____  \ \
  /                              \  \ \
  \____                      ____/  / /
       /                    \      / /
      /                      \    / /
     /                        \  / /
    /                          \/ /
   /                             /
   \                            /
    \       ____________       /
     |     |            |     |
     |_|_|_|            |_|_|_|
```
##                  PIKACHU
