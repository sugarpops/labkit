=== Hardware Notes ===

* Rev1, 2 are Arduino Nano-based, expecting to use 128x64 monochrome OLED
* Rev 3 is Arduino Uno-based, using 320x240 TFT with touch screen
* Rev 4 will be ESP-32 based, using a WROOM module on an Arduino Uno-form factor "ESPDuino32" board with a "sandwich" adaptor board to level shift, bring out signals, and adapt to the TFT touch screen display

