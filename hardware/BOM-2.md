# LabKit version 2 bill of materials 

* Containers
    * partitioned case
    * medium parts case
* Full size breadboad
* Arduino Nano equivalent
* Red, Green, Blue LEDs
* 3x390 ohm, 1/4watt, through hole resistors
* USB A male to mini male cable
* Four position membrane switch (like Adafruit #1332)
(Rev 2)
* Piezo speaker
* 128x64 .96" Monochrome I2C Display
* ST M41T62 RTC chip on [ugly custom breakout](https://oshpark.com/shared_projects/BPihxc5Z)
* Supercapacitor (RTC backup)
* 4 Channel 3-5v level shifter (I2C compatible)

