# labkit version 1 bill of materials 

* Containers
  * partitioned case
  * medium parts case
* Full size breadboad
* Arduino Nano or equivalent
* Red, Green, Blue LEDs
* 3x390 ohm, 1/4watt, through hole resistors
* USB A male to mini male cable
* Four position membrane switch (like Adafruit #1332)
