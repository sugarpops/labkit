# LabKit proto version 3 bill of materials 

* Containers
    * partitioned case (Sterilite 1724)
    * medium parts case
* Full size breadboard
* Arduino Nano equivalent
* Red, Green, Blue LEDs
* 3x390 ohm, 1/4watt, through hole resistors
* USB A male to mini male cable
* Four position membrane switch (like Adafruit #1332)
(Rev 3)
* Piezo speaker
* 100 ohm resistor (piezo)
* Pushbutton SPST switch (reset)
* 320x240 TFT display with resistive touch screen (Seeed 2.8" v2.1)
* ST M41T62 RTC chip on [ugly custom breakout](https://oshpark.com/shared_projects/BPihxc5Z)
* Supercapacitor (RTC backup)
* 4 Channel 3-5v level shifter (BSS138-based, I2C compatible)
* White LED (power)
* 1k resistor (power LED)
* SPST pushbutton switch (reset)
* 2 .1uF throughole capacitors (AREF noise, extra bypass for RTC)
* 8 position male .1" centers header (connection to breadboard)
* 15 inches (approx) ribbon cable (interconnect)

