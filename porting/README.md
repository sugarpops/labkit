# Porting from Atmega32 to ESP32

This directory will hold notes, scripts, and programs to do with making code written for Atmega-based Arduinos build and run on an ESP32.

## Immediate problems

* There is no path to the esp32/cores/esp32/libraries being defined for builds within the arduino IDE, so a raft of header files cannot be fund. But actually, the dependencies on these header files are probably going to be difficult anyway, with the possible exception of pgmspace.
* ESP32 may have to be explicitly defined in some cases.
* Pete's development environment needs major work to function with the Espressif cross-tools

