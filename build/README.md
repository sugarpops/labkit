## Build tools

* setup - shell script to source for pathes and aliases
    * usage: . ./setup [<path to Arduino IDE>] 
    * current default Arduino path /opt/arduino-1.8.5
    * Linux/(Cygwin?) make file can be in {/b/d,/opt}/Arduino-Makefile
    * PATH to avr tools set by this script (overrides any in /usr/bin)
* bin - directory for utility scripts
    * b - regular build and view compile/link log
    * bu - build and upload
* Makefile - default make file for regular .ino build
* Makefile.app - default make file for building an app to run with Kanto
* Makefile.shell - default make file for building Kanto
