/*
 * Check an Intel hex file via standard input. 
 * Flags bad checksums.
 * Lists gaps and start address records.
 *
 * Copyright (c) 2018 Pete Soper
 * MIT license (see LICENSE in this repo)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define LINE_SIZE 128

static uint8_t sum = 0;

int main(int argc, char **argv) {
  uint8_t buffer[1024*128]; // enough for an atmega1284
  int next_address = 0;	    // expected next address

  // read standard input
  while (!feof(stdin)) {
    char junk;
    char line[LINE_SIZE];
    int offset,address, length,record_type;
    sum = 0;
    fgets(line,LINE_SIZE,stdin);

    // strip any combo of line terminators
    if ((line[strlen(line)-1] == 0xa) || (line[strlen(line)-1] == 0xd)) {
      line[strlen(line) - 1] = 0;
    }
    if ((line[strlen(line)-1] == 0xa) || (line[strlen(line)-1] == 0xd)) {
      line[strlen(line) - 1] = 0;
    }

    // Break out fields 
    sscanf(line,"%c%2x%4x%2x",&junk,&length,&address,&record_type);

    // Sum fields
    sum += (uint8_t) (length & 0xff);
    sum += (uint8_t) ((address >> 8) & 0xff);
    sum += (uint8_t) (address & 0xff);
    sum += (uint8_t) (record_type & 0xff);

    // Sum data bytes
    int count = 0;
    for (int i=0;i<length;i++) {
      uint32_t tempx;
      sscanf(&line[9+(i*2)],"%2x",&tempx);
      sum += (uint8_t) (tempx & 0xff);
    }

    // Get checksum from record
    int temp;
    sscanf(&line[strlen(line)-2],"%2x",&temp);
    uint8_t s = temp;
    sum = (~sum) + 1;

    // Check and report mismatch
    if ((sum) != s) {
      fprintf(stdout,"%s\n",line);
      fprintf(stdout,"bad checksum: %x sb %x\n", s, sum);
    }

    if (record_type == 0) {
      // Check for gaps if data
      if (address != next_address) {
        fprintf(stdout,"gap: %4x %4x\n", next_address, address-1);
      } 
      next_address = address + length;
    } else if (record_type == 3) {
      // List start address
      uint32_t temp;
      sscanf(&line[9],"%8x",&temp);
      fprintf(stdout, "start address: %x\n", temp);
    } else if (record_type == 1) {
      // EOF - done
      fprintf(stdout,"EOF\n");
      return 0;
    }
  }
}
