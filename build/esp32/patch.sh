#!/bin/bash
#
# Safely apply Espduino32 support to an Arduino IDE install
# With a path argument, install with no options but don't overwrite
# existing files. With -f overwrite or with -r remove support.
# The -r option is designed for the day when doit.am coughs up real 
# support!)

usage () {
  echo $1
  echo "usage: patch [-f | -r] [<arduino ide directory path>]"
  echo "example: patch.sh /opt/arduino-1.8.5"
  exit 1
}

ESP_PATH=hardware/espressif/esp32

force=0
remove=0

if [ $# -eq 2 ] ; then
  if [ $1 = "-f" ] ; then
    force=1
    shift
  elif [ $1 = "-r" ] ; then
    remove=1
    shift
  else
    usage "with two args, first expected to be -f or -r"
  fi
fi

if [ $# -eq 1 ] ; then
  if [ $remove -eq 1 ] ; then
    grep -v '^espduino32\.' <$1/$ESP_PATH/boards.txt >/tmp/b$$
    mv -f /tmp/b$$ $1/$ESP_PATH/boards.txt
    # Make a redundant -r harmless
    if [ -d $1/$ESP_PATH/variants/espduino32 ] ; then
      rm $1/$ESP_PATH/variants/espduino32/pins_arduino.h
      rmdir $1/$ESP_PATH/variants/espduino32
    fi
    exit 0
  else
    if [ -d $1/$ESP_PATH ] ; then
      if [ -d $1/$ESP_PATH/variants/espduino32 ] ; then
	if [ $force -eq 0 ] ; then
	  echo "espduino32 patch already applied"
	  exit 0
	fi
      fi
      cp -r variants $1/$ESP_PATH
      sed -e"s@^espduino32\..*@@" <$1/$ESP_PATH/boards.txt >/tmp/b$$
      cat /tmp/b$$ boards.txt >$1/$ESP_PATH/boards.txt
      rm /tmp/b$$
      exit 0
    else
      usage "no esp32 support found in $1"
    fi
  fi
fi

usage "wrong number of arguments"
