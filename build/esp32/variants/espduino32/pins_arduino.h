#ifndef Pins_Arduino_h
#define Pins_Arduino_h

/*
 * This information NOT FULLY TESTED! User beware. DO NOT spread this on the
 * Internet. This >>> IS NOT <<< in any way an official header file and has
 * no connection to the creators of the ESPDuino32 board. WAIT until this
 * header comment is updated to clearly show it has been fully tested with
 * the board before assuming that it will do anything more than trick you
 * into ruining your hardware.
 *
 * It is ABSOLUTELY CLEAR that the ESPDuino32 board in no way, shape, form
 * or fashion provides "plug and play" compatibilty with Uno form factor
 * Arduino shields. First, the ESPDuino32 is a 3.3 volt system and cannot
 * tolerate five volt input signals nor satisfy the need for 5 volt logic
 * output signals. Second, the pin numbers mapped by the ESP32 core do not
 * in any way correspond to the numbering scheme required by existing 
 * shield software.
 */

/*
 * ESPDuino32 pin definitions. No official file has been found yet, but it
 * should be possible to adapt to it if/when one is found. As near as I can 
 * tell the folks at doit.am created the Espduino32 over a long weekend and
 * then went on to some other project, hoping somebody else would finish the
 * documentation. Multiple searches of their sites, their repositories, 
 * wikis, etc, and the wider Internet turn up redundant copies of the one 
 * sliver of information (a few photos and an incomplete and incorrect 
 * schematic). 
 *
 *
 * Pete Soper May, 2018
 */

#include <stdint.h>

#define EXTERNAL_NUM_INTERRUPTS 16
#define NUM_DIGITAL_PINS        40
#define NUM_ANALOG_INPUTS       16

#define analogInputToDigitalPin(p)  (((p)<20)?(esp32_adc2gpio[(p)]):-1)
#define digitalPinToInterrupt(p)    (((p)<40)?(p):-1)
#define digitalPinHasPWM(p)         (p < 34)

static const uint8_t LED_BUILTIN = 2;
#define BUILTIN_LED  LED_BUILTIN // backward compatibility

static const uint8_t BOOT = 0; // GPIO0 strapping pin

static const uint8_t RX = 4;
static const uint8_t TX = 1;

static const uint8_t SDA = 21;
static const uint8_t SCL = 22;

static const uint8_t SS    = 5;
static const uint8_t MOSI  = 23;
static const uint8_t MISO  = 19;
static const uint8_t SCK   = 18;

static const uint8_t A0 = 39; 
static const uint8_t A1 = 36;
static const uint8_t A2 = 34;
static const uint8_t A3 = 35;
static const uint8_t A4 = 4;
static const uint8_t A5 = 2;

static const uint8_t T0 = 3;
static const uint8_t T1 = 1;
static const uint8_t T2 = 26;
static const uint8_t DAC1 = T2; // Following Espressif wacko 1-origin numbering
static const uint8_t T3 = 25;
static const uint8_t DAC2 = T3;
static const uint8_t T4 = 17;
static const uint8_t T5 = 16;
static const uint8_t T6 = 27;
static const uint8_t T7 = 14;
static const uint8_t T8 = 12;
static const uint8_t T9 = 13;
static const uint8_t T10 = 5;
static const uint8_t T11 = 23;
static const uint8_t T12 = 19;
static const uint8_t T13 = 18;

#endif /* Pins_Arduino_h */
