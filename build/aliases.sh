# Suggested shell aliases to make life easier

# define these in ~/.bashrc to override the default

if [ "$MY_BAUD " = " " ] ; then
  export MY_BAUD=9600
fi

# Initialize labkit build environment
alias in='cd $LABKIT/build;. ./setup;cd ..'
# ESP32
alias in32='export LABKIT_TARGET=ESP32;in'

# Initialize labkit build environment but don't change directories
alias inh='pushd .;in;popd'
# ESP32
alias in32h='pushd .;in32;popd'

# Get into the guts
alias avr='pushd .;cd $ARDUINO_DIR/hardware/tools/avr/avr'
alias esp32='pushd .;cd $ARDUINO_DIR/hardware/espressif/esp32'
alias core='pushd .;cd $ARDUINO_DIR/hardware/arduino/avr/cores/arduino'
alias core32='pushd .;cd $ARDUINO_DIR/hardware/espressif/esp32/cores/esp32'
alias lib='cd $LABKIT/lib'
alias t='cd $LABKIT/lib/Task'

# Monitors for first, second, and third board plugged in
alias cn0='screen /dev/ttyACM0 $MY_BAUD'
alias cn='cn'
alias cn1='screen /dev/ttyACM1 $MY_BAUD'
alias cn2='screen /dev/ttyACM2 $MY_BAUD'
alias sn0='screen /dev/ttyUSB0 $MY_BAUD'
alias sn='sn'
alias sn1='screen /dev/ttyUSB1 $MY_BAUD'
alias sn2='screen /dev/ttyUSB2 $MY_BAUD'

# Not sure how to do this for esp32 yet (too lazy to look it up)
alias ds='avr-objdump -d build-uno/*.elf | less'

alias findvi='ps -eaf | grep /usr/bin/vi | grep -v grep'

# git time savers
alias gd='git diff '
alias gitcache='git config credential.helper cache'
alias gst='git status | less'
alias newlib='rm -rf $LABKIT/lib/*/build-uno;rsync -avz $LABKIT/lib/* $ARDUINO_DIR/libraries'
