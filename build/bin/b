#!/bin/bash
# Do a build with output to a log file and then view the log file.
# Any args are passed to the make except "-nolog" that causes output to go to
# standard output instead of a log (e.g. we're being invoked by the regression
# test system and so all output is being collected into that log file).

nolog=0
if [ $# -gt 0 ] ; then
  if [ $1 = "-nolog" ] ; then
    nolog=1
    shift
  fi
fi

# Look at directory context and decide what make file to use
top=`pick-make-file`

# Full path
MF=$LABKIT/build/$top

# Log what we're doing
if [ $nolog -eq 1 ] ; then
  echo make -f $MF $@
else
  echo make -f $MF $@ >log 2>&1
fi

# The build
if [ $nolog -eq 1 ] ; then
  make -f $MF MAKE="make -f $MF" $@
else
  make -f $MF MAKE="make -f $MF" $@ >>log 2>&1
fi

# Detect failure to avoid obscuring error messages

if [ $? -ne 0 ] ; then
  if [ $nolog -eq 1 ] ; then
    echo "MAKE FAILED"
  else
    echo "MAKE FAILED" >>log
    less log
  fi
  exit 1
fi

# Figure out more context
top=`basename $MF`
if [ "$root " = "Makefile.kantoboot " ] ; then
  # Have to use the chip programmer for the bootsections
  # Programs both optiboot and Kanto bootloader
  if [ $nolog -eq 1 ] ; then
    flash-bootsection.sh
  else
    flash-bootsection.sh >>log 2>&1
  fi
fi

if [ $nolog -eq 0 ] ; then
  less log
fi
